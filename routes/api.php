<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('product')->group(function () {
    Route::get('/{brand?}/{category?}','API\SiteController@getProduct');
});
Route::prefix('category')->group(function () {
    Route::get('/','API\SiteController@getCategory');
    Route::get('/subcategory/{category}','API\SiteController@getSubCategory');
});
Route::prefix('brands')->group(function () {
    Route::get('/','API\SiteController@getBrand');
});


Route::prefix('saleOrder')->group(function () {
    Route::get('/','API\SiteController@saleOrder');
});

Route::prefix('saleOrderStatus')->group(function () {
    Route::get('/','API\SiteController@saleOrderStatus');
});


Route::prefix('createProduct')->group(function () {
    Route::get('/','API\SiteController@createProduct');
});
