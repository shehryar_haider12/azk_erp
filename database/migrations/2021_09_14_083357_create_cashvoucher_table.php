<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCashvoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher', function (Blueprint $table) {
            $table->id();
            $table->string('account1', 255)->nullable();
            $table->string('account1Code', 255)->nullable();
            $table->string('account2', 255)->nullable();
            $table->string('account2Code', 255)->nullable();
            $table->string('debit', 200)->nullable();
            $table->string('credit', 200)->nullable();
            $table->string('description', 200)->nullable();
            $table->date('accounting_date')->nullable();
            $table->date('posted_date')->nullable();
            $table->string('period', 200)->nullable();
            $table->string('status', 200)->nullable()->default('Pending');
            $table->string('type', 200)->nullable();
            $table->string('v_type', 200)->nullable();
            $table->unsignedBigInteger('created_by')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cashvoucher');
    }
}
