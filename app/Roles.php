<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $table = 'roles';
    protected $fillable = [
        'name',
        'description',
        'status',
        'created_by',
        'updated_by',
    ];

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }

    public function roleM()
    {
        return $this->hasMany('App\RoleMenu','r_id','id');
    }

    public function user()
    {
        return $this->hasMany('App\User','r_id','id');
    }

    public function roleP()
    {
        return $this->hasMany('App\RolePermission','r_id','id');
    }
}
