<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSupplierUpdate extends Model
{
    protected $table = 'product_supplier_update';
    protected $primaryKey = 'id';
    protected $fillable = [
        'p_id',
        's_id',
        'type',
    ];

    public function variant()
    {
        return $this->hasOne('App\ProductVariants','id','p_id');
    }

    public function products()
    {
        return $this->hasOne('App\Products','id','p_id');
    }
    public function supplier()
    {
        return $this->hasOne('App\Vendors','id','s_id');
    }
}
