<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'area';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
    ];

    public function customer()
    {
        return $this->hasOne('App\Vendors','a_id','id');
    }
    public function customerreq()
    {
        return $this->hasOne('App\VendorsUpdateRequest','a_id','id');
    }
}
