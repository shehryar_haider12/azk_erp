<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $table = 'warehouse';
    protected $primaryKey = 'id';
    protected $fillable = [
        'w_name',
        'w_address',
        'w_type',
        'w_contactNo',
        'c_p_name',
        'c_p_contactNo',
        'c_id',
        'created_by',
        'updated_by',
    ];
    public function city()
    {
        return $this->hasOne('App\City','id','c_id');
    }
    public function products()
    {
        return $this->belongsTo('App\Products');
    }
    public function purchase()
    {
        return $this->belongsTo('App\PurchaseOrder');
    }

    public function request()
    {
        return $this->belongsTo('App\PurchaseOrder');
    }
    public function sale()
    {
        return $this->belongsTo('App\Sales');
    }
    public function stocks()
    {
        return $this->belongsTo('App\Stocks');
    }

    public function currentstocks()
    {
        return $this->belongsTo('App\CurrentStock');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }

    public function transfer()
    {
        return $this->belongsTo('App\ProductTransaferHistory');
    }

    public function stockout()
    {
        return $this->belongsTo('App\StockOut');
    }

}
