<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseRequestDetails extends Model
{
    protected $table = 'purchase_request_details';
    protected $primaryKey = 'id';
    protected $fillable = [
        'o_id',
        'p_id',
        'quantity',
        'type'
    ];

    public function products()
    {
        return $this->hasOne('App\Products','id','p_id');
    }

    public function variant()
    {
        return $this->hasOne('App\ProductVariants','id','p_id');
    }


    public function request()
    {
        return $this->hasOne('App\PurchaseRequest','id','o_id');
    }

    public function qdetails()
    {
        return $this->hasMany('App\QuotationDetails','p_id','p_id');
    }
}
