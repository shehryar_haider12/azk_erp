<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Incentives;
use App\GeneralLedger;
use App\AccountDetails;
use App\Vendors;
use DataTables;
use Carbon\Carbon;
use Auth;

class IncentivesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view()
    {
        $vendor=Vendors::where('v_type','Saleperson')->get();
        return view('incentive.index',compact('vendor'));
    }

    public function datatable()
    {
        $inc = Incentives::with('saleperson')->get();
        return DataTables::of($inc)->make();
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $inc = Incentives::with('saleperson')->where('sp_id',$request->id)
        ->where('month',$request->month)
        ->first();
        Incentives::where('sp_id',$request->id)
        ->where('month',$request->month)
        ->update([
            'status' => $request->status
        ]);


        $u_id = Auth::user()->id;
        $id = GeneralLedger::max('id');
        $ledger = GeneralLedger::where('id',$id)->first();
        if($ledger == null)
        {
            $link_id=1;
        }
        else
        {
            $link_id = $ledger->link_id + 1;
        }
        $posted_date = Carbon::now()->format('Y-m-d');
        $period = Carbon::now()->format('M-y');


        $account_cash = AccountDetails::where('name_of_account','Cash In Hand')->first();
        $cashL = GeneralLedger::where('account_code',$account_cash->Code)
        ->get();
        if($cashL->isEmpty())
        {
            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Amount Paid to sale incentive'.$inc->saleperson->name,
                'account_name' => $account_cash->name_of_account,
                'link_id' => $link_id,
                'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'account_code' => $account_cash->Code,
                // 'transaction_no' => $ref_no,
                'currency_code' =>'PKR',
                'credit' => $inc->amount,
                'debit' => '0',
                'net_value' => 0 - $inc->amount ,
                'balance' => 0 - $inc->amount,
            ]);
        }
        else
        {
            $balance = 0;
            foreach ($cashL as $key => $c) {
                $balance+=$c->net_value;
            }
            $cash_n = 0;
            $cash_n = $balance + (0 - $inc->amount);

            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Amount Paid to sale incentive'.$inc->saleperson->name,
                'account_name' => $account_cash->name_of_account,
                'link_id' => $link_id,
                'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'account_code' => $account_cash->Code,
                // 'transaction_no' => $ref_no,
                'currency_code' =>'PKR',
                'credit' => $inc->amount,
                'debit' => '0',
                'net_value' => 0 - $inc->amount ,
                'balance' => $cash_n
            ]);
        }


    }
}
