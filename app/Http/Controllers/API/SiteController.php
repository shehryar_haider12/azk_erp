<?php

namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products;
use App\Category;
use App\Subcategory;
use App\Brands;

class SiteController extends Controller
{
    public function getProduct(Request $request, $brand = 0, $category = 0)
    {
        if($category && $brand){

            $products = Products::where([['cat_id',$category],['brand_id',$brand],['status',1]])->get();
        }
        elseif ($category) {

            $products = Products::where([['cat_id',$category],['status',1]])->get();
        }
        elseif ($brand) {

            $products = Products::where([['brand_id',$brand],['status',1]])->get();
        }else{

            $products = Products::where([['status',1]])->get();
        }
        return response()->json([
            'product'   =>  $products
        ],200);
    }

    public function getCategory()
    {
        $category = Category::where([['status',1]])->get();
        return response()->json([
            'category'   =>  $category
        ],200);
    }

    public function getSubCategory(Request $request, $category)
    {
        $category = Subcategory::where([['cat_id',$category],['status',1]])->get();
        return response()->json([
            'category'   =>  $category
        ], 200);
    }

    public function getBrand()
    {
        $brand = Brands::where([['status',1]])->get();
        return response()->json([
            'brand'   =>  $brand
        ], 200);
    }
}

?>
