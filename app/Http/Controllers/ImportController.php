<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\ProductSupplier;
use App\HeadCategory;
use App\AccountDetails;
use App\Brands;
use App\Category;
use App\Subcategory;
use App\User;
use App\Unit;
use App\Vendors;
use DataTables;
use Auth;
use DB;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class ImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function product(Request $request)
    {
        $request->validate([
             'csv_file' => 'required',
        ]);
        $filePath       =   pathinfo($request->csv_file)['dirname'].'/'.pathinfo($request->csv_file)['basename'];
        $data           =   readCsv($filePath);
        $u_id = Auth::user()->id;

        for ($i = 0; $i < count($data); $i ++)
        {
            $brand          =   Brands::where('b_name',$data[$i]['brand'])->first();
            $category       =   Category::where('cat_name',$data[$i]['category'])->first();
            $subcategory    =   Subcategory::where('s_cat_name',$data[$i]['sub-category'])->first();
            $unit           =   Unit::where('u_name',$data[$i]['unit'])->first();
            $supplier       =   Vendors::where([['v_type','Supplier'],['name',$data[$i]['Supplier'],['company',$data[$i]['Company']]]])->first();

            if(empty($brand)){
                $brand = Brands::create([
                    'b_name'  => $data[$i]['brand']
                ]);
            }
            if(empty($category)){
                $category = Category::create([
                    'cat_name'  => $data[$i]['category']
                ]);
            }
            if(empty($subcategory)){
                $subcategory = Subcategory::create([
                    'cat_id'        => $category->id,
                    's_cat_name'    => $data[$i]['sub-category']
                ]);
            }
            if(empty($unit)){
                $unit = Unit::create([
                    'u_name'  => $data[$i]['unit']
                ]);
            }
            if(empty($supplier)){
                $supplier = Vendors::create([
                    'name'      => $data[$i]['Supplier'],
                    'company'   => $data[$i]['Company'],
                    'v_type'    => 'Supplier',
                    'status'    => 1
                ]);
                $hcat = HeadCategory::where('name','Payables')->first();

                $account = AccountDetails::where('c_id',$hcat->id)
                ->latest('created_at')->orderBy('id','desc')->first();

                if($account == null)
                {
                    $sid = 001;
                }
                else
                {
                    $sid = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                }

                $str_length = strlen((string)$sid)+2;
                $sid = substr("0000{$sid}", -$str_length);
                $code = $hcat->code.'-'.$sid;

                $data_supplier = [
                    'Code' => $code,
                    'name_of_account' => $data[$i]['Supplier'].' - '.$data[$i]['Company'],
                    'c_id' => $hcat->id,
                    'created_by' => $u_id
                ];
                AccountDetails::create($data_supplier);
            }
            $product = Products::where('pro_name','LIKE','%'.$data[$i]['product name'].'%')->first();
            // dd($unit);s
            $product_data=[
                'pro_name'         =>   $data[$i]['product name'],
                'brand_id'         =>   $brand->id,
                'cat_id'           =>   $category->id,
                's_cat_id'         =>   $subcategory->id,
                'p_type'           =>   'Finished',
                'pro_code'         =>   $data[$i]['code'],
                'unit_id'          =>   $unit->id,
                'cost'             =>   $data[$i]['cost'],
                'price'            =>   $data[$i]['price'],
                'case'              =>   $data[$i]['Case'],
                'description'      =>   $data[$i]['description'] ?? '',
                'mrp'              =>   $data[$i]['mrp'],
                'company_name'      =>   $data[$i]['Company'],
                'weight'           =>   '',
                'alert_quantity'   =>   10,
                'created_by'       =>   Auth::user()->id,
                'status'           =>   1,
                'vstatus'          =>   0
            ];

            if(empty($product))
            {
                $product = Products::create($product_data);

                ProductSupplier::create([
                    'p_id' => $product->id,
                    'type' => 0,
                    's_id' => $supplier->id,
                ]);
                $hcat = HeadCategory::where('name','Inventory')->first();

                $account = AccountDetails::where('c_id',$hcat->id)
                ->latest('created_at')->orderBy('id','desc')->first();

                if($account == null)
                {
                    $id = 001;
                }
                else
                {
                    $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                }

                $str_length = strlen((string)$id)+2;
                $id         = substr("0000{$id}", -$str_length);
                $code       = $hcat->code.'-'.$id;

                $data1 = [
                    'Code'              =>  $code,
                    'name_of_account'   =>  $product->pro_code.' - '.$product->pro_name,
                    'c_id'              =>  $hcat->id,
                    'created_by'        =>  Auth::user()->id,
                    'type'              =>  0
                ];
                AccountDetails::create($data1);
            }
        }
        return redirect()->back()->with('uploaded','Product Uploaded Successfully');
    }

}
