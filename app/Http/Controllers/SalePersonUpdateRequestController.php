<?php

namespace App\Http\Controllers;

use App\Vendors;
use App\VendorsUpdateRequest;
use DataTables;
use Auth;
use Illuminate\Http\Request;

class SalePersonUpdateRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('saleperson.request',compact('permissions'));
    }

    public function datatable()
    {
        $vendor=VendorsUpdateRequest::where('v_type','Saleperson')->get();
        return DataTables::of($vendor)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function status($id)
    {
        $v = VendorsUpdateRequest::find($id);
        $u_id = Auth::user()->id;

        if($v->password == null)
        {
            User::where('id',$v->u_id)
            ->update([
                'name' => $v->name,
                'email' => $v->email
            ]);
        }
        else
        {
            User::where('id',$v->u_id)
            ->update([
                'name' => $v->name,
                'email' => $v->email,
                'password' => $v->password
            ]);
        }
        $vendor=Vendors::where('id',$id)
        ->update([
            'name' => $v->name,
            'address' => $v->address,
            'c_no' => $v->c_no,
            'email' => $v->email,
            'c_id' => $v->c_id,
            'updated_by' => $u_id,
            'p_type' => $v->p_type,
            'commission' => $v->commission,
        ]);
        VendorsUpdateRequest::where('id',$id)
        ->update([
            'status' => 'Approved'
        ]);

        toastr()->success('Saleperson updated successfully!');
        return redirect(url('').'/salepersonUpdateRequest');
    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->ajax())
        {
            $vendor=Vendors::with(['city'])
            ->where('id',$id)
            ->first();
            return $vendor;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
