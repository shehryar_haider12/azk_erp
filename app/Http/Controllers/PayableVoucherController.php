<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Voucher;
use App\AccountDetails;
use App\GeneralLedger;
use App\TransactionHistory;
use App\PurchaseOrder;
use App\Vendors;
use App\User;
use App\Bank;
use DataTables;
use Auth;
use DB;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;
use Carbon\Carbon;

class PayableVoucherController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $account = AccountDetails::with(['headCategory'])->get();
        return view('payablevoucher.index',compact('account'));
    }

    public function datatable()
    {
        $cv = Voucher::with(['createdUser'])->where('v_type','Payable')->get();
        return DataTables::of($cv)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cash=Voucher::max('id');
        if($cash == null)
        {
            $id=1;
        }
        else
        {
            $id=$cash+1;
        }
        $data =[
            'account'    => AccountDetails::with(['headCategory'])->get(),
            'isEdit'  => false,
            'id'      => $id,
            'cby'     => Auth::user()->name,
            'date'    => Carbon::now()->format("Y-m-d"),
            'u_id'     => Auth::user()->id,
        ];

        return view('payablevoucher.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'accounting_date'      =>  'required',
            'posted_date'          =>  'required',
            'period'               =>  'required',
            'account1'             =>  'required',
            'account2'             =>  'required',
            'description'          =>  'required',
            'debit'                =>  'required',
            'type'                =>  'required',
        ]);
        $data['account1Code'] = AccountDetails::where('name_of_account',$request->account1)->pluck('Code')->first();
        $data['account2Code'] = AccountDetails::where('name_of_account',$request->account2)->pluck('Code')->first();
        $data['credit']       = $data['debit'];
        $data['v_type']       = $request->v_type;
        $data['created_by']   = $request->created_by;
        Voucher::create($data);


        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'New Payable voucher has been added by '.$u_name,
            'link' => url('').'/payableVoucher',
            'name' => 'View Payable Vouchers',
        ];
        Notification::send($user, new AddNotification($data1));
        toastr()->success('Payable Voucher added successfully!');
        return redirect(url('').'/payableVoucher');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $voucher = Voucher::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=[
            'isEdit' => true,
            'rcv'   =>   Voucher::find($id),
            'account'    => AccountDetails::with(['headCategory'])->get(),
        ];
        return view('payablevoucher.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'accounting_date'      =>  'required',
            'posted_date'          =>  'required',
            'period'               =>  'required',
            'account1'             =>  'required',
            'account2'             =>  'required',
            'description'          =>  'required',
            'debit'                =>  'required',
            'type'                =>  'required',
        ]);
        $data['account1Code'] = AccountDetails::where('name_of_account',$request->account1)->pluck('Code')->first();
        $data['account2Code'] = AccountDetails::where('name_of_account',$request->account2)->pluck('Code')->first();
        $data['credit']       = $data['debit'];

        Voucher::where('id',$id)->update($data);


        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'Payable voucher has been updated by '.$u_name,
            'link' => url('').'/payableVoucher',
            'name' => 'View Payable Vouchers',
        ];
        Notification::send($user, new AddNotification($data1));
        toastr()->success('Payable Voucher added successfully!');
        return redirect(url('').'/payableVoucher');
    }

    public function showBalance($name)
    {
        $balance  = GeneralLedger::where('account_name',$name)->latest('created_at')->orderBy('id','desc')->first();
        return $balance;
    }

    public function status(Request $request)
    {
        $id     = $request->id;
        $status = $request->status;
        $cash = Voucher::find($id);
        $u_id = Auth::user()->id;
        $id = GeneralLedger::max('id');
        $ledger = GeneralLedger::where('id',$id)->first();
        if($ledger == null)
        {
            $link_id=1;
        }
        else
        {
            $link_id = $ledger->link_id + 1;
        }

        if ($cash->update(['status' => $status])) {
            $cash->update([
                'status' => $status,
            ]);
            if($cash->type == "Cash")
            {
                $account_cash = AccountDetails::where('name_of_account',$cash->account2)->first();
                $cashL = GeneralLedger::where('account_code',$account_cash->Code)
                ->get();
                $b_id = null;
                if($cashL->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid to '.$cash->account1,
                        'account_name' => $account_cash->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $cash->accounting_date,
                        'posted_date' => $cash->posted_date,
                        'period' => $cash->period,
                        'account_code' => $account_cash->Code,
                        // 'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'credit' => $cash->credit,
                        'debit' => '0',
                        'net_value' => 0 - $cash->credit ,
                        'balance' => 0 - $cash->credit ,
                    ]);
                }
                else
                {
                    $balance = 0;
                    foreach ($cashL as $key => $c) {
                        $balance+=$c->net_value;
                    }
                    $cash_n = 0;
                    $cash_n = $balance + (0 - $cash->credit );

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid to '.$cash->account1,
                        'account_name' => $account_cash->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $cash->accounting_date,
                        'posted_date' => $cash->posted_date,
                        'period' => $cash->period,
                        'account_code' => $account_cash->Code,
                        // 'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'credit' => $cash->credit,
                        'debit' => '0',
                        'net_value' => 0 - $cash->credit ,
                        'balance' => $cash_n
                    ]);
                }
            }
            else
            {
                $account_b = AccountDetails::where('name_of_account',$cash->account2)->first();
                $bank_l = GeneralLedger::where('account_code',$account_b->Code)
                ->get();

                $b_name =  substr($cash->account1,0,strpos($cash->account2,'-')-1);
                $branch = substr($cash->account1,strpos($cash->account2,'-')+2);

                $bank = Bank::where('branch',$branch)->where('name',$b_name)->first();
                $b_id = $bank->id;
                if($bank_l->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid to: '.$cash->account1,
                        'account_name' => $account_b->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $cash->accounting_date,
                        'posted_date' => $cash->posted_date,
                        'period' => $cash->period,
                        'account_code' => $account_b->Code,
                        // 'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'credit' => $cash->credit,
                        'debit' => '0',
                        'net_value' => 0 - $cash->credit,
                        'balance' => 0 - $cash->credit
                    ]);
                }
                else
                {
                    $balance = 0;
                    foreach ($bank_l as $key => $c) {
                        $balance+=$c->net_value;
                    }
                    $bank_n = 0;
                    $bank_n = $balance + (0 - $cash->credit);

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid to: '.$cash->account1,
                        'account_name' => $account_b->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $cash->accounting_date,
                        'posted_date' => $cash->posted_date,
                        'period' => $cash->period,
                        'account_code' => $account_b->Code,
                        // 'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'credit' => $cash->credit,
                        'debit' => '0',
                        'net_value' => 0 - $cash->credit ,
                        'balance' => $bank_n
                    ]);
                }
            }
            $supplier_n = 0;
            $account_c = AccountDetails::where('name_of_account',$cash->account1)
            ->first();
            $suuplier = GeneralLedger::where('account_code',$account_c->Code)
            ->get();
            $balance = 0;
            foreach ($suuplier as $key => $c) {
                $balance+=$c->net_value;
            }
            $supplier_n = $balance + ( $cash->debit);

            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Amount paid for purchase',
                'account_name' => $account_c->name_of_account,
                'link_id' => $link_id,
                'created_by' => $u_id,
                'accounting_date' => $cash->accounting_date,
                'posted_date' => $cash->posted_date,
                'period' => $cash->period,
                'account_code' => $account_c->Code,
                // 'transaction_no' => $ref_no,
                'currency_code' => 'PKR',
                'debit' => $cash->debit,
                'credit' => '0',
                'net_value' =>  $cash->debit - 0,
                'balance' => $supplier_n
            ]);
            $accountname=  explode('-',$cash->account1);
            $company = $accountname[count($accountname) - 1];
            $supplier = Vendors::where([['name','like','%'.$accountname[0].'%'],['company',trim($company)]])->where('v_type','Supplier')->first();
            $purchase = PurchaseOrder::where('s_id',$supplier->id)->where('p_status','Partial')
            ->orWhere('p_status','Pending')->orderBy('id','asc')->get();
            $amount = $cash->debit;
            foreach ($purchase as $key => $s) {
                if($amount < $s->total && $amount != 0)
                {
                    PurchaseOrder::where('id',$s->id)->update([
                        'p_status' => 'Partial'
                    ]);
                }
                else
                {
                    PurchaseOrder::where('id',$s->id)->update([
                        'p_status' => 'Paid'
                    ]);
                }
                TransactionHistory::create([
                    'p_s_id' => $s->id,
                    'p_type' => 'Purchase',
                    't_type' => 'Sent',
                    'paid_by' => $cash->type,
                    'total' => $amount,
                    'b_id' => $b_id,
                    'created_by' => $u_id
                ]);
                $amount-=$s->total;
            }


            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }
}
