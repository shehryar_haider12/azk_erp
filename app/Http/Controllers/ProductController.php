<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Products;
use App\ProductsUpdateRequest;
use App\Unit;
use App\Brands;
use App\Category;
use App\Vendors;
use App\Variants;
use App\ProductVariants;
use App\ProductVariantUpdate;
use App\Stocks;
use App\StockReceiving;
use App\Subcategory;
use App\Warehouse;
use App\CurrentStock;
use App\FinishProducts;
use App\HeadCategory;
use App\AccountDetails;
use App\GeneralLedger;
use App\User;
use App\ProductSupplier;
use App\ProductSupplierUpdate;
use DataTables;
use Storage;
use DB;
use App\Exports\ProductsExport;
use App\Exports\FinishProductExport;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use Carbon\Carbon;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //pricechecker
    public function productsearch($id)
    {
        $product = Products::find($id);
        return $product;
    }

    public function index(Request $request)
    {
        $cat=Category::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('product.index',compact('cat','brands','permissions'));
    }

    public function datatable()
    {
        $product=Products::with(['brands','unit','category','subcategory','variants','Psupplier.supplier'])
        ->doesntHave('finish.rawproducts')
        ->withCount(['currentstocks as total' => function($query) {
            $avg = $query->select(DB::raw('sum(quantity)'))->where('type',0);
        }])
        ->withCount(['currentstocks as unit_total' => function($query) {
            $avg = $query->select(DB::raw('sum(unit_quantity)'))->where('type',0);
        }])
        ->get();
        // return $product;
        return DataTables::of($product)->make();
    }

    public function finishProducts(Request $request)
    {
        $cat=Category::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('product.Finishindex',compact('cat','brands','permissions'));
    }

    public function finishProductsDatatable()
    {
        $product=Products::with(['brands','unit','category','subcategory'])
        ->has('finish.rawproducts')
        ->withCount(['currentstocks as total' => function($query) {
            $avg = $query->select(DB::raw('sum(quantity)'))->where('type',0);
        }])
        ->where('p_type','Finished')
        ->get();
        // return $product;
        return DataTables::of($product)->make();
    }

    public function modal($id)//sales create using warehouse
    {
        $product=CurrentStock::with(['products','products.brands','products.unit','products.category','products.subcategory','variant.product.brands','variant.product.category','variant.product.unit'])
        ->where('w_id',$id)
        ->get();
        return DataTables::of($product)->make();
    }

    public function detail($id)//each finish product detail
    {
        $fp = FinishProducts::with(['rawproducts','rawproducts2'])
        ->where('id',$id)
        ->get();
        return $fp;
    }

    public function finishProductEdit($id)
    {
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $ware=Warehouse::all();
        $code ='P-'.rand();
        $fp = FinishProducts::with(['rawproducts.brands','rawproducts.unit'])
        ->where('id',$id)
        ->get();
        $supplier = Vendors::where('v_type','Supplier')
        ->where('status',1)->get();
        $product = Products::find($id);
        $pro=Products::with(['brands','unit','category','subcategory'])
        ->doesntHave('finish.rawproducts')
        ->get();
        $ps = ProductSupplier::where('p_id',$id)->get();
        $data= [
            'isEdit' => true,
            'unit' => $unit,
            'cat' => $cat,
            'sub' => $sub,
            'brands' => $brands,
            'ware' => $ware,
            'product' => $product,
            'pro' => $pro,
            'fp' => $fp,
            'code' => $code,
            'supplier' => $supplier,
            'ps' => $ps,
        ];
        return view('product.finish',$data);
    }

    public function finishProductUpdate($id,Request $request)
    {
        $product = Products::find($id);
        $u_id = Auth::user()->id;
        $unit = Unit::where('u_name' , $request->unit_id)
        ->first();
        DB::table('finish_products')->where('id', $id)->delete();
        DB::table('product_supplier')->where('p_id', $id)->delete();
        $final = 0;
        for ($i=0; $i <count($request->p_id) ; $i++)
        {
            if($unit->u_name == 'Mililiter' && $request->unit1[$i] == 'Liter')
            {
                $final = 1 * $request->quantity[$i];
            }
            else if($unit->u_name == 'Liter' && $request->unit1[$i] == 'Liter')
            {
                $final = 1 * $request->quantity[$i] * 1000;
            }
            else if($unit->u_name == 'Grams' && $request->unit1[$i] == 'Kilograms')
            {
                $final = 1 * $request->quantity[$i];
            }
            else if($unit->u_name == 'Kilograms' && $request->unit1[$i] == 'Kilograms')
            {
                $final = 1 * $request->quantity[$i] * 1000;
            }
            else
            {
                $final =$request->quantity[$i] * 1;
            }
            FinishProducts::create([
                'id' => $id,
                'p_id' => $request->p_id[$i],
                'quantity' => $final
            ]);
        }
        if(isset($request->image))
        {
            $data=([
                'image' => $request->image,
                's_cat_id' => $request->s_cat_id,
                'pro_name' => $request->pro_name,
                'pro_code' => $request->pro_code,
                'weight' => $request->weight,
                'unit_id' => $unit->id,
                'cost' => $request->cost,
                'price' => $request->price,
                // 'quantity' => $request->quantity,
                'alert_quantity' => $request->alert_quantity,
                'brand_id' => $request->brand_id,
                'cat_id' => $request->cat_id,
                // 'w_id' => $request->w_id,
                'visibility' => $request->visibility,
                'description' => $request->description,
                'updated_by' => $u_id,
                'mrp' => $request->mrp
            ]);
            $data['image']= Storage::disk('uploads')->putFile('',$request->image);
            Products::where('id',$id)
            ->update($data);
            toastr()->success('Product updated successfully!');
            return redirect(url('').'/product/finishProducts');
        }
        else
        {
            $data=([
                's_cat_id' => $request->s_cat_id,
                'pro_name' => $request->pro_name,
                'pro_code' => $request->pro_code,
                'weight' => $request->weight,
                'unit_id' => $unit->id,
                'cost' => $request->cost,
                'price' => $request->price,
                // 'quantity' => $request->quantity,
                'alert_quantity' => $request->alert_quantity,
                'brand_id' => $request->brand_id,
                'cat_id' => $request->cat_id,
                // 'w_id' => $request->w_id,
                'visibility' => $request->visibility,
                'description' => $request->description,
                'updated_by' => $u_id,
                'mrp' => $request->mrp
            ]);
            Products::where('id',$id)
            ->update($data);
        }
        for ($i=0; $i < count($request->s_id) ; $i++) {
            ProductSupplier::create([
                'p_id' => $p->id,
                'type' => 0,
                's_id' => $request->s_id[$i],
            ]);
        }
        $hcat = HeadCategory::where('name','Inventory')->first();

        $account = AccountDetails::where('c_id',$hcat->id)
        ->latest('created_at')->first();

        if($account == null)
        {
            $id = 001;
        }
        else
        {
            $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
        }

        $str_length = strlen((string)$id)+2;
        $id = substr("0000{$id}", -$str_length);
        $code = $hcat->code.'-'.$id;
        $data1 = [
            'Code' => $code,
            'name_of_account' => $request->pro_name.' - '.$request->pro_code,
            'c_id' => $hcat->id,
            'created_by' => $u_id
        ];
        AccountDetails::create($data1);

        toastr()->success('Product updated successfully!');
        return redirect(url('').'/product/finishProducts');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $menu_id =   getMenuId($request);
        $supplier = Vendors::where('v_type','Supplier')
        ->where('status',1)->get();
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $ware=Warehouse::all();
        $variant=Variants::all();
        $code =rand();
        $data= [
            'isEdit' => false,
            'unit' => $unit,
            'cat' => $cat,
            'sub' => $sub,
            'brands' => $brands,
            'ware' => $ware,
            'code' => $code,
            'variant' => $variant,
            'supplier' => $supplier,
            'permissions' => getRolePermission($menu_id)
        ];
        return view('product.create',$data);
    }


    public function addDT()
    {
        $product=DB::select("SELECT * FROM brands,units, products where (products.p_type='Raw Material' or products.p_type='Packaging')
        and brands.id = products.brand_id and units.id = products.unit_id");
        return DataTables::of($product)->make();
    }

    public function add(Request $request)
    {
        $menu_id =   getMenuId($request);
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $ware=Warehouse::all();
        $code = rand();
        $supplier = Vendors::where('v_type','Supplier')
        ->where('status',1)->get();
        $pro=Products::with(['brands','unit','category','subcategory'])
        ->doesntHave('finish.rawproducts')
        ->get();
        $data= [
            'isEdit' => false,
            'unit' => $unit,
            'cat' => $cat,
            'sub' => $sub,
            'brands' => $brands,
            'ware' => $ware,
            'pro' => $pro,
            'code' => $code,
            'supplier' => $supplier,
            'permissions' => getRolePermission($menu_id)
        ];
        return view('product.finish',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function addFinishProduct(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'pro_code'      =>  'required|string|max:255|unique:products'
        ]);

        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $status = 1;
        }
        else
        {
            $status = 0;
        }

        $vstatusp=0;
        // dd($status);

        $u_id = Auth::user()->id;
        $unit = Unit::find($request->unit_id);
        $quantity = 0;
        $date=Carbon::now()->format('Y-m-d');
        if(isset($request->image))
        {
            $data=([
                'image' => $request->image,
                's_cat_id' => $request->s_cat_id,
                'pro_name' => $request->pro_name,
                'pro_code' => $request->pro_code,
                'weight' => $request->weight,
                'unit_id' => $request->unit_id,
                'cost' => $request->cost,
                'price' => $request->price,
                'alert_quantity' => $request->alert_quantity,
                'brand_id' => $request->brand_id,
                'cat_id' => $request->cat_id,
                // 'w_id' => $request->w_id,
                'visibility' => $request->visibility,
                'description' => $request->description,
                'p_type' => 'Finished',
                'created_by' => $u_id,
                'status' => $status,
                'vstatus' => $vstatusp,
                'mrp' => $request->mrp
            ]);
            $data['image']= Storage::disk('uploads')->putFile('',$request->image);
        }
        else
        {
            $data=([
                's_cat_id' => $request->s_cat_id,
                'pro_name' => $request->pro_name,
                'pro_code' => $request->pro_code,
                'weight' => $request->weight,
                'unit_id' => $request->unit_id,
                'cost' => $request->cost,
                'price' => $request->price,
                'alert_quantity' => $request->alert_quantity,
                'brand_id' => $request->brand_id,
                'cat_id' => $request->cat_id,
                // 'w_id' => $request->w_id,
                'visibility' => $request->visibility,
                'description' => $request->description,
                'p_type' => 'Finished',
                'created_by' => $u_id,
                'status' => $status,
                'vstatus' => $vstatusp,
                'mrp' => $request->mrp
            ]);
        }

        $p = Products::create($data);
        for ($i=0; $i < count($request->s_id) ; $i++) {
            ProductSupplier::create([
                'p_id' => $p->id,
                'type' => 0,
                's_id' => $request->s_id[$i],
            ]);
        }
        $final = 0;
        for ($i=1; $i <= count($request->p_id) ; $i++) {
            if($unit->u_name == 'Mililiter' && $request->unit1[$i] == 'Liter')
            {
                $final = 1 * $request->quantity[$i];
            }
            else if($unit->u_name == 'Liter' && $request->unit1[$i] == 'Liter')
            {
                $final = 1 * $request->quantity[$i] ;
            }
            else if($unit->u_name == 'Grams' && $request->unit1[$i] == 'Kilograms')
            {
                $final = 1 * $request->quantity[$i];
            }
            else if($unit->u_name == 'Kilograms' && $request->unit1[$i] == 'Kilograms')
            {
                $final = 1 * $request->quantity[$i] ;
            }
            else
            {
                $final =$request->quantity[$i] * 1;
            }
            FinishProducts::create([
                'id' => $p->id,
                'p_id' => $request->p_id[$i],
                'quantity' => $final,
                'type' => $request->type[$i]
            ]);
        }
           $hcat = HeadCategory::where('name','Inventory')->first();

            $account = AccountDetails::where('c_id',$hcat->id)
            ->latest('created_at')->first();

            if($account == null)
            {
                $id = 001;
            }
            else
            {
                $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
            }

            $str_length = strlen((string)$id)+2;
            $id = substr("0000{$id}", -$str_length);
            $code = $hcat->code.'-'.$id;
            $data1 = [
                'Code' => $code,
                'name_of_account' => $request->pro_code.' - '.$request->pro_name,
                'c_id' => $hcat->id,
                'created_by' => $u_id,
                'type' => 0
            ];
            AccountDetails::create($data1);

        $u_name = Auth::user()->name;
        $user = User::where('r_id',env('ADMIN_ID'))->get();
        $data2 = [
            'notification' => 'New Finished product has been added by '.$u_name,
            'link' => url('').'/product/finishProducts',
            'name' => 'View Finish Products',
        ];
        Notification::send($user, new AddNotification($data2));
        toastr()->success('Final Product added successfully!');
        return redirect()->back();
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'pro_code'      =>  'required|string|max:255|unique:products'
        ]);

        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $status = 1;
            $vstatus = 1;
        }
        else
        {
            $status = 0;
            $vstatus = 0;
        }
        if($request->variants[1][0] != null)
        {
            $vstatusp=1;
        }
        else
        {
            $vstatusp=0;
        }
        // dd($vstatusp);
        $u_id = Auth::user()->id;

        $unit = Unit::where('u_name' , $request->unit_id)
        ->first();
        // Rule::uniuqe('products')->where()
        $pro=Products::where('pro_name',$request->pro_name)
        ->where('brand_id',$request->brand_id)
        ->where('pro_code',$request->pro_code)
        ->where('pro_code',$request->pro_code)
        ->where('s_cat_id',$request->s_cat_id)
        ->where('cat_id',$request->cat_id)
        ->first();
        // dd(count($request->variants));
        if($pro == null)
        {
            if(isset($request->image))
            {
                $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
            }
            $data=([
                's_cat_id' => $request->s_cat_id,
                'pro_name' => $request->pro_name,
                'pro_code' => $request->pro_code,
                'weight' => $request->weight,
                'unit_id' => $unit->id,
                'cost' => $request->cost,
                'price' => $request->price,
                // 'quantity' => $request->quantity,
                'alert_quantity' => $request->alert_quantity,
                'brand_id' => $request->brand_id,
                'cat_id' => $request->cat_id,
                // 'w_id' => $request->w_id,
                'visibility' => $request->visibility,
                'description' => $request->description,
                'p_type' => $request->p_type,
                'created_by' => $u_id,
                'status' => $status,
                'vstatus' => $vstatusp,
                'mrp' => $request->mrp,
                'company_name' => $request->company_name,
                'case' => $request->case,
            ]);
            $p=Products::create($data);
            if(isset($request->request_type) && $request->request_type == "ajax")
            {
                for ($i=0; $i < count($request->s_id) ; $i++) {
                    ProductSupplier::create([
                        'p_id' => $p->id,
                        'type' => 0,
                        's_id' => $request->s_id[$i],
                    ]);
                }
                $hcat = HeadCategory::where('name','Inventory')->first();

                $account = AccountDetails::where('c_id',$hcat->id)
                ->latest('created_at')->orderBy('id','desc')->first();

                if($account == null)
                {
                    $id = 001;
                }
                else
                {
                    $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                }

                $str_length = strlen((string)$id)+2;
                $id = substr("0000{$id}", -$str_length);
                $code = $hcat->code.'-'.$id;
                // dd($accoun);
                $data1 = [
                    'Code' => $code,
                    'name_of_account' => $request->pro_code.' - '.$request->pro_name,
                    'c_id' => $hcat->id,
                    'created_by' => $u_id,
                    'type' => 0
                ];
                AccountDetails::create($data1);
                $response['message'] = 'Product added successfully!';

                $response['p'] = $p;
                return response()->json($response, 200);
            }

            $u_name = Auth::user()->name;
            $user   = User::where('r_id',env('ADMIN_ID'))->get();
            $data2  = [
                'notification' => 'New '.$request->p_type.' product has been added by '.$u_name,
                'link' => url('').'/product',
                'name' => 'View Products',
            ];


            if($request->variants[1][0] != null)
            {
                for ($i=1; $i <= count($request->variants) ; $i++) {
                    $name = $request->pro_code.' - '.$request->pro_name.'-';

                    for ($j=0; $j < count($request->variants[$i]) ; $j++) {
                        if($j == count($request->variants[$i] ) - 1){
                            $name .=$request->variants[$i][$j];
                        }else {
                            $name .=$request->variants[$i][$j].'-';
                        }
                    }
                    // dd($name);

                    $pv = ProductVariants::create([
                        'p_id' => $p->id,
                        'name' => $name,
                        'status' => $vstatus,
                        'cost' => $request->costv[$i] == null ? $request->cost : $request->costv[$i]  ,
                        'price' => $request->pricev[$i] == null ? $request->price : $request->pricev[$i],
                        'mrp' => $request->mrpV[$i] == null ? $request->mrp : $request->mrpV[$i],
                    ]);
                    for ($k=0; $k < count($request->s_id) ; $k++) {
                        ProductSupplier::create([
                            'p_id' => $pv->id,
                            's_id' => $request->s_id[$k],
                            'type' => 1,
                        ]);
                    }
                    $hcat = HeadCategory::where('name','Inventory')->first();

                    $account = AccountDetails::where('c_id',$hcat->id)
                    ->latest('created_at')->first();

                    if($account == null)
                    {
                        $id = 001;
                    }
                    else
                    {
                        $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                    }
                    // dd($id);
                    $str_length = strlen((string)$id)+2;
                    $id = substr("0000{$id}", -$str_length);
                    $code = $hcat->code.'-'.$id;

                    $data1 = [
                        'Code' => $code,
                        'name_of_account' => $name,
                        'c_id' => $hcat->id,
                        'created_by' => $u_id,
                        'type' => 1
                    ];
                    AccountDetails::create($data1);
                }
                // dd($id);
            }
            else
            {
                for ($i=0; $i < count($request->s_id) ; $i++) {
                    ProductSupplier::create([
                        'p_id' => $p->id,
                        'type' => 0,
                        's_id' => $request->s_id[$i],
                    ]);
                }
                $hcat = HeadCategory::where('name','Inventory')->first();

                $account = AccountDetails::where('c_id',$hcat->id)
                ->latest('created_at')->orderBy('id','desc')->first();

                if($account == null)
                {
                    $id = 001;
                }
                else
                {
                    $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                }

                $str_length = strlen((string)$id)+2;
                $id = substr("0000{$id}", -$str_length);
                $code = $hcat->code.'-'.$id;
                // dd($accoun);
                $data1 = [
                    'Code' => $code,
                    'name_of_account' => $request->pro_code.' - '.$request->pro_name,
                    'c_id' => $hcat->id,
                    'created_by' => $u_id,
                    'type' => 0
                ];
                AccountDetails::create($data1);
            }
            // dd($id);
            Notification::send($user, new AddNotification($data2));
            toastr()->success('Product added successfully!');
            return redirect()->back();
        }
        else
        {
            if(isset($request->request_type) && $request->request_type == "ajax")
            {
                $response['message'] = 'Product already exist!';
                $p = null;
                return response()->json($response, 200);
            }
            toastr()->error('Product already exist!');
            return redirect()->back();
        }
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');
        $u_id = Auth::user()->id;
        $item = Products::find($id);
        if ($item->update(['status' => $status])) {
            Products::where('id',$id)
            ->update([
                'status' => $status,
                'updated_by' =>$u_id
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }


    public function Vstatus(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');
        $item = ProductVariants::find($id);
        if ($item->update(['status' => $status])) {
            ProductVariants::where('id',$id)
            ->update([
                'status' => $status
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Products::with(['brands','unit','category','subcategory','finish'])
        ->where('id',$id)
        ->first();
        return $product;
    }
    public function stockShow($id)
    {
        if(request()->ajax())
        {
            $p_id = substr($id, 0, strpos($id, '-'));
            $type = substr($id, strpos($id, '-')+1);
            $idntype = [];
            $qty = [];
            if($type == 0)
            {
                $product = Products::with(['brands','unit','category','subcategory','finish'])
                ->where('id',$id)
                ->first();
                if($product->finish->isNotEmpty())
                {
                    foreach ($product->finish as $key => $f) {
                        if($f->type == 1)
                        {
                            $punit = ProductVariants::with('product.unit')
                            ->where('id',$f->p_id)
                            ->first();
                            if($punit->product->unit->u_name == 'Liter' || $punit->product->unit->u_name == 'Mililiter' || $punit->product->unit->u_name == 'Grams' || $punit->product->unit->u_name == 'Kilograms')
                            {
                                array_push($idntype, array($f->p_id, $f->type));
                            }
                        }
                        else
                        {
                            $punit = Products::with('unit')->where('id',$f->p_id)->first();
                            if($punit->unit->u_name == 'Liter' || $punit->unit->u_name == 'Mililiter' || $punit->unit->u_name == 'Grams' || $punit->unit->u_name == 'Kilograms')
                            {
                                array_push($idntype, array($f->p_id, $f->type));
                            }
                        }
                    }
                    if(count($idntype) > 0)
                    {

                        foreach ($idntype as $key => $value) {
                            if($value[1] == 1)
                            {
                                $qty = StockReceiving::with(['warehouse','variant','unit'])->where('p_id',$value[0])
                                ->where('type',$value[1])->get();
                            }
                            else {
                                $qty = StockReceiving::with(['warehouse','products','unit'])->where('p_id',$value[0])
                                ->where('type',$value[1])->get();
                            }
                        }
                    }
                    else
                    {
                        $qty = [];
                    }

                }
                else {
                    $qty = [];
                }
                return [$product,$type,$qty];
            }
            else
            {
                $product = ProductVariants::with(['product.brands','product.unit','product.category','product.subcategory'])
                ->where('id',$id)
                ->first();
                return [$product,$type];
            }

        }

    }

    public function sales($id)
    {
        $p_id = substr($id, 0, strpos($id, '.'));
        $w_id = substr($id, strpos($id, '.')+3);
        $prod = Products::with(['unit'])->where('id',$p_id)->first();
        $type = $prod->vstatus;
        if($prod->unit->u_name == 'Liter' || $prod->unit->u_name == 'Mililiter' || $prod->unit->u_name =='Kilograms' || $prod->unit->u_name == 'Grams')
        {
            if($prod->vstatus == 0)
            {
                $product = Products::with(['brands','unit','category','subcategory'])
                ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
                    $avg = $query->where('w_id',$w_id)->where('type',$type)->select(DB::raw('sum(unit_quantity)'));
                }])
                ->where('id',$p_id)
                ->first();
            }
            else {
                $product = Products::with(['brands','unit','category','subcategory','variants'])
                // ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id) {
                //     $avg = $query->where('w_id',$w_id)->select(DB::raw('sum(unit_quantity)'));
                // }])
                ->where('id',$p_id)
                ->first();
            }
        }
        else
        {
            if ($prod->vstatus == 0) {
                $product = Products::with(['brands','unit','category','subcategory'])
                ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
                    $avg = $query->select(DB::raw('sum(quantity)'))->where('w_id',$w_id)->where('type',$type);
                }])
                ->where('id',$p_id)
                ->first();
            }
            else {
                $product = Products::with(['brands','unit','category','subcategory','variants'])
                // ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id) {
                //     $avg = $query->select(DB::raw('quantity'))->where('w_id',$w_id);
                // }])
                ->where('id',$p_id)
                ->first();
            }
        }
        // dd($product);
        return $product;
    }

    public function salesProduct($id) //use to fetch product data in sales create
    {
        // dd($id);
        $p_id = substr($id, 0, strpos($id, '.'));
        $type = substr($id, strpos($id, '.')+1);
        $type = substr($type,0, strpos($type, '.'));


        $w_id = substr($id,  strpos($id, '.', strpos($id, '.')+1)+1);
        // dd($type,$w_id,$id,$p_id);
        if($type == 1)
        {
            // dd('1');
            $prod = ProductVariants::find($p_id);
            $product = ProductVariants::with(['product.brands','product.unit','product.category','product.subcategory'])
            ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
                $avg = $query->where('w_id',$w_id)->where('type',$type)->select(DB::raw('sum(quantity)'));
            }])
            ->where('id',$p_id)
            ->first();
        }
        else
        {
            // dd('0');
            $prod = Products::with(['unit'])->where('id',$p_id)->first();
            if($prod->unit->u_name == 'Liter' || $prod->unit->u_name == 'Mililiter' || $prod->unit->u_name =='Kilograms' || $prod->unit->u_name == 'Grams')
            {
                // dd('y');
                $product = Products::with(['brands','unit','category','subcategory'])
                ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
                    $avg = $query->where('w_id',$w_id)->where('type',$type)->select(DB::raw('sum(quantity)'));
                }])
                ->where('id',$p_id)
                ->first();
            }
            else
            {
                // dd('n');

                $product = Products::with(['brands','unit','category','subcategory'])
                ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
                    $avg = $query->select(DB::raw('sum(quantity)'))->where('w_id',$w_id)->where('type',$type);
                }])
                ->where('id',$p_id)
                ->first();
                // dd($product);

            }
        }
        // dd($product);

        return [$product,$type];
    }

    public function addSingleProduct($id)//creating finish product it shows data of single product
    {
        if(request()->ajax())
        {
            $product = Products::with(['brands','unit','variants'])
            ->where('id',$id)->first();
            return $product;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $variant=Variants::all();
        $supplier = Vendors::where('v_type','Supplier')
        ->where('status',1)->get();
        $p_variant=ProductVariants::where('p_id',$id)->get();
        $product = Products::find($id);
        $ps = ProductSupplier::where('p_id',$id)->get();
        // dd($p_variant);
        $data=[
            'isEdit' => true,
            'product' => $product,
            'unit' => $unit,
            'cat' => $cat,
            'sub' => $sub,
            'brands' => $brands,
            'variant' => $variant,
            'p_variant' => $p_variant,
            'supplier' => $supplier,
            'ps' => $ps
        ];
        return view('product.create',$data);
    }

    public function variants($id)
    {
        $variants = ProductVariants::where('p_id',$id)->get();
        return $variants;
    }

    public function Pvariants($id)
    {
        $v_id = substr($id, 0, strpos($id, '.'));
        $w_id = substr($id, strpos($id, '.')+1);
        $type = 1;
        $variants = ProductVariants::where('id',$v_id)
        ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
            $avg = $query->select(DB::raw('quantity'))->where('w_id',$w_id)->where('type',$type);
        }])
        ->with('product')
        ->where('status',1)
        ->first();
        return $variants;
    }

    public function Pvariantsforfinishproduct($id)
    {
        $variants = ProductVariants::where('id',$id)
        ->with('product.unit')
        ->first();
        return $variants;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $productid = $id;
        // dd($request->all());
        $product = Products::find($id);
        $ps = ProductSupplier::where('p_id',$id)->get();
        $var = ProductVariants::where('p_id',$id)->pluck('id')->toArray();
        $vid= [];
        if(isset($request->v_id))
        {
            for ($i=1; $i <= count($request->v_id) ; $i++)
            {
                array_push($vid,$request->v_id[$i]);
            }
        }
        // dd($vid);

        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');
        $unit = Unit::where('u_name' , $request->unit_id)
        ->first();

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            DB::table('product_supplier')->where('p_id', $productid)->delete();
            for ($i=0; $i < count($request->s_id) ; $i++) {
                ProductSupplier::create([
                    'p_id' => $productid,
                    'type' => 0,
                    's_id' => $request->s_id[$i],
                ]);
            }
            $u_id = Auth::user()->id;
            if(isset($request->image))
            {
                $data=([
                    'image' => $request->image,
                    's_cat_id' => $request->s_cat_id,
                    'pro_name' => $request->pro_name,
                    'pro_code' => $request->pro_code,
                    'weight' => $request->weight,
                    'unit_id' => $unit->id,
                    'cost' => $request->cost,
                    'price' => $request->price,
                    'alert_quantity' => $request->alert_quantity,
                    'brand_id' => $request->brand_id,
                    'cat_id' => $request->cat_id,
                    'visibility' => $request->visibility,
                    'description' => $request->description,
                    'updated_by' => $u_id,
                    'mrp' => $request->mrp,
                    'p_type' => $request->p_type,
                    'company_name' => $request->company_name,
                    'case' => $request->case,
                ]);
                $data['image']= Storage::disk('uploads')->putFile('',$request->image);
                Products::where('id',$productid)
                ->update($data);
            }
            else
            {
                $data=([
                    's_cat_id' => $request->s_cat_id,
                    'pro_name' => $request->pro_name,
                    'pro_code' => $request->pro_code,
                    'weight' => $request->weight,
                    'unit_id' => $unit->id,
                    'cost' => $request->cost,
                    'price' => $request->price,
                    // 'quantity' => $request->quantity,
                    'alert_quantity' => $request->alert_quantity,
                    'brand_id' => $request->brand_id,
                    'cat_id' => $request->cat_id,
                    // 'w_id' => $request->w_id,
                    'visibility' => $request->visibility,
                    'description' => $request->description,
                    'updated_by' => $u_id,
                    'mrp' => $request->mrp,
                    'p_type' => $request->p_type,
                    'company_name' => $request->company_name,
                    'case' => $request->case,
                ]);
                Products::where('id',$productid)
                ->update($data);

            }

            if($request->variants[1][0] != null)
            {


                for ($i=1; $i <= count($request->variants) ; $i++) {
                    if(isset($request->v_id[$i]))
                    {
                        // dd('yes');
                        for ($j=0; $j < count($var) ; $j++)/// if any variant is deleted by user then check in ledger if ledger is null delete it
                        {
                            if(in_array($var[$j],$vid))
                            {
                                $pv = ProductVariants::find($var[$j]);
                                $ledger = GeneralLedger::where('account_name',$pv->name)
                                ->first();
                                if($ledger == null)
                                {
                                    DB::table('product_variants')->where('id', $var[$j])->delete();
                                    DB::table('account_details')->where('name_of_account', $pv->name)->delete();
                                    $name = $request->pro_code.' - '.$request->pro_name.'-';
                                    for ($k=0; $k < count($request->variants[$i]) ; $k++) {
                                        if($k == count($request->variants[$i]) - 1){
                                            $name .=$request->variants[$i][$k];
                                        }else {
                                            $name .=$request->variants[$i][$k].'-';
                                        }

                                    }
                                    $pvr = ProductVariants::create([
                                        'p_id' => $productid,
                                        'name' => $name,
                                        'status' => 1,
                                        'cost' => $request->costv[$i] == null ? $request->cost : $request->costv[$i] == null ,
                                        'price' => $request->pricev[$i] == null ? $request->price : $request->pricv[$i] == null,
                                    ]);
                                    $hcat = HeadCategory::where('name','Inventory')->first();

                                    $account = AccountDetails::where('c_id',$hcat->id)
                                    ->latest('created_at')->first();

                                    if($account == null)
                                    {
                                        $id = 001;
                                    }
                                    else
                                    {
                                        $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                                    }

                                    $str_length = strlen((string)$id)+2;
                                    $id = substr("0000{$id}", -$str_length);
                                    $code = $hcat->code.'-'.$id;
                                    $data1 = [
                                        'Code' => $code,
                                        'name_of_account' => $name,
                                        'c_id' => $hcat->id,
                                        'created_by' => $u_id,
                                        'type' => 1
                                    ];
                                    AccountDetails::create($data1);
                                    continue;
                                }
                                else {
                                    # code...
                                }
                            }
                            else {
                                $pv = ProductVariants::find($var[$j]);
                                $ledger = GeneralLedger::where('account_name',$pv->name)
                                ->first();
                                if($ledger == null)
                                {
                                    DB::table('product_variants')->where('id', $var[$j])->delete();
                                    DB::table('account_details')->where('name_of_account', $pv->name)->delete();
                                }
                                else {
                                    # code...
                                }

                            }
                        }
                    }
                    else {
                        // dd($productid);
                        Products::where('id',$productid)->update(['vstatus'=>1]);
                        $name = $request->pro_code.' - '.$request->pro_name.'-';
                        for ($j=0; $j < count($request->variants[$i]) ; $j++) {
                            if($j == count($request->variants[$i]) - 1){
                                $name .=$request->variants[$i][$j];
                            }else {
                                $name .=$request->variants[$i][$j].'-';
                            }

                        }
                        $pvar = new ProductVariants;
                        $pvar->p_id = $productid;
                        $pvar->name = $name;
                        $pvar->status = 1;
                        $pvar->cost = $request->costv[$i] == null ? $request->cost : $request->costv[$i] == null;
                        $pvar->price = $request->pricev[$i] == null ? $request->price : $request->pricv[$i] == null;
                        $pvar->save();


                        $hcat = HeadCategory::where('name','Inventory')->first();

                        $account = AccountDetails::where('c_id',$hcat->id)
                        ->latest('created_at')->first();

                        if($account == null)
                        {
                            $id = 001;
                        }
                        else
                        {
                            $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                        }

                        $str_length = strlen((string)$id)+2;
                        $id = substr("0000{$id}", -$str_length);
                        $code = $hcat->code.'-'.$id;
                        $data1 = [
                            'Code' => $code,
                            'name_of_account' => $name,
                            'c_id' => $hcat->id,
                            'created_by' => $u_id,
                            'type' => 1
                        ];
                        AccountDetails::create($data1);

                    }
                }
            }
            else
            {

                if(count($var)<=0)
                {
                }
                else
                {
                    foreach ($var as $key => $va) {
                        $ledger = GeneralLedger::where('account_name',$va->name)
                        ->first();
                        if($ledger == null)
                        {
                            DB::table('product_variants')->where('id', $va->id)->delete();
                        }
                    }
                    $vars = ProductVariants::where('p_id',$productid)->pluck('id')->toArray();
                    if (count($vars) > 0) {
                        Products::where('id',$productid)->update([
                            'vstatus' => 1
                        ]);
                    } else {
                        Products::where('id',$productid)->update([
                            'vstatus' => 0
                        ]);
                    }


                }
            }

            toastr()->success('Product updated successfully!');
            return redirect(url('').'/product');

        }
        else
        {
            for ($i=0; $i < count($request->s_id) ; $i++) {
                ProductSupplierUpdate::create([
                    'p_id' => $productid,
                    'type' => 0,
                    's_id' => $request->s_id[$i],
                ]);
            }
            if(isset($request->image))
            {
                $data=([
                    'id' => $productid,
                    'pro_name' => $request->pro_name,
                    'pro_code' => $request->pro_code,
                    'weight' => $request->weight,
                    'cost' => $request->cost,
                    'price' => $request->price,
                    'alert_quantity' => $request->alert_quantity,
                    'unit_id' => $unit->id,
                    'brand_id' => $request->brand_id,
                    'cat_id' => $request->cat_id,
                    's_cat_id' => $request->s_cat_id,
                    'image' => $request->image,
                    'description' => $request->description,
                    'visibility' => $request->visibility,
                    'p_type' => $request->p_type,
                    'mrp' => $request->mrp,
                    'company_name' => $request->company_name,
                    'case' => $request->case,
                ]);
                $data['image']= Storage::disk('uploads')->putFile('',$request->image);
            }
            else
            {
                $data=([
                    'id' => $productid,
                    'pro_name' => $request->pro_name,
                    'pro_code' => $request->pro_code,
                    'weight' => $request->weight,
                    'cost' => $request->cost,
                    'price' => $request->price,
                    'alert_quantity' => $request->alert_quantity,
                    'unit_id' => $unit->id,
                    'brand_id' => $request->brand_id,
                    'cat_id' => $request->cat_id,
                    's_cat_id' => $request->s_cat_id,
                    'image' => $request->image,
                    'description' => $request->description,
                    'visibility' => $request->visibility,
                    'p_type' => $request->p_type,
                    'mrp' => $request->mrp,
                    'company_name' => $request->company_name,
                    'case' => $request->case,
                ]);
            }
            ProductsUpdateRequest::create($data);
            if($request->variants[1][0] != null)
            {
                for ($i=1; $i <= count($request->variants) ; $i++) {
                    if(isset($request->v_id[$i]))
                    {
                        for ($j=0; $j < count($var) ; $j++)/// if any variant is deleted by user then check in ledger if ledger is null delete it
                        {
                            if(in_array($var[$j],$vid))
                            {
                                $pv = ProductVariants::find($var[$j]);
                                $ledger = GeneralLedger::where('account_name',$pv->name)
                                ->first();
                                if($ledger == null)
                                {
                                    DB::table('product_variants')->where('id', $var[$j])->delete();
                                    DB::table('account_details')->where('name_of_account', $pv->name)->delete();
                                    $name = $request->pro_code.' - '.$request->pro_name.'-';
                                    for ($k=0; $k < count($request->variants[$i]) ; $k++) {
                                        if($k == count($request->variants[$i]) - 1){
                                            $name .=$request->variants[$i][$k];
                                        }else {
                                            $name .=$request->variants[$i][$k].'-';
                                        }

                                    }
                                    ProductVariantUpdate::create([
                                        'p_id' => $productid,
                                        'name' => $name,
                                        'cost' => $request->costv[$i] == null ? $request->cost : $request->costv[$i] == null ,
                                        'price' => $request->pricev[$i] == null ? $request->price : $request->pricv[$i] == null,
                                    ]);
                                    continue;
                                }
                                else {
                                    # code...
                                }
                            }
                            else {
                                $pv = ProductVariants::find($var[$j]);
                                $ledger = GeneralLedger::where('account_name',$pv->name)
                                ->first();
                                if($ledger == null)
                                {
                                    DB::table('product_variants')->where('id', $var[$j])->delete();
                                    DB::table('account_details')->where('name_of_account', $pv->name)->delete();
                                }
                                else {
                                    # code...
                                }

                            }
                        }
                    }
                    else {
                        $name = $request->pro_code.' - '.$request->pro_name.'-';
                        for ($j=0; $j < count($request->variants[$i]) ; $j++) {
                            if($j == count($request->variants[$i]) - 1){
                                $name .=$request->variants[$i][$j];
                            }else {
                                $name .=$request->variants[$i][$j].'-';
                            }

                        }
                        $pvr=ProductVariantUpdate::create([
                            'p_id' => $productid,
                            'name' => $name,
                            'cost' => $request->costv[$i] == null ? $request->cost : $request->costv[$i] == null ,
                            'price' => $request->pricev[$i] == null ? $request->price : $request->pricv[$i] == null,
                            'mrp' => $request->mrpV[$i] == null ? $request->mrp : $request->mrpV[$i],
                        ]);

                    }
                }
            }
            else
            {
                if(count($var)<=0)
                {
                }
                else
                {
                    foreach ($var as $key => $va) {
                        $ledger = GeneralLedger::where('account_name',$va->name)
                        ->first();
                        if($ledger == null)
                        {
                            DB::table('product_variants')->where('id', $va->id)->delete();
                        }
                    }

                }
            }


            toastr()->success('Product update request sent successfully successfully!');
            return redirect(url('').'/product');

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function excel()
    {
        return Excel::download(new ProductsExport, 'Products.xlsx');
    }

    public function Finishexcel()
    {
        return Excel::download(new FinishProductExport, 'FinishProducts.xlsx');
    }
}
