<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Variants;

class VariantsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $variant=Variants::with('parent')->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return  view('variants.index',compact('variant','permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $variant = Variants::all();
        return view('variants.create',compact('variant'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'p_id' => 'required',
        ]);
        Variants::create($data);
        toastr()->success('Variant added successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Variants $variant)
    {
        return $variant;
    }
}
