<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Area;
use App\User;
use DataTables;
use Auth;
use DB;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class AreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('area.index',compact('permissions'));
    }

    public function datatable()
    {
        $area=Area::withCount(['customer as total' => function($query) {
            $avg = $query->where('v_type','Customer')->select(DB::raw('count(id)'));
        }])
        ->get();
        return DataTables::of($area)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false
        ];
        return view('area.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'      =>  'required|string|max:255|unique:area'
        ]);
        $u_id = Auth::user()->id;
        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');
        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'New arae has been added by '.$u_name,
            'link' => url('').'/area',
            'name' => 'View Areas',
        ];
        Notification::send($user, new AddNotification($data1));
        Area::create($data);
        toastr()->success('Area added successfully!');
        return redirect()->back();
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Area $area)
    {
        $data=[
            'isEdit' => true,
            'area' =>$area
        ];
        return view('area.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Area $area)
    {
        $data = $request->validate([
            'name'      =>  'required|string|max:255|unique:area,name,'.$area->id
        ]);
        $area->update($data);
        toastr()->success('Area Updated successfully!');
        return redirect(url('').'/area');
    }
}
