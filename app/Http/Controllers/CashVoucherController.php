<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Voucher;
use App\AccountDetails;
use App\GeneralLedger;
use App\User;
use DataTables;
use Auth;
use DB;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;
use Carbon\Carbon;


class CashVoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $account = AccountDetails::with(['headCategory'])->get();
        return view('cashvoucher.index',compact('account'));
    }

    public function datatable()
    {
        $cv = Voucher::with(['createdUser'])->where('v_type','Cash')->get();
        return DataTables::of($cv)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cash=Voucher::max('id');
        if($cash == null)
        {
            $id=1;
        }
        else
        {
            $id=$cash+1;
        }
        $data =[
            'account'    => AccountDetails::with(['headCategory'])->get(),
            'isEdit'  => false,
            'id'      => $id,
            'cby'     => Auth::user()->name,
            'date'    => Carbon::now()->format("Y-m-d"),
            'u_id'     => Auth::user()->id,
        ];

        return view('cashvoucher.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'accounting_date'      =>  'required',
            'posted_date'          =>  'required',
            'period'               =>  'required',
            'type'                 =>  'required',
            'account1'             =>  'required',
            'account2'             =>  'required',
            'description'          =>  'required',
            'debit'                =>  'required',
        ]);
        $data['account1Code'] = AccountDetails::where('name_of_account',$request->account1)->pluck('Code')->first();
        $data['account2Code'] = AccountDetails::where('name_of_account',$request->account2)->pluck('Code')->first();
        $data['credit']       = $data['debit'];
        $data['v_type']       = $request->v_type;
        $data['created_by']   = $request->created_by;
        Voucher::create($data);


        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'New Cash voucher has been added by '.$u_name,
            'link' => url('').'/cashVoucher',
            'name' => 'View Cash Vouchers',
        ];
        Notification::send($user, new AddNotification($data1));
        toastr()->success('Cash Voucher added successfully!');
        return redirect(url('').'/cashVoucher');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $voucher = Voucher::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=[
            'isEdit' => true,
            'cash'   =>   Voucher::find($id),
            'account'    => AccountDetails::with(['headCategory'])->get(),
        ];
        return view('cashvoucher.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'accounting_date'      =>  'required',
            'posted_date'          =>  'required',
            'period'               =>  'required',
            'type'                 =>  'required',
            'account1'             =>  'required',
            'account2'             =>  'required',
            'description'          =>  'required',
            'debit'                =>  'required',
        ]);
        $data['account1Code'] = AccountDetails::where('name_of_account',$request->account1)->pluck('Code')->first();
        $data['account2Code'] = AccountDetails::where('name_of_account',$request->account2)->pluck('Code')->first();
        $data['credit']       = $data['debit'];

        Voucher::where('id',$id)->update($data);


        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'Cash voucher has been updated by '.$u_name,
            'link' => url('').'/cashVoucher',
            'name' => 'View Cash Vouchers',
        ];
        Notification::send($user, new AddNotification($data1));
        toastr()->success('Cash Voucher added successfully!');
        return redirect(url('').'/cashVoucher');
    }

    public function status(Request $request)
    {
        $id     = $request->id;
        $status = $request->status;
        $cash = Voucher::find($id);
        $u_id = Auth::user()->id;
        $id = GeneralLedger::max('id');
        $ledger = GeneralLedger::where('id',$id)->first();
        if($ledger == null)
        {
            $link_id=1;
        }
        else
        {
            $link_id = $ledger->link_id + 1;
        }

        if ($cash->update(['status' => $status])) {
            $cash->update([
                'status' => $status,
            ]);
            if($cash->type == "Cash To Bank")
            {
                $account_cash = AccountDetails::where('name_of_account',$cash->account2)->first();
                $cashL = GeneralLedger::where('account_code',$account_cash->Code)
                ->get();
                $account_b = AccountDetails::where('name_of_account',$cash->account1)->first();
                $bank_l = GeneralLedger::where('account_code',$account_b->Code)
                ->get();
                if($cashL->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid to bank '.$cash->account1,
                        'account_name' => $account_cash->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $cash->accounting_date,
                        'posted_date' => $cash->posted_date,
                        'period' => $cash->period,
                        'account_code' => $account_cash->Code,
                        // 'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'credit' => $cash->credit,
                        'debit' => '0',
                        'net_value' => 0 - $cash->credit ,
                        'balance' => 0 - $cash->credit,
                    ]);
                }
                else
                {
                    $balance = 0;
                    foreach ($cashL as $key => $c) {
                        $balance+=$c->net_value;
                    }
                    $cash_n = 0;
                    $cash_n = $balance + (0 - $cash->credit );

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid to bank '.$cash->account1,
                        'account_name' => $account_cash->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $cash->accounting_date,
                        'posted_date' => $cash->posted_date,
                        'period' => $cash->period,
                        'account_code' => $account_cash->Code,
                        // 'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'credit' => $cash->credit,
                        'debit' => '0',
                        'net_value' => 0 - $cash->credit ,
                        'balance' => $cash_n
                    ]);
                }

                if($bank_l->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid By: '.$cash->account2,
                        'account_name' => $account_b->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $cash->accounting_date,
                        'posted_date' => $cash->posted_date,
                        'period' => $cash->period,
                        'account_code' => $account_b->Code,
                        // 'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'debit' => $cash->debit,
                        'credit' => '0',
                        'net_value' => $cash->debit - 0,
                        'balance' => $cash->debit - 0
                    ]);
                }
                else
                {
                    $balance = 0;
                    foreach ($bank_l as $key => $c) {
                        $balance+=$c->net_value;
                    }
                    $bank_n = 0;
                    $bank_n = $balance + ($cash->debit);

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid By: '.$cash->account2,
                        'account_name' => $account_b->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $cash->accounting_date,
                        'posted_date' => $cash->posted_date,
                        'period' => $cash->period,
                        'account_code' => $account_b->Code,
                        // 'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'debit' => $cash->debit,
                        'credit' => '0',
                        'net_value' => $cash->debit - 0,
                        'balance' => $bank_n
                    ]);
                }
            }
            else
            {
                $account_cash = AccountDetails::where('name_of_account',$cash->account1)->first();
                $cashL = GeneralLedger::where('account_code',$account_cash->Code)
                ->get();
                $account_b = AccountDetails::where('name_of_account',$cash->account2)->first();
                $bank_l = GeneralLedger::where('account_code',$account_b->Code)
                ->get();


                if($cashL->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid from bank'.$cash->account2,
                        'account_name' => $account_cash->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $cash->accounting_date,
                        'posted_date' => $cash->posted_date,
                        'period' => $cash->period,
                        'account_code' => $account_cash->Code,
                        // 'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'debit' => $cash->debit,
                        'credit' => '0',
                        'net_value' => $cash->debit ,
                        'balance' => $cash->debit,
                    ]);
                }
                else
                {
                    $balance = 0;
                    $cash_n = 0;
                    foreach ($cashL as $key => $c) {
                        $balance+=$c->net_value;
                    }
                    $cash_n = $balance + ($cash->debit - 0 );

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid from bank'.$cash->account2,
                        'account_name' => $account_cash->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $cash->accounting_date,
                        'posted_date' => $cash->posted_date,
                        'period' => $cash->period,
                        'account_code' => $account_cash->Code,
                        // 'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'debit' => $cash->debit,
                        'credit' => '0',
                        'net_value' => $cash->debit ,
                        'balance' => $cash_n
                    ]);
                }

                if($bank_l->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid to: '.$cash->account1,
                        'account_name' => $account_b->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $cash->accounting_date,
                        'posted_date' => $cash->posted_date,
                        'period' => $cash->period,
                        'account_code' => $account_b->Code,
                        // 'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'credit' => $cash->credit,
                        'debit' => '0',
                        'net_value' => 0 - $cash->credit ,
                        'balance' => 0 - $cash->credit
                    ]);
                }
                else
                {
                    $balance = 0;
                    foreach ($bank_l as $key => $c) {
                        $balance+=$c->net_value;
                    }
                    $bank_n = 0;
                    $bank_n = $balance + (0 - $cash->credit);

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid to: '.$cash->account1,
                        'account_name' => $account_b->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $cash->accounting_date,
                        'posted_date' => $cash->posted_date,
                        'period' => $cash->period,
                        'account_code' => $account_b->Code,
                        // 'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'credit' => $cash->credit,
                        'debit' => '0',
                        'net_value' => 0 - $cash->credit,
                        'balance' => $bank_n
                    ]);
                }
            }



            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }
}
