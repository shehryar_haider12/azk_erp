<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleCounter extends Model
{
    protected $table = 'sale_counter';
    protected $primaryKey = 'id';
    protected $fillable = [
        'u_id',
        'co_id',
        's_id',
        'date',
        'ret_id',
    ];
    public function sale()
    {
        return $this->hasOne('App\Sales','id','s_id');
    }
    public function sreturn()
    {
        return $this->hasOne('App\SaleReturn','id','ret_id');
    }
    public function counter()
    {
        return $this->hasOne('App\Counter','id','co_id');
    }
    public function user()
    {
        return $this->hasOne('App\User','id','u_id');
    }
}
