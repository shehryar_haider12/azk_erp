<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incentives extends Model
{
    protected $table = 'incentives';
    protected $primaryKey = 'id';
    protected $fillable = [
        'sp_id',
        'month',
        'amount',
        'status',
    ];

    public function saleperson()
    {
        return $this->hasOne('App\Vendors','id','sp_id');
    }

}
