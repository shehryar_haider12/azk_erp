<?php

namespace App\Exports;

use DB;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\FromArray;
use App\ExpenseVoucher;
use App\GeneralLedger;
use App\HeadCategory;
use App\AccountDetails;

class incomeStatementExport implements FromArray, WithMapping,WithEvents, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function array():array
    {
        $data = array('sales' => '1');
        return ($data);
    }


    public function map($data): array
    {
        $sales = GeneralLedger::where('account_name','Sales')->sum('credit');
        $return = GeneralLedger::where('account_name','Damages Warehouse')->sum('debit');
        $opening_stock = GeneralLedger::where('account_code','Like','CA-02%')
        ->where('account_code','NOT Like','NCA-02%')
        ->sum('debit');
        $purchase = GeneralLedger::where('account_name','Purchase')->sum('debit');
        $closing_stock_credit = GeneralLedger::where('account_code','Like','CA-02%')
        ->where('account_code','NOT Like','NCA-02%')
        ->sum('credit');
        $closing_stock = $opening_stock - $closing_stock_credit;

        $saleinc = GeneralLedger::where('account_code','Like','EXP-06%')->sum('debit');
        $transportHead = HeadCategory::where('name','Transportation')->first();
        $transportAccount = AccountDetails::where('Code','Like',$transportHead->code.'%')->get();

        $expHead = HeadCategory::where('name','!=','Transportation')
        ->where('name','!=','Sales Incentive')
        ->where('code','Like','EXP%')
        ->get();
        $transportTotal=0;

        $map = [
            [
                'INCOME STATEMENT'
            ],
            [
                'Sales:Add',
                '',
                $sales
            ],
            [
                'Returns Inward:Sub',
                $return,
                '',
            ],
            [
                'Net Sales',
                '',
                $sales - $return,
            ],
            [
                'COGS',
                '',
                ''
            ],
            [
                'Opening Inventory:Add',
                $opening_stock,
                '',
            ],
            [
                'Purchases:Add',
                $purchase,
                '',
            ],
            [
                'Closing Inventory:Sub',
                $closing_stock,
                '',
            ],
            [
                'Total COGS',
                '',
                ($opening_stock + $purchase) - $closing_stock ,
            ],
            [
                'Sales Incentive:Sub',
                '',
                $saleinc ,
            ]

        ];
         return $map;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange3 = 'A1:C1'; // All headers
                $cellRange = 'A2:C2'; // All headers
                $cellRange4 = 'A3:C3'; // All headers
                $cellRange5 = 'A4:C4'; // All headers
                $cellRange6 = 'A5:C5'; // All headers
                $cellRange1 = 'A6:C6'; // All headers
                $cellRange2 = 'A7:C7'; // All headers
                $cellRange8 = 'A8:C8'; // All headers
                $event->sheet->mergeCells('A1:C1');
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setBold(true);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getFont()->setBold(true);

                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange3)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange4)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange5)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange6)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange8)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            },
        ];
    }

}
