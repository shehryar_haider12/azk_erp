<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $table = 'quotation';
    protected $primaryKey = 'id';
    protected $fillable = [
        'date',
        'r_id',
        's_id',
        'status',
        'created_by',
        'updated_by',
        'attachment'
    ];

    public function request()
    {
        return $this->hasOne('App\PurchaseRequest','id','r_id');
    }

    public function supplier()
    {
        return $this->hasOne('App\Vendors','id','s_id');
    }

    public function qdetails()
    {
        return $this->hasMany('App\QuotationDetails','q_id','id');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }
}
