<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockReceiving extends Model
{
    protected $table = 'stock_receiving';
    protected $primaryKey = 'id';
    protected $fillable = [
        'size',
        'quantity',
        'p_id',
        'type',
        'u_id',
        'w_id',
        'total'
    ];

    public function warehouse()
    {
        return $this->hasOne('App\Warehouse','id','w_id');
    }
    public function products()
    {
        return $this->hasOne('App\Products','id','p_id');
    }
    public function variant()
    {
        return $this->hasOne('App\ProductVariants','id','p_id');
    }
    public function unit()
    {
        return $this->hasOne('App\Unit','id','u_id');
    }
}
