<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    protected $table = 'voucher';
    protected $primaryKey = 'id';
    protected $fillable = [
        'account1',
        'account1Code',
        'account2',
        'account2Code',
        'debit',
        'credit',
        'description',
        'accounting_date',
        'posted_date',
        'period',
        'status',
        'type',
        'v_type',
        'created_by',
    ];

    public function createdUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }
}
