<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Products;
use App\User;
use DB;
use Auth;
use App\Notifications\StockQuantityNotification;
use Illuminate\Support\Facades\Notification;

class StockCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'stock:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'check each product stock in warehouse ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $product = DB::table('current_stock')
        ->join('products','current_stock.p_id','products.id')
        ->join('warehouse','current_stock.w_id','warehouse.id')
        ->where('type',0)
        ->get();
        foreach ($product as $key => $p) {
            if($p->alert_quantity == $p->quantity)
            {
                $user = User::where('r_id',env('ADMIN_ID'))->get();
                $data = [
                    'notification' => $p->pro_name.' product stock has comes to an end.',
                    'link' => '/stocks/current',
                    'name' => 'View Current Stock',
                ];
                Notification::send($user, new StockQuantityNotification($data));
            }
        }
    }
}
