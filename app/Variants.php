<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variants extends Model
{
    protected $table = 'variants';
    protected $primaryKey = 'id';
    protected $fillable = [
        'p_id',
        'name',
    ];

    public function children()
    {
        return $this->hasMany('App\Variants','p_id','id');
    }
    public function parent()
    {
        return $this->hasOne('App\Variants','id','p_id');
    }
}
