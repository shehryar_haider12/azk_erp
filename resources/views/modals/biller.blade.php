

<div id="billerModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #5F9EA0">
                <h4 class="modal-title">Accountant</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Accountant Name<span class="text-danger">*</span></label>
                                <input class="form-control " id="nameb" type="text" placeholder="Enter Biller Name" name="name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >City<span class="text-danger">*</span></label>
                                <select required id="city_idb" class="form-control js-example-basic-single c_id" name="c_id" >
                                    <option value="" selected>Select...</option>
                                        @foreach ($city as $s)
                                        <option value="{{$s->id}}">{{$s->c_name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <input type="hidden" id="v_typeb" name="v_type" value="Biller" id="">
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Company Name<span class="text-danger">*</span></label>
                                <input class="form-control " id="companyb" type="text" placeholder="Enter Company Name" name="company" required>
                                <span class="text-danger">{{$errors->first('company') ? 'Company already exist' : null}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Address<span class="text-danger">*</span></label>
                                <input  class="form-control " id="addressb" type="text" placeholder="Enter Address" name="address" required>
                                <span class="text-danger">{{$errors->first('address') ? 'Address already exist' : null}}</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact No<span class="text-danger">*</span></label>
                                <input  class="form-control " id="c_nob" type="text" placeholder="Enter Contact Number" name="c_no" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Country <small>(optional)</small></label>
                                <input class="form-control " id="countryb" type="text" placeholder="Enter Country" name="country" >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >NTN <small>(optional)</small></label>
                                <input class="form-control " id="VATb" type="text" placeholder="Enter NTN Number" name="VAT" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >GST <small>(optional)</small></label>
                                <input class="form-control " id="GSTb" type="text" placeholder="Enter GST Number" name="GST" >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >State <small>(optional)</small></label>
                                <input class="form-control " id="stateb" type="text" placeholder="Enter State" name="state" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Email <small>(optional)</small></label>
                                <input class="form-control " id="emailb" type="email" placeholder="Enter Email" name="email" >
                                <span class="text-danger">{{$errors->first('email') ? 'email already exist' : null}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Postal Code <small>(optional)</small></label>
                                <input class="form-control " id="postalCodeb" type="text" placeholder="Enter Postal Code" name="postalCode" >
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="request_typeb" name="request_type" value="ajax">
                    <br>
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="submit" id="submitBiller" class="btn btn-success">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
    $('#submitBiller').click(function(e){
        e.preventDefault();
        var name = $('#nameb').val();
        var c_id = $('#city_idb').val();
        var company = $('#companyb').val();
        var address = $('#addressb').val();
        var c_no = $('#c_nob').val();
        var country = $('#countryb').val();
        var VAT = $('#VATb').val();
        var GST = $('#GSTb').val();
        var state = $('#stateb').val();
        var email = $('#emailb').val();
        var postalCode = $('#postalCodeb').val();
        var v_type = $('#v_typeb').val();
        var request_type = $('#request_typeb').val();
        if(name == '' || company == '' || address == '' || c_no == '' )
        {
            alert('Fill all the required fields');
        }
        else
        {

            axios
            .post('{{route("biller.store")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    name:name,
                    c_id:c_id,
                    company:company,
                    address:address,
                    c_no:c_no,
                    country:country,
                    VAT:VAT,
                    GST:GST,
                    state:state,
                    email:email,
                    postalCode:postalCode,
                    v_type:v_type,
                    request_type:request_type,
                })
                .then(function (responsive) {
                    $('#billerModal').modal('hide');
                    $('#b_id').append(`<option selected value="`+responsive.data.biller.id+`">`+responsive.data.biller.name+`</option>`);
                })
                .catch(function (error) {
                    alert(error);
            });
        }
    });
</script>
