

<div id="bankModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #5F9EA0">
                <h4 class="modal-title">Bank</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <form class="form-horizontal"  >
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Bank Name<span class="text-danger">*</span></label>
                                <input value="{{old('name')}}" class="form-control name" type="text" placeholder="Enter Name" name="name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >City <small>(optional)</small></label>
                                <select id="c_id" class="form-control js-example-basic-single c_id" name="c_id" >
                                    <option value="" disabled selected>Select...</option>
                                        @foreach ($city as $s)
                                        <option value="{{$s->id}}">{{$s->c_name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Address<span class="text-danger">*</span></label>
                                <input value="{{old('address')}}" class="form-control address" type="text" placeholder="Enter Address" name="address" required>
                                <span class="text-danger">{{$errors->first('address') ? 'Address already exist' : null}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact No <small>(optional)</small></label>
                                <input min="0" value="{{old('c_no')}}" class="form-control c_no" type="text" placeholder="Enter Contact Number" name="c_no" >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Branch<span class="text-danger">*</span></label>
                                <input value="{{ old('branch')}}" required class="form-control branch" type="text" placeholder="Enter Branch" name="branch" >
                            </div>
                        </div>

                    </div>
                    <input type="hidden" class="request_type" name="request_type" value="ajax">
                    <br>
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="submit" id="submitBank" class="btn btn-success">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<script>
    $('#submitCategory').click(function(e){
        e.preventDefault();
        var name = $('.name').val();
        var c_id = $('.c_id').val();
        var address = $('.address').val();
        var c_no = $('.c_no').val();
        var branch = $('.branch').val();
        var request_type = $('.request_type').val();
        if(name == '' || address =='' || branch = '')
        {
            alert('Fill all the required fields');
        }
        else
        {
            axios
            .post('{{route("bank.store")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    name:name,
                    c_id:c_id,
                    address:address,
                    c_no:c_no,
                    branch:branch,
                    request_type:request_type,
                })
                .then(function (responsive) {
                    $('#bankModal').modal('hide');
                    $('#bank_id').append(`<option selected value="`+responsive.data.bank.id+`">`+responsive.data.bank.name+`</option>`);
                })
                .catch(function (error) {
                    alert(error);
            });
        }
    });
</script>
