

<div id="supplierModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #5F9EA0">
                <h4 class="modal-title">Supplier</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <form class="form-horizontal" >
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Supplier Name<span class="text-danger">*</span></label>
                                <input class="form-control name" type="text" placeholder="Enter Supplier Name" name="name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >City<span class="text-danger">*</span></label>
                                <select required id="c_id" class="form-control js-example-basic-single c_id" name="c_id" >
                                    <option value="" disabled selected>Select...</option>
                                        @foreach ($city as $s)
                                        <option value="{{$s->id}}">{{$s->c_name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        {{-- <div class="col-sm-1">
                            <div class="form-control" style="margin-top: 24px; margin-left:-20px;  width:50px">
                                <a href="#"  data-toggle="modal" data-target="#cityModal">
                                    <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                </a>
                            </div>
                        </div> --}}
                        <input type="hidden" name="v_type" value="Supplier" id="">
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Company Name<span class="text-danger">*</span></label>
                                <input class="form-control company" type="text" placeholder="Enter Company Name" name="company" required>
                                <span class="text-danger">{{$errors->first('company') ? 'Company already exist' : null}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Address<span class="text-danger">*</span></label>
                                <input class="form-control address" type="text" placeholder="Enter Address" name="address" required>
                                <span class="text-danger">{{$errors->first('address') ? 'Address already exist' : null}}</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact No<span class="text-danger">*</span></label>
                                <input class="form-control c_no" type="text" placeholder="Enter Contact Number" name="c_no" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Country <small>(optional)</small></label>
                                <input class="form-control country" type="text" placeholder="Enter Country" name="country" >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >NTN <small>(optional)</small></label>
                                <input class="form-control VAT" type="text" placeholder="Enter VAT Number" name="VAT" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >GST <small>(optional)</small></label>
                                <input class="form-control GST" type="text" placeholder="Enter GST Number" name="GST" >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >State <small>(optional)</small></label>
                                <input class="form-control tate" type="text" placeholder="Enter State" name="state" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Email <small>(optional)</small></label>
                                <input class="form-control email" type="email" placeholder="Enter Email" name="email" >
                                <span class="text-danger">{{$errors->first('email') ? 'email already exist' : null}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Postal Code <small>(optional)</small></label>
                                <input class="form-control postalCode" type="text" placeholder="Enter Postal Code" name="postalCode" >
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="request_type" name="request_type" value="ajax">
                    <br>
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="submit" id="submitSupplier" class="btn btn-success">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
    $('#submitSupplier').click(function(e){
        e.preventDefault();
        var name = $('.name').val();
        var c_id = $('.c_id').val();
        var company = $('.company').val();
        var address = $('.address').val();
        var c_no = $('.c_no').val();
        var country = $('.country').val();
        var VAT = $('.VAT').val();
        var GST = $('.GST').val();
        var state = $('.state').val();
        var email = $('.email').val();
        var postalCode = $('.postalCode').val();
        var v_type = $('.v_type').val();
        var request_type = $('.request_type').val();
         axios
        .post('{{route("supplier.store")}}', {
                _token: '{{csrf_token()}}',
                _method: 'post',
                name:name,
                c_id:c_id,
                company:company,
                address:address,
                c_no:c_no,
                country:country,
                VAT:VAT,
                GST:GST,
                state:state,
                email:email,
                postalCode:postalCode,
                v_type:v_type,
                request_type:request_type,
            })
            .then(function (responsive) {
                if(responsive.data.supplier == null)
                {
                    alert(responsive.data.message);
                }
                else
                {
                    $('#supplierModal').modal('hide');
                    $('#s_id').append(`<option selected value="`+responsive.data.supplier.id+`">`+responsive.data.supplier.name+`</option>`);
                }
            })
            .catch(function (error) {
                alert(error);
        });
    });
</script>
