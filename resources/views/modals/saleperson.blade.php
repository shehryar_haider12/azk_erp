

<div id="personModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        @php
            $user=App\User::max('id');
        @endphp
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #5F9EA0">
                <h4 class="modal-title">Sale Person</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <form class="form-horizontal" >
                <div class="modal-body">
                    <input type="hidden" id="u_ids" name="u_id" value="{{ $user}}">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Sale Person Name<span class="text-danger">*</span></label>
                                <input value="{{old('name')}}" class="form-control " id="names" type="text" placeholder="Enter Name" name="name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label>Sale Person Type<span class="text-danger">*</span></label>
                                <select name="p_type" required class="form-control  " id="p_types" data-live-search="true">
                                    <option value="" selected disabled>Select</option>
                                        <option value="B2B">B2B</option>
                                        <option value="Retail">Retail</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >City <small>(optional)</small></label>
                                <select  class="form-control js-example-basic-single " id="city_ids" name="c_id" >
                                    <option value="" disabled selected>Select...</option>
                                        @foreach ($city as $s)
                                        <option value="{{$s->id}}">{{$s->c_name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="v_type" id="v_types" value="Saleperson" >
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Address<span class="text-danger">*</span></label>
                                <input value="{{ old('address')}}" class="form-control " id="addresss" type="text" placeholder="Enter Address" name="address" required>
                                <span class="text-danger">{{$errors->first('address') ? 'Address already exist' : null}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact No<span class="text-danger">*</span></label>
                                <input min="0" value="{{old('c_no')}}" class="form-control " id="c_nos" type="text" placeholder="Enter Contact Number" name="c_no" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Email<span class="text-danger">*</span></label>
                                <input value="{{old('email')}}" required class="form-control " id="emails" type="email" placeholder="Enter Email" name="email" >
                                <span class="text-danger">{{$errors->first('email') ? 'email already exist' : null}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Password<span class="text-danger">*</span></label>
                                <input value="" class="form-control " id="passwords" required type="password" placeholder="Enter Password" name="password" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label>Commission*</label>
                                <input type="text" class="form-control " id="commissions" required name="commission" >
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="request_types" name="request_type" value="ajax">
                    <br>
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="submit" id="submitSaleperson" class="btn btn-success">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
    $('#submitSaleperson').click(function(e){
        e.preventDefault();
        var name = $('#names').val();
        var address = $('#addresss').val();
        var c_no = $('#c_nos').val();
        var email = $('#emails').val();
        var c_id = $('#city_ids').val();
        var u_id = $('#u_ids').val();
        var v_type = $('#v_types').val();
        var p_type = $('#p_types').val();
        var commission = $('#commissions').val();
        var password = $('#passwords').val();
        var request_type = $('#request_types').val();
        if(name == '' || p_type == '' || address == '' || c_no == '' || email == '' || password == '' || commission == '')
        {
            alert('Fill all the required fields');
        }
        else
        {
            axios
            .post('{{route("saleperson.store")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    name:name,
                    address:address,
                    c_no:c_no,
                    email:email,
                    c_id:c_id,
                    u_id:u_id,
                    v_type:v_type,
                    p_type:p_type,
                    commission:commission,
                    password:password,
                    request_type:request_type,
                })
                .then(function (responsive) {
                    if(responsive.data.saleperson == null)
                    {
                        alert(responsive.data.message);
                    }
                    else
                    {
                        $('#personModal').modal('hide');
                        $('#sp_id').append(`<option selected value="`+responsive.data.saleperson.id+`">`+responsive.data.saleperson.name+`</option>`);
                    }
                })
                .catch(function (error) {
                    alert(error);
            });
        }

    });
</script>
