

<div id="productModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #5F9EA0">
                <h4 class="modal-title">Add Product</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Product Name<span class="text-danger">*</span></label>
                                <input  class="form-control pro_name" type="text" placeholder="Enter Product Name" name="pro_name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Product Code<span class="text-danger">*</span></label>
                                <input  class="form-control pro_code" type="text" placeholder="Enter Product Code" name="pro_code" required>
                                <span class="text-danger">{{$errors->first('pro_code') ? 'Product Code already exist' : null}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Product Type<span class="text-danger">*</span></label>
                                <select class="form-control js-example-basic-single p_type" name="p_type" id="p_type" required>
                                    <option value="" disabled selected>Select...</option>
                                    <option>Raw Material</option>
                                    <option>Packaging</option>
                                    <option>Finished</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Product Weight <small>(optional)</small></label>
                                <input  class="form-control weight" type="text" placeholder="Enter Product Weight" name="weight">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Product Unit<span class="text-danger">*</span></label>
                                <select class="form-control js-example-basic-single unit_id" name="unit_id" required id="unit_id">
                                    <option value="" disabled selected>Select...</option>
                                    @foreach ($unit as $u)
                                    <option  value="{{$u->u_name}}">{{$u->u_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Cost<span class="text-danger">*</span></label>
                                <input class="form-control cost" type="text" placeholder="Enter Product Cost" name="cost" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Price<span class="text-danger">*</span></label>
                                <input class="form-control price" type="text" placeholder="Enter Product Price" name="price" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Description <small>(optional)</small></label>
                                <input class="form-control description" type="text" placeholder="Enter Product Description" name="description">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Alert Quantity<span class="text-danger">*</span></label>
                                <input class="form-control alert_quantity" type="text" placeholder="Enter Product Alert Quantity" name="alert_quantity" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Brand<span class="text-danger">*</span></label>
                                <select class="form-control js-example-basic-single brand_id" name="brand_id" id="brand_id" required>
                                    <option value="" disabled selected>Select...</option>
                                    @foreach ($brands as $u)
                                    <option  value="{{$u->id}}">{{$u->b_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Category<span class="text-danger">*</span></label>
                                <select class="form-control js-example-basic-single cat_id" name="cat_id" id="cat_id" required>
                                    <option value="" disabled selected>Select...</option>
                                    @foreach ($cat as $u)
                                    <option  value="{{$u->id}}">{{$u->cat_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Sub Category<span class="text-danger">*</span></label>
                                <select class="form-control js-example-basic-single s_cat_id" name="s_cat_id" id="s_cat_id" required>
                                    <option value="" disabled selected>Select...</option>
                                    @foreach ($sub as $u)
                                    <option  value="{{$u->id}}">{{$u->s_cat_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Visible to POS <small>(optional)</small> </label>
                                <select class="form-control  visibility" name="visibility" id="visibility" required>
                                    <option value="" disabled selected>Select...</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label for="">Supplier<span class="text-danger">*</span></label>
                                <select multiple class="form-control js-example-basic-multiple" multiple="multiple" data-live-search="true" id="supplier_id" name="s_id[]" required>
                                    <option disabled >Select...</option>
                                    @foreach ($supplier as $s)
                                        <option value="{{$s->id}}">{{$s->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Case<span class="text-danger">*</span></label>
                                <input class="form-control case" type="text" placeholder="Enter Case" name="case" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Company Name<span class="text-danger">*</span></label>
                                <input class="form-control company_name" type="text" placeholder="Enter Company Name" name="company_name" required>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="request_type" name="request_type" value="ajax">
                    <br>
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="submit" id="submitProduct" class="btn btn-success">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<script>
    $('#submitProduct').click(function(e){
        e.preventDefault();
        var s_id = $('#supplier_id').val();
        var pro_name = $('.pro_name').val();
        var pro_code = $('.pro_code').val();
        var p_type = $('#p_type').val();
        var weight = $('.weight').val();
        var unit_id = $('#unit_id').val();
        var cost = $('.cost').val();
        var price = $('.price').val();
        var description = $('.description').val();
        var alert_quantity = $('.alert_quantity').val();
        var brand_id = $('#brand_id').val();
        var cat_id = $('#cat_id').val();
        var s_cat_id = $('#s_cat_id').val();
        var visibility = $('#visibility').val();
        var cases = $('.case').val();
        var company_name = $('.company_name').val();
        var request_type = $('.request_type').val();
        if(pro_name == '' || pro_code =='' || p_type == '' || unit_id == '' || cases == '' || cost == '' || price == '' || alert_quantity == '' || brand_id == '' || cat_id == '' || s_cat_id == '' || company_name == '' || s_id == '')
        {
            alert('Fill all the required fields');
        }
        else
        {

            axios
            .post('{{route("product.store")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    s_id: s_id,
                    pro_name: pro_name,
                    pro_code: pro_code,
                    p_type: p_type,
                    weight: weight,
                    unit_id: unit_id,
                    cost: cost,
                    price: price,
                    description: description,
                    alert_quantity: alert_quantity,
                    brand_id: brand_id,
                    cat_id: cat_id,
                    s_cat_id: s_cat_id,
                    visibility: visibility,
                    case: cases,
                    company_name: company_name,
                    request_type:request_type,
                })
                .then(function (responsive) {
                    if(responsive.data.p == null)
                    {
                        alert(responsive.data.message);
                    }
                    else
                    {

                        $('#productModal').modal('hide');
                        $('#p_id').append(`<option  value="`+responsive.data.p.id+`">`+responsive.data.p.pro_code+` - `+responsive.data.p.pro_name+`</option>`);
                    }
                })
                .catch(function (error) {
                    alert(error);
            });
        }

    });
</script>
