

<div id="brandModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #5F9EA0">
                <h4 class="modal-title">Brand</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <form class="form-horizontal" >
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Brand Name<span class="text-danger">*</span></label>
                                <input class="form-control b_name" type="text" placeholder="Enter Brand Name" name="b_name" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact Person Name</label>
                                <input value="{{$brand->c_p_name ?? old('c_p_name')}}" class="form-control c_p_name" type="text" placeholder="Enter Contact Person Name" name="c_p_name" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact Person Number</label>
                                <input min="0" title="11 characters minimum" pattern="[0-9]{11}" value="{{$brand->c_p_contactNo ?? old('c_p_contactNo')}}" class="form-control c_p_contactNo" type="text" placeholder="Enter Contact Person Number" name="c_p_contactNo" >
                            </div>
                            <span class="text-danger">{{$errors->first('c_p_contactNo') ? 'Contact number not be greater than 11 digits' : null}}</span>
                        </div>
                    </div>
                    <input type="hidden" class="request_type" name="request_type" value="ajax">
                    <br>
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="submit" id="submitBrand" class="btn btn-success">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<script>
    $('#submitBrand').click(function(e){
        e.preventDefault();
        var b_name = $('.b_name').val();
        var c_p_name = $('.c_p_name').val();
        var c_p_contactNo = $('.c_p_contactNo').val();
        var request_type = $('.request_type').val();
        if(b_name == '')
        {
            alert('Enter Brand Name');
        }
        else
        {

            axios
                .post('{{route("brands.store")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    b_name:b_name,
                    c_p_contactNo:c_p_contactNo,
                    c_p_name:c_p_name,
                    request_type:request_type,
                })
                .then(function (responsive) {
                    $('#brandModal').modal('hide');
                    $('#brand_id').append(`<option selected value="`+responsive.data.brands.id+`">`+responsive.data.brands.b_name+`</option>`);
                })
                .catch(function (error) {
                    alert(error);
            });
        }
    });
</script>
