

<div id="customerModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header" style="background-color: #5F9EA0">
                <h4 class="modal-title">Customer</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <form class="form-horizontal" >
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Company Name<span class="text-danger">*</span></label>
                                <input class="form-control " id="company" type="text" placeholder="Enter Company Name" name="company" required>
                                <span class="text-danger">{{$errors->first('company') ? 'Company already exist' : null}}</span>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >City <small>(optional)</small></label>
                                <select class="form-control js-example-basic-single " id="city_id"  name="city_id" >
                                    <option value="" disabled selected>Select...</option>
                                        @foreach ($city as $s)
                                        <option value="{{$s->id}}">{{$s->c_name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="v_type" id="v_type" value="Customer" >
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Customer Group<span class="text-danger">*</span></label>
                                <select required class="form-control  c_group" id="c_group" name="c_group" >
                                    <option value="" disabled selected>Select...</option>
                                        @foreach ($cgroup as $s)
                                        <option value="{{$s->id}}">{{$s->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Price Group<span class="text-danger">*</span></label>
                                <select required class="form-control  " id="p_group"  name="p_group" >
                                    <option value="" disabled selected>Select...</option>
                                        @foreach ($pgroup as $s)
                                        <option value="{{$s->id}}">{{$s->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Address<span class="text-danger">*</span></label>
                                <input class="form-control " id="address" type="text" placeholder="Enter Address" name="address" required>
                                <span class="text-danger">{{$errors->first('address') ? 'Address already exist' : null}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Country <small>(optional)</small></label>
                                <input class="form-control " id="country" type="text" placeholder="Enter Country" name="country" >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Customer Name<span class="text-danger">*</span></label>
                                <input class="form-control " id="name" type="text" placeholder="Enter Customer Name" name="name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact No<span class="text-danger">*</span></label>
                                <input class="form-control " id="c_no" type="text" placeholder="Enter Contact Number" name="c_no" required>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Customer Name 2<span class="text-danger">*</span></label>
                                <input class="form-control " id="name2" type="text" placeholder="Enter Customer Name" name="name2" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact No 2<span class="text-danger">*</span></label>
                                <input  class="form-control " id="c_no2" type="text" placeholder="Enter Contact Number" name="c_no2" required>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >NTN <small>(optional)</small></label>
                                <input class="form-control " id="VAT" type="text" placeholder="Enter NTN Number" name="VAT" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >GST <small>(optional)</small></label>
                                <input class="form-control " id="GST" type="text" placeholder="Enter GST Number" name="GST" >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >State  <small>(optional)</small></label>
                                <input class="form-control " id="state" type="text" placeholder="Enter State" name="state" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Email <small>(optional)</small></label>
                                <input class="form-control " id="email" type="email" placeholder="Enter Email" name="email" >
                                <span class="text-danger">{{$errors->first('email') ? 'email already exist' : null}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Postal Code <small>(optional)</small></label>
                                <input class="form-control " id="postalCode" type="text" placeholder="Enter Postal Code" name="postalCode" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Balance <small>(optional)</small></label>
                                <input  class="form-control " id="balance" type="text" placeholder="Enter Balance" name="balance" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Area <small>(optional)</small></label>
                                <select id="a_id" class="form-control  " id="a_id" data-live-search="true" name="a_id" >
                                    <option value="" disabled selected>Select...</option>
                                        @foreach ($area as $s)
                                        <option value="{{$s->id}}">{{$s->name}}</option>
                                        @endforeach

                                </select>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="request_type" name="request_type" value="ajax">
                    <br>
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="submit" id="submitCustomer" class="btn btn-success">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
    $('#submitCustomer').click(function(e){
        e.preventDefault();
        var name = $('#name').val();
        var c_id = $('#city_id').val();
        var company = $('#company').val();
        var address = $('#address').val();
        var c_no = $('#c_no').val();
        var country = $('#country').val();
        var VAT = $('#VAT').val();
        var GST = $('#GST').val();
        var state = $('#state').val();
        var email = $('#email').val();
        var postalCode = $('#postalCode').val();
        var v_type = $('#v_type').val();
        var c_group = $('#c_group').val();
        var p_group = $('#p_group').val();
        var name2 = $('#name2').val();
        var c_no2 = $('#c_no2').val();
        var a_id = $('#a_id').val();
        var balance = $('#balance').val();
        var request_type = $('#request_type').val();

        if(company == '' || c_group == '' || p_group == '' || address =='' || name == '' || c_no == '' || name2 == '' || c_no2 == '' )
        {
            alert('Fill all the required fields');
        }
        else
        {

            axios
            .post('{{route("customer.store")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    name:name,
                    c_id:c_id,
                    company:company,
                    address:address,
                    c_no:c_no,
                    country:country,
                    VAT:VAT,
                    GST:GST,
                    state:state,
                    email:email,
                    postalCode:postalCode,
                    v_type:v_type,
                    c_group:c_group,
                    p_group:p_group,
                    name2:name2,
                    c_no2:c_no2,
                    a_id:a_id,
                    balance:balance,
                    request_type:request_type,
                })
                .then(function (responsive) {
                    if(responsive.data.ven == null)
                    {
                        alert(responsive.data.message);
                    }
                    else
                    {
                        $('#s_address').prop('readonly',true);
                        $('#c_address').prop('readonly',false);
                        $('#customerModal').modal('hide');
                        $('#cm_id').append(`<option selected value="`+responsive.data.ven.id+`">`+responsive.data.ven.name+`</option>`);
                    }
                })
                .catch(function (error) {
                    alert(error);
            });
        }

    });
</script>
