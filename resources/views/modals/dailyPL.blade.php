

<div id="dailyPLReportModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="title" style="font-weight: bold"> </h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <select id="ware_id" class="form-control js-example-basic-single"  >

                        </select>
                    </div>
                </div>
                <br>
                <div class="table-responsive">
                    <table id="example5" class="table table-striped table-bordered" style="width:100%">
                            <tbody>

                            </tbody>

                        </table>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
