@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li class="breadcrumb-item"><a href="{{ url('')}}/saleperson">Sale Persons</a></li>
<li class="breadcrumb-item"><span>{{$isEdit ? 'Edit' : 'Add'}} Sale Person</span></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="card" id="form_wizard_1">
                <div class="card-header">
                    <div class="caption">
                        <i class=" fa fa-user font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Sale Person</span>
                    </div>

                </div>
                <div class="card-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('saleperson.update',$vendor->id) :  route('saleperson.store')}} " class="form-horizontal" method="POST" id="salePersonForm">
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">
                                <input type="hidden" name="u_id" value="{{$isEdit ? $vendor->u_id : $u_id}}">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Sale Person Name<span class="text-danger">*</span></label>
                                        <input value="{{$vendor->name ?? old('name')}}" class="form-control" type="text" placeholder="Enter Name" name="name" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Sale Person Type<span class="text-danger">*</span></label>
                                        <select name="p_type" required class="form-control js-example-basic-single" >
                                            <option value="" selected disabled>Select</option>
                                            @if ($isEdit)
                                                <option value="B2B" {{$vendor->p_type == 'B2B' ? 'selected' : null}}>B2B</option>
                                                <option value="Retail" {{$vendor->p_type == 'Retail' ? 'selected' : null}}>Retail</option>
                                            @else
                                                <option value="B2B" {{old('p_type') == 'B2B' ? 'selected' : null}} >B2B</option>
                                                <option value="Retail" {{old('p_type') == 'Retail' ? 'selected' : null}}>Retail</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add City',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >City <small>(optional)</small></label>
                                        <select id="c_id" class="form-control js-example-basic-single"  name="c_id" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add City',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($city as $s)
                                                <option {{$s->id == $vendor->c_id ? 'selected' : null}} value="{{$s->id}}">{{$s->c_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($city as $s)
                                                <option value="{{$s->id}}" {{old('c_id') == $s->id ? 'selected' : null}}>{{$s->c_name}}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add City',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 30px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#cityModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <input type="hidden" name="v_type" value="Saleperson" id="">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Address<span class="text-danger">*</span></label>
                                        <input value="{{$vendor->address ?? old('address')}}" class="form-control" type="text" placeholder="Enter Address" name="address" required>
                                        <span class="text-danger">{{$errors->first('address') ? 'Address already exist' : null}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Contact No<span class="text-danger">*</span></label>
                                        <input min="0" value="{{$vendor->c_no ?? old('c_no')}}" class="form-control" type="text" placeholder="Enter Contact Number" name="c_no" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Email<span class="text-danger">*</span></label>
                                        <input value="{{$vendor->email ?? old('email')}}" required class="form-control" type="email" placeholder="Enter Email" name="email" >
                                        <span class="text-danger">{{$errors->first('email') ? 'email already exist' : null}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Password<span class="text-danger">*</span></label>
                                        <input value="" class="form-control" {{$isEdit ? '' : 'required'}} type="password" placeholder="Enter Password" name="password" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Commission in(%) <span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" required name="commission" value="{{$vendor->commission ?? old('commission')}}">
                                    </div>
                                </div>
                            </div>
                            <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
    @include('modals.city')
@endsection
@section('custom-script')
@toastr_js
@toastr_render
<script>
    $(document).ready(function(){
        $('.js-example-basic-single').select2();
        $('#c_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#cityModal').modal("show"); //Open Modal
            }
        });
    });
</script>
<script>
    $('#salePersonForm').validate({
        rules: {
            name: {
                required: true,
            },
            p_type: {
                required: true,
            },
            address: {
                required: true,
            },
            email: {
                required: true,
            },
            password: {
                required: true,
            },
            commission: {
                required: true,
            },
            c_no: {
                required: true,
            },

        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>
@endsection
