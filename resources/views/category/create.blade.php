@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
    <li class="breadcrumb-item"><a href="{{ url('')}}/category">Category</a></li>
    <li class="breadcrumb-item"><span>{{$isEdit ? 'Edit' : 'Add'}} Category</span></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="card" id="form_wizard_1">
                <div class="card-header">
                    <div class="caption">
                        <i class="fab fa-cuttlefish font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Category</span>
                    </div>

                </div>
                <div class="card-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('category.update',$cat->id) :  route('category.store')}} " class="form-horizontal" method="POST" id="categoryForm">
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-outline">
                                        <label >Category Name<span class="text-danger">*</span></label>
                                        <input value="{{$cat->cat_name ?? old('cat_name')}}" class="form-control" type="text" placeholder="Enter Category Name" name="cat_name" >
                                        <span class="text-danger">{{$errors->first('cat_name') ? 'Already exist or Enter category' : null}}</span>
                                    </div>
                                </div>
                            </div>
                            <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0 ">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>

@endsection
@section('custom-script')
<script>
    $('#categoryForm').validate({
        rules: {
            cat_name: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>
@toastr_js
@toastr_render
@endsection
