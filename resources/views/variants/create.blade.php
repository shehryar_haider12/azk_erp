@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li class="breadcrumb-item"><a href="{{ url('')}}/variants">Variants</a></li>
    <li class="breadcrumb-item"><span>Add Variants</span></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="card" id="form_wizard_1">
                <div class="card-header">
                    <div class="caption">
                        <i class=" fa fa-list-alt font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">Add Variants</span>
                    </div>

                </div>
                <div class="card-body">
                    <!-- BEGIN FORM-->
                    <form action="{{route('variants.store')}} " class="form-horizontal" method="POST" id="variantForm">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label for="">Parent Variant<span class="text-danger">*</span></label>
                                        <select class="form-control " style="overflow-y: auto" data-live-search="true"  name="p_id" id="p_id" >
                                            <option value=""  selected disabled>Select...</option>
                                            <option value="0">No Parent</option>
                                            @foreach ($variant as $m)
                                                @if ($m->p_id == 0)
                                                    <option value="{{$m->id}}">{{$m->name}}</option>
                                                @else
                                                    <option value="{{$m->id}}">&emsp;{{$m->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{$errors->first('p_id') ? 'Select anyone' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Name<span class="text-danger">*</span></label>
                                        <input value="{{ old('name')}}" class="form-control" type="text" placeholder="Enter Variant Name" name="name" >
                                        <span class="text-danger">{{$errors->first('name') ? 'Enter name' : null}}</span>
                                    </div>
                                </div>

                            </div>
                            <br>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>

@endsection
@section('custom-script')
<script>
    $('#variantForm').validate({
        rules: {
            p_id: {
                required: true,
            },
            name: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>
@toastr_js
@toastr_render
@endsection
