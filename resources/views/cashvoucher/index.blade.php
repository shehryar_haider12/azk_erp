@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css

@section('sidebar-name1')
<li class="breadcrumb-item"><a href="{{ url('')}}/cashVoucher">Cash Voucher</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="card">
                <div class="card-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="card-header" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-sticky-note font-white"></i>View Cash Voucher
                                            </div>
                                            <div class="col-md-5 col-sm-5 "></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                                {{-- @if(in_array('add',$permissions)) --}}
                                                    <a id="GFG" href="{{route('cashVoucher.create')}}" >
                                                        <button style="margin-top: -9px" type="button"  class="btn btn-block btn-info btn-md ">Add Cash Voucher</button>
                                                    </a>
                                                {{-- @endif --}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="tableview">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Debit Account</label>
                                                        <select name="debit" id="debit" class="form-control js-example-basic-single">
                                                            <option selected value="">Select</option>
                                                            @foreach ($account as $a)
                                                                @if ($a->headCategory->name == 'Cash' || $a->headCategory->name == "Bank")
                                                                    <option>{{$a->name_of_account}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Credit Account</label>
                                                            <select name="credit" class="form-control js-example-basic-single">
                                                                <option selected value="">Select</option>
                                                                @foreach ($account as $a)
                                                                    @if ($a->headCategory->name == 'Cash' || $a->headCategory->name == "Bank")
                                                                        <option>{{$a->name_of_account}}</option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Accounting Date</label>
                                                            <input type="date" name="a_date"  class="form-control" placeholder="Head Category Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Posted Date</label>
                                                            <input type="date" name="p_date"  class="form-control" placeholder="Head Category Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Period</label>
                                                            <input type="month" name="period"  class="form-control" placeholder="Head Category Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select class="form-control " data-live-search="true" name="status" id="status" >
                                                                <option value=""  selected>Select...</option>
                                                                <option>Pending</option>
                                                                <option>Approved</option>
                                                            </select>
                                                        </div>
                                                        </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        {{-- <label for="">Unit Name</label> --}}
                                                        <label for="" style="visibility: hidden">.</label>
                                                        <button id="search" style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">Voucher#</th>
                                                            <th>Accounting Date</th>
                                                            <th>Posted Date</th>
                                                            <th>Period</th>
                                                            <th>Created By</th>
                                                            <th>Debit Account</th>
                                                            <th>Credit Account</th>
                                                            <th>Description</th>
                                                            <th>Amount</th>
                                                            <th>Status</th>
                                                            <th>Type</th>
                                                            <th width="15%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th>TOTAL</th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                    </tfoot>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
            <!-- End: life time stats -->
    </div>
    @section('modal')
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Cash Voucher</h4>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Voucher#</label>
                                    <input class="form-control" type="text"  id="vno" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Accounting date</label>
                                    <input class="form-control" type="text" id="adate" readonly >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Posted Date</label>
                                    <input class="form-control" type="text"  id="pdate" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Period</label>
                                    <input  class="form-control" type="text" id="period" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Debit Account</label>
                                    <input class="form-control" type="text" id="debits" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Credit Account</label>
                                    <input  class="form-control" type="text" id="credit" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Amount</label>
                                    <input class="form-control" type="text" id="amount" readonly >
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

    @endsection
@section('custom-script')
@toastr_js
@toastr_render
    <script type="text/javascript">
        function view(id)
        {
            $.ajax({
                url:"{{url('')}}/cashVoucher/"+id,
                method:"GET",
                error: function (request, error) {
                            alert(" Can't do because: " + error +request);
                        },
                success:function(data){
                    $('#vno').val(data.id);
                    $('#adate').val(data.accounting_date);
                    $('#pdate').val(data.posted_date);
                    $('#period').val(data.period);
                    $('#debits').val(data.account1);
                    $('#credit').val(data.account2);
                    $('#amount').val(data.debit);
                }
            });
        }

        $('#example').on('click','td', function () {
            var col_index = $(this).index();
            var row_index = $(this).parent().index();
            var count = document.getElementById('example').rows[row_index].cells.length - 1;
            if(col_index <count)
            {
                var $tr = $(this).closest('tr');
                var id = $tr.find("td:eq(0) input[type='text']").attr('data-id');
                view(id);
                $('#myModal').modal('show');
            }
        });

        $(document).ready(function () {
            $('.js-example-basic-single').select2();
            var table = $('#example').DataTable({
                order: [[ 0, "desc" ]],
                processing: true,
                serverSide: true,
                ajax: '{{route("cashVoucher.datatable")}}',
                "columns": [{
                        "data": "id",
                        "defaultContent": ""
                    },
                    {
                        "data": "accounting_date",
                        "defaultContent": ""
                    },
                    {
                        "data": "posted_date",
                        "defaultContent": ""
                    },
                    {
                        "data": "period",
                        "defaultContent": ""
                    },
                    {
                        "data": "created_user.name",
                        "defaultContent": ""
                    },
                    {
                        "data": "account1",
                        "defaultContent": ""
                    },
                    {
                        "data": "account2",
                        "defaultContent": ""
                    },
                    {
                        "data": "description",
                        "defaultContent": ""
                    },
                    {
                        "data": "debit",
                        "defaultContent": ""
                    },
                    {
                        "data": "status",
                        "defaultContent": ""
                    },
                    {
                        "data": "type",
                        "defaultContent": ""
                    },
                    {
                        "data": "id",
                        "defaultContent": ""
                    },
                ],
                "columnDefs": [{
                        "targets": 'no-sort',
                        "orderable": false,
                    },
                    {
                        "targets": 0,
                        "render": function (data, type, row, meta) {
                            var id = meta.row + 1;
                            return `<input class="form-control" type="text" readonly data-id="`+row.id+`" style="background: transparent;border: none;" value="`+id+`">`;
                        },
                    },
                    {
                        "targets": -1,
                        "render": function (data, type, row, meta) {
                            if(row.status == "Pending")
                            {
                                return `
                                <select class="form-control action" id="`+row.id+`">
                                    <option>Actions</option>
                                    <option >Approved</option>
                                    <option>Edit Voucher</option>
                                    <option>Voucher Details</option>
                                </select>`;
                            }
                            if(row.status == "Approved")
                            {
                                return `
                                <select class="form-control action" id="`+row.id+`">
                                    <option>Actions</option>
                                    <option>Voucher Details</option>
                                </select>`;
                            }
                        },
                    },
                    {
                        "targets": -3,
                        "render": function (data, type, row, meta) {
                            if(row.status == "Pending")
                            {
                                return `<button type="button" class="btn btn-xs  red" id="`+row.id+`">
                                    `+row.status+`
                                </button>`;
                            }
                            if(row.status == "Approved")
                            {
                                return `<button type="button" class="btn btn-xs  blue" id="`+row.id+`">
                                    `+row.status+`
                                </button>`;
                            }
                        }
                    }
                ],
                "footerCallback": function( tfoot, data, start, end, display ) {
                    var amount = 0;
                    for (var i = 0; i < data.length; i++) {
                        amount = amount + parseInt(data[i]['debit']);
                    }
                    $( table.column( 8 ).footer() ).html(Number.isNaN(amount) ? '0' : amount);
                },

            });
            $('#advanceSearch').submit(function(e){
                    e.preventDefault();
                    table.columns(3).search($('input[name="period"]').val());
                    table.columns(5).search($('select[name="debit"]').val());
                    table.columns(6).search($('select[name="credit"]').val());
                    table.columns(9).search($('select[name="status"]').val());
                    table.columns(1).search($('input[name="a_date"]').val());
                    table.columns(2).search($('input[name="p_date"]').val());
                    table.draw();
            });
            $(document).on('change','.action',function(){
                var val=$(this).val();
                if(val == "Approved")
                {
                    var id = $(this).attr('id');
                    var status = val;
                    axios
                    .post('{{route("cashVoucher.status")}}', {
                        _token: '{{csrf_token()}}',
                        _method: 'post',
                        id: id,
                        status: status,
                        })
                        .then(function (responsive) {
                        console.log('responsive');
                        location.reload();
                        })
                        .catch(function (error) {
                        console.log(error);
                    });
                }
                if(val == 'Edit Voucher')
                {
                    var id=$(this).attr("id");
                    window.location.href='{{url('')}}/cashVoucher/'+id+'/edit';
                }
                if(val == 'Voucher Details')
                {
                    var id=$(this).attr("id");
                    view(id);
                    $('#myModal').modal('show');
                    $('.action').val('Actions');
                }
            });

        });

    </script>

@endsection
@endsection
