@extends('layouts.master')
@section('top-styles')
@toastr_css
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #5F9EA0;
    }
    .modal-header{
        background: #5F9EA0 !important;
    }
    .form-check-inline {
        display: inline-flex;
        align-items: center;
        padding-left: 0;
        margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
        position: static;
        margin-top: 0;
        margin-right: .3125rem;
        margin-left: 0;
    }
</style>
@toastr_css

@section('sidebar-name1')
@if(Session::has('download'))
    <meta http-equiv="refresh" content="5;url={{ Session::get('download') }}">
@endif
<li class="breadcrumb-item"><a href="{{ url('')}}/sales">Sales</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="card">
                <div class="card-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="card-header" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fa fa-credit-card font-white"></i>View POS Sales
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="custom_datatable">

                                            {{-- <form action="{{url('')}}/sales/search" method="POST" id="advanceSearch">
                                                @csrf
                                                <input type="hidden" value="{{$menu_id}}" id="menuid" name="menuid">
                                                <div class="tableview">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio2">
                                                            <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Sale Date
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="Status">By Sale Status
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="PStatus">By Payment Status
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="customer">By Customer
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="saleperson">By Sale Person
                                                        </label>
                                                    </div>
                                                <div class="row">

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Year</label>
                                                            <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Month</label>
                                                        <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">From</label>
                                                            <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">To</label>
                                                            <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Customer Name</label>
                                                            <select class="form-control" data-live-search="true" name="c_id" id="c_id" disabled >
                                                                <option value=""  selected>Select...</option>
                                                                @foreach ($customer as $u)
                                                                <option  value="{{$u->id}}">{{$u->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Sale Persons Name</label>
                                                            <select class="form-control" data-live-search="true" name="sp_id" id="sp_id" disabled >
                                                                <option value=""  selected>Select...</option>
                                                                @foreach ($saleperson as $u)
                                                                <option  value="{{$u->id}}">{{$u->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Sales Status</label>
                                                            <select name="status" id="status" class="form-control " disabled>
                                                                <option disabled selected>No Filter</option>
                                                                <option>Pending</option>
                                                                <option>Approved</option>
                                                                <option>Partial</option>
                                                                <option>Complete</option>
                                                                <option>Delivered</option>
                                                                <option>Return</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Payment Status</label>
                                                            <select name="p_status" id="p_status" class="form-control " disabled>
                                                                <option disabled selected>No Filter</option>
                                                                <option>Pending</option>
                                                                <option>Partial</option>
                                                                <option>Paid</option>
                                                                <option>Return</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                        <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="" style="visibility: hidden">.</label>
                                                            <button id="search" disabled style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                            <i class="fa fa-search pr-1"></i> Search</button>
                                                        </div>
                                                        </div>
                                                </div>
                                                </div>
                                            </form> --}}
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Sales Date</th>
                                                            <th>Warehouse</th>
                                                            <th>Customer</th>
                                                            <th>Total</th>
                                                            <th>Tax</th>
                                                            <th>Sale Status</th>
                                                            <th>Payment Status</th>
                                                            <th>Return Status</th>
                                                            <th>Sale Type</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $ttl=0;
                                                            $bln=0;
                                                            $paid=0;
                                                            $taxx=0;
                                                        @endphp
                                                        @foreach ($sales as $s)
                                                            @if ($counter == null)
                                                                <tr>
                                                                    <td>
                                                                        {{$s->id}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->sale_date}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->warehouse->w_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->customer->name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->total}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->tax == null ? 0 : $s->tax}}
                                                                    </td>
                                                                    @if($s->s_status=='Pending')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-warning" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->s_status=='Approved')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-primary" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->s_status=='Partial')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-info" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->s_status=='Complete')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-success" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->s_status=='Return')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-default" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->s_status=='Delivered')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-danger" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->p_status=='Pending')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-warning" id="{{$s->id}}">
                                                                                {{$s->p_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->p_status=='Partial')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-primary" id="{{$s->id}}">
                                                                                {{$s->p_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->p_status=='Paid')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-success" id="{{$s->id}}">
                                                                                {{$s->p_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->p_status=='Return')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-info" id="{{$s->id}}">
                                                                                {{$s->p_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->return_status=='Partial')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-info" id="{{$s->id}}">
                                                                                {{$s->return_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->return_status=='No Return')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-success" id="{{$s->id}}">
                                                                                {{$s->return_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->return_status=='Complete')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-default" id="{{$s->id}}">
                                                                                {{$s->return_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->return_status=='Requested')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-warning" id="{{$s->id}}">
                                                                                {{$s->return_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    <td>
                                                                        {{$s->s_type}}
                                                                    </td>

                                                                    <td>
                                                                        <select class="form-control action" id="{{$s->id}}" >
                                                                            <option >Actions</option>
                                                                            @if(in_array('show',$permissions))
                                                                                <option >Sale Details</option>
                                                                            @endif
                                                                            @if(in_array('report',$permissions))
                                                                                <option >Sale Report</option>
                                                                            @endif
                                                                            @if ($s->return_status !='Requested')
                                                                                @if(in_array('Return',$permissions))
                                                                                    <option >Sale Return</option>
                                                                                @endif
                                                                            @endif
                                                                            @if(in_array('View Payment',$permissions))
                                                                                <option >View Payment</option>
                                                                            @endif
                                                                        </select>

                                                                    </td>
                                                                </tr>
                                                            @else
                                                                @php
                                                                    $uid = Auth::user()->id;
                                                                    $sale =  \App\SaleCounter::where('u_id',$uid)->where('co_id',$counter)->get();
                                                                @endphp
                                                                    @foreach ($sale as $ss)
                                                                        @if ($ss->s_id == $s->id)
                                                                        <tr>
                                                                            <td>
                                                                                {{$s->id}}
                                                                            </td>
                                                                            <td>
                                                                                {{$s->sale_date}}
                                                                            </td>
                                                                            <td>
                                                                                {{$s->warehouse->w_name}}
                                                                            </td>
                                                                            <td>
                                                                                {{$s->customer->name}}
                                                                            </td>
                                                                            <td>
                                                                                {{$s->total}}
                                                                            </td>nput  width="10"  type="text" class="form-control" readonly id="amount" value="{{$s->total - $s->total_amount}}">
                                                                            </td>
                                                                            @if($s->s_status=='Pending')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  red" id="{{$s->id}}">
                                                                                        {{$s->s_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->s_status=='Approved')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  btn-primary" id="{{$s->id}}">
                                                                                        {{$s->s_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->s_status=='Partial')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  btn-info" id="{{$s->id}}">
                                                                                        {{$s->s_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->s_status=='Complete')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  green" id="{{$s->id}}">
                                                                                        {{$s->s_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->s_status=='Return')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  btn-info" id="{{$s->id}}">
                                                                                        {{$s->s_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->s_status=='Delivered')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  green" id="{{$s->id}}">
                                                                                        {{$s->s_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->p_status=='Pending')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  red" id="{{$s->id}}">
                                                                                        {{$s->p_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->p_status=='Partial')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  blue" id="{{$s->id}}">
                                                                                        {{$s->p_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->p_status=='Paid')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  green" id="{{$s->id}}">
                                                                                        {{$s->p_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->p_status=='Return')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  btn-info" id="{{$s->id}}">
                                                                                        {{$s->s_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->return_status=='Partial')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  blue" id="{{$s->id}}">
                                                                                        {{$s->return_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->return_status=='No Return')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  green" id="{{$s->id}}">
                                                                                        {{$s->return_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->return_status=='Complete')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  red" id="{{$s->id}}">
                                                                                        {{$s->return_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            @if($s->return_status=='Requested')
                                                                                <td>
                                                                                    <button type="button" class="btn btn-xs  yellow" id="{{$s->id}}">
                                                                                        {{$s->return_status}}
                                                                                    </button>
                                                                                </td>
                                                                            @endif
                                                                            <td>
                                                                                {{$s->s_type}}
                                                                            </td>

                                                                            <td>
                                                                                <select class="form-control action" id="{{$s->id}}" >
                                                                                    <option >Actions</option>
                                                                                    @if(in_array('show',$permissions))
                                                                                        <option >Sale Details</option>
                                                                                    @endif
                                                                                    @if(in_array('report',$permissions))
                                                                                        <option >Sale Report</option>
                                                                                    @endif
                                                                                    @if ($s->return_status !='Requested')
                                                                                        @if(in_array('Return',$permissions))
                                                                                            <option >Sale Return</option>
                                                                                        @endif
                                                                                    @endif
                                                                                    @if(in_array('View Payment',$permissions))
                                                                                        <option >View Payment</option>
                                                                                    @endif
                                                                                </select>

                                                                            </td>
                                                                        </tr>
                                                                        @endif
                                                                    @endforeach
                                                            @endif
                                                            @php
                                                                $ttl+=$s->total;
                                                                $bln+=(($s->total - $s->total_amount)+$s->return);
                                                                $paid+=($s->total_amount - $s->return) ;
                                                                $taxx+=$s->tax == null ? 0 : $s->tax;
                                                        @endphp
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th>TOTAL</th>
                                                            <th>{{$ttl}}</th>
                                                            <th>{{$taxx}}</th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('modal')
        @include('modals.payment')
        @include('modals.transaction')
        @include('modals.delivery')
        @include('modals.salereturn')

        <div id="GDN" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">View GDN History</h4>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>

                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Sale Number</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Name" id="SaleNo" readonly>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="table-responsive">
                            <table id="example7" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="5%">S.No</th>
                                            <th>Delivery Date</th>
                                            <th>GDN No</th>
                                            <th>Delivered Product</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Sub Total</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>

                                </table>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Sales</h4>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Sale Date</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Name" id="sale_date" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Reference No</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Code" id="ref_no" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Warehouse</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Weight" id="w_name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Customer</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="c_name" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Accountant</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="b_name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Sale Person</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="s_name" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Tax Status</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="tax_status" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Advance payment</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="advance" readonly>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-1">
                                <a href="" id="edithref">
                                    <button type="button"  id="edit" class="btn btn-sm  btn-info" >Edit</button>
                                </a>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="5%">S.No</th>
                                            <th width="35%">Code - Name</th>
                                            <th>Unit</th>
                                            <th>Brand</th>
                                            @if (in_array('price',$permissions))
                                                <th width="7%">Price</th>
                                            @endif
                                            <th width="5%">Quantity</th>
                                            <th width="5%">Delivered Quantity</th>
                                            <th width="5%">Return Quantity</th>
                                            @if (in_array('percent',$permissions))
                                                <th width="5%">Discount Percent</th>
                                            @endif
                                            @if (in_array('amount',$permissions))
                                                <th width="5%">Discounted Amount</th>
                                            @endif
                                            @if (in_array('subtotal',$permissions))
                                                <th width="10%">Sub total</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>


                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    @endsection
    @section('custom-script')
    <script>
        CKEDITOR.replace( 'editor2' );
   </script>
    @toastr_js
    @toastr_render
<script type="text/javascript">

    $(document).ready(function () {
        $('.js-example-basic-single').select2();
        var table = $('#example').DataTable({
            scrollX: true,
            order: [[ 0, "desc" ]]
        });

        $(document).on('change','.action',function(){
            var val=$(this).val();

            if(val == 'Sale Report')//
            {
                var id=$(this).attr("id");
                $.ajax({
                    url:"{{url('')}}/sales/pdf/"+id,
                    method:"GET",
                    data:
                    {
                        id:id,
                    },
                    error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        window.location.href= '{{url('')}}/sales/pdf/'+id;
                        // location.reload();
                        $('.action').val('Actions');
                    }
                });
            }
            if(val == 'Sale Details')//
            {

                var id=$(this).attr("id");
                $("#example1 tbody").empty();
                $.ajax({
                    url:"{{url('')}}/sales/"+id,
                    method:"GET",
                    error: function (request, error) {
                        lert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        $('#sale_date').val(data[0].sale_date);
                        $('#ref_no').val(data[0].ref_no);
                        $('#w_name').val(data[0].warehouse.w_name);
                        $('#c_name').val(data[0].customer.name);
                        $('#b_name').val(data[0].biller.name);
                        $('#s_name').val(data[0].saleperson.name);
                        $('#advance').val(data[0].advance);
                        $('#tax_status').val(data[0].tax_status);
                        if(data[0].s_status == "Pending")
                        {
                            $('#edit').attr('disabled',false);
                        }
                        else
                        {
                            $('#edit').attr('disabled',true);
                        }
                        $('#edithref').attr('href','{{url('')}}/sales/'+id+'/edit');
                        for (let i = 0; i < data[1].length; i++) {
                            if(data[1][i].type == 1)
                            {
                                $("#example1").append("<tr><td>"+data[1][i].id+"</td><td>"+data[1][i].variant.name+"</td><td>"+data[1][i].variant.product.unit.u_name+"</td><td>"+data[1][i].variant.product.brands.b_name+"</td> @if (in_array('price',$permissions))<td>"+data[1][i].price+"</td>@endif<td>"+data[1][i].quantity+"</td><td>"+data[1][i].delivered_quantity+"</td><td>"+data[1][i].returnQty+"</td> @if (in_array('percent',$permissions))<td>"+data[1][i].discount_percent+"</td> @endif  @if (in_array('amount',$permissions))<td>"+data[1][i].discounted_amount+"</td> @endif  @if (in_array('subtotal',$permissions))<td>"+data[1][i].sub_total+"</td> @endif </tr>");
                            }
                            else
                            {
                                $("#example1").append("<tr><td>"+data[1][i].id+"</td><td>"+data[1][i].products.pro_code+" - "+data[1][i].products.pro_name+"</td><td>"+data[1][i].products.unit.u_name+"</td><td>"+data[1][i].products.brands.b_name+"</td> @if (in_array('price',$permissions))<td>"+data[1][i].price+"</td> @endif <td>"+data[1][i].quantity+"</td><td>"+data[1][i].delivered_quantity+"</td><td>"+data[1][i].returnQty+"</td> @if (in_array('percent',$permissions)) <td>"+data[1][i].discount_percent+"</td> @endif @if (in_array('amount',$permissions)) <td>"+data[1][i].discounted_amount+"</td> @endif @if (in_array('subtotal',$permissions)) <td>"+data[1][i].sub_total+"</td> @endif </tr>");
                            }
                        }
                        $('#myModal').modal("show");
                        $('.action').val('Actions');
                        $('#example1').DataTable();

                    }
                });
            }
            if(val == 'View Payment')//
            {
                var id=$(this).attr("id");
                $("#example2 tbody").empty();
                $.ajax({
                    url:"{{url('')}}/transaction/view",
                    method:"GET",
                    data:
                    {
                        p_s_id:id,
                        p_type:'Sales',
                    },
                    error: function (request, error) {
                        lert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        for (let i = 0; i < data.length; i++) {
                            date = moment(data[i].created_at).format('MM/DD/YYYY');
                            $("#example2").append("<tr><td>"+data[i].id+"</td><td>"+date+"</td><td>"+data[i].paid_by+"</td><<td>"+data[i].total+"</td> @if(in_array('Download Invoice',$permissions)) <td> <button type='button' class='btn green invoice' id="+data[i].id+"><i class='fa fa-download'></i></button></td> @else <td> <button type='button' disabled class='btn green invoice' id="+data[i].id+"><i class='fa fa-download'></i></button></td> @endif</tr>");
                        }
                        $('#transaction').modal("show");
                        $('.action').val('Actions');
                        $('#example2').DataTable();

                    }
                });
            }
            if(val == 'Sale Return')
            {
                var id=$(this).attr("id");
                $('#w_id2').val('');
                $('#return').find("select").val('').end();
                $("#example8 tbody").empty();
                $.ajax({
                    url:"{{url('')}}/sales/"+id,
                    method:"GET",
                    error: function (request, error) {
                        lert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        var a =2;
                        for (let i = 0; i < data[1].length; i++) {
                             max= data[1][i].delivered_quantity;

                            if(data[1][i].quantity == data[1][i].returnQty)
                            {
                            }
                            else
                            {
                                if(data[1][i].type == 1)
                                {
                                    $("#example8").append("<tr><td> <input type='text' style='background-color: transparent;border: 0px solid;' readonly name='id[]' value='"+data[1][i].id+"'><input type='hidden' style='background-color: transparent;border: 0px solid;' readonly name='p_id[]' value='"+data[1][i].variant.id+"'></td><td>"+data[1][i].variant.name+"</td><input type='hidden' name='type[]' value='"+data[1][i].type+"'><td><input type='text' style='background-color: transparent;border: 0px solid;' class='q"+a+"' readonly name='quantity[]' value='"+data[1][i].quantity+"'></td><td>"+data[1][i].delivered_quantity+"</td><td><input type='number' max='"+max+"' value='1' min='0' required class='form-control rcv quantitys"+a+"' name='returnQty[]'></td></tr>");
                                }
                                else
                                {
                                    $("#example8").append("<tr><td> <input type='text' style='background-color: transparent;border: 0px solid;' readonly name='id[]' value='"+data[1][i].id+"'><input type='hidden' style='background-color: transparent;border: 0px solid;' readonly name='p_id[]' value='"+data[1][i].p_id+"'></td><td>"+data[1][i].products.pro_name+" - "+data[1][i].products.pro_code+"</td><input type='hidden' name='type[]' value='"+data[1][i].type+"'><td><input type='text' style='background-color: transparent;border: 0px solid;' class='q"+a+"' readonly name='quantity[]' value='"+data[1][i].quantity+"'></td><td>"+data[1][i].delivered_quantity+"</td><td><input type='number' max='"+max+"' value='1' min='0' required class='form-control rcv quantitys"+a+"' name='returnQty[]'></td></tr>");
                                }
                            }
                            a++;
                        }
                        // // console.log(a);
                        $('#w_id2').val(data[0].w_id);
                        $('#s_id2').val(id);

                        // console.log(amount);
                        $('#return').modal("show");
                        $('.action').val('Actions');
                        $('#example8').DataTable();

                    }
                });
            }
        });
    });
    $(document).on('click','.invoice',function(){
        var id=$(this).attr("id");
        $.ajax({
            url:"{{url('')}}/sales/invoice/"+id,
            method:"GET",
            data:
            {
                id:id,
            },
            error: function (request, error) {
                alert(" Can't do because: " + error +request);
            },
            success:function(data){
                console.log(data);
                window.location.href= '{{url('')}}/sales/invoice/'+id;
                // location.reload();
                $('.action').val('Actions');
            }
        });
    });




    $(document).on('click','#delivered',function(){
        $('#delivery').modal("hide");
    });
</script>

    @endsection
@endsection
