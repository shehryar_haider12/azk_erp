<html>
  <head>
    {{-- <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> --}}
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
      .attendance-table table{
        width: 88%;
        border-collapse: collapse;
        border: 1px solid #000;
        margin-left: 100px;
        font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .blank-cell{

        min-width: 50px;


        }

        .attendance-cell{

        padding: 4px;
        /* border: 1px solid #000; */

        }

        .attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
            border: 1px solid #000;
        }
        h2,h4 {
            color: #00CCFF;
        }
        .table-inv
        {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            border-collapse: collapse;
            width: 50%;
        }
        .table-inv thead
        {
            background-color: #ADD8E6;
        }
        .table-inv td,th
        {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        .row
        {
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            padding-left: 30px;
        }
        .col
        {
            font-size: 15px;
            background: #28AE01;
            color: #000;
            text-align: left;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            width: 20%;
            /* height: 28px; */
        }
        header
        {
            margin-left: 150px;
        }
        .label
        {
            background: #28AE01;
            font-size: 15px;
            text-align: center;
            color: #000;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            width: 60%;
            margin-left: 200px;
            /* padding-left: 30px; */
        }
        @page
        {
            margin: 0;
            size: A3;
        }
        #overlay
        {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background-image: url('{{url('')}}/uploads/CN7R2pF2hxI5A9KsFYNhsFSJ2HYFi0bsNDjt016D.jpg');
            background-position: center top;
            background-repeat: no-repeat;
            z-index: -1;
        }
  </style>
  <body>
        <header>
            <img src="{{url('')}}/uploads/logo-1.png" height="50px" width="800px" alt="logo" class="logo-default"  />
        </header>
        <br>
    <div class="label col-sm-12">
        <label>
            <b>Sales Invoice</b>
        </label>
        {{-- <label><b>Reference#: </b>{{$sales->ref_no}}</label> --}}
        {{-- <br>
        @php
            $date=Carbon\Carbon::parse($sales->sale_date)->format('d-m-y');
        @endphp
        <label><b>Sale Date: </b>{{$date}}</label>
        <br>
        <label><b>Sale#: </b>{{$sales->id}}</label>
        <br>
        <label><b>Customer ID: </b>{{$sales->customer->id}}</label>
        <br>
        <label><b>Terms: </b>{{$sales->note}}</label>
        <br>
        <label><b>Sale Person Name: </b>{{$sales->saleperson->name}}</label>
        <br>
        <label><b>Remaining Balance: </b>{{$balance}}</label> --}}
    </div>
    <br>
    <div class="form-body">
        <div class="row col" style="margin-left: 200px;">
            <div class="form-outline">
                <label><b>Customer Details:</b></label>
            </div>
        </div>
        <div class="row col" style="margin-left: 620px; margin-top: -20px">
            <div class="form-outline">
                <label><b>Company Details:</b></label>
            </div>
        </div>
        <div class="row">
            <div>
                <div class="form-outline" style="margin-left: 180px;">
                    <p><b>Customer Id: {{ucwords($sales->customer->id)}}</b></p>
                    <p><b>Name: {{ucwords($sales->customer->name)}}</b></p>
                    <p><b>Number: {{ucwords($sales->customer->c_no)}}</b></p>
                    <p><b>Address: {{ucwords($sales->s_address)}}</b></p>
                    <p><b>Area: {{ucwords($sales->customer->a_id == null ? 'Noarea' : $sales->customer->area->name )}}</b></p>
                </div>

            </div>
            <div style="margin-top: -200px; margin-left: 600px">
                <div class="form-outline">
                    <p><b>Name: Mungalo</b></p>
                    <p><b>Number: 03332918981</b></p>
                    <p><b>Date: {{ucwords($sales->sale_date)}}</b></p>
                    <p><b>Invoice No: {{ucwords($sales->id)}}</b></p>
                    <p><b>Sales-Rep: {{$sales->saleperson->name}}</b></p>
                </div>
            </div>
        </div>
    </div>
    <div class="attendance-table" >
        <table class="table table-striped table-bordered">

            <thead style="color:black;background: #28AE01;">
                <tr>
                    <th class="attendance-cell"  width="5%">S.No</th>
                    <th class="attendance-cell"  width="30%">Name</th>
                    {{-- <th class="attendance-cell"  width="10%">Weight</th> --}}
                    {{-- <th class="attendance-cell"   width="15%">Brand</th> --}}
                    <th class="attendance-cell"   width="7%">Price</th>
                    <th class="attendance-cell"  width="7%">Quantity</th>
                    <th class="attendance-cell"  width="20%">Discounted Amount</th>
                    <th class="attendance-cell"  width="7%">Sub total</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $amount=0;
                    $total=0;
                    $tax=0;
                @endphp
                @foreach($saledetail as $d)
                    <tr>
                        <td class="attendance-cell" >{{$d->id}}</td>
                        @if ($d->type == 1)
                            <td class="attendance-cell" >{{$d->variant->name}}</td>
                            {{-- <td class="attendance-cell" >{{$d->variant->product->brands->b_name}}</td> --}}
                        @else
                         <td class="attendance-cell" >{{$d->products->pro_code}} - {{$d->products->pro_name}}</td>
                         {{-- <td class="attendance-cell" >{{$d->products->brands->b_name}}</td> --}}
                        @endif
                        {{-- <td class="attendance-cell" >{{$d->products->weight}} {{$d->products->unit->u_name}}</td> --}}
                        <td class="attendance-cell" >{{$d->price}}</td>
                        <td class="attendance-cell" >{{$d->quantity}}</td>
                        <td class="attendance-cell" >{{$d->discounted_amount}}</td>
                        <td class="attendance-cell" >{{$d->sub_total}}</td>
                    </tr>
                    @php
                        $amount+=$d->sub_total;
                        $total+=$d->sub_total;
                    @endphp
                @endforeach
                <tr >
                    <td class="attendance-cell"  colspan="4"></td>
                    <td class="attendance-cell" ><b>SUB TOTAL</b></td>
                    <td class="attendance-cell" >{{$amount}}</td>
                </tr>
                @if ($sales->tax!=null)
                    <tr>
                        <td class="attendance-cell"  colspan="4"></td>
                        <td class="attendance-cell" ><b>TAX</b></td>
                        <td class="attendance-cell" >{{$sales->tax}}</td>
                    </tr>
                    @php
                        $tax=(($amount * $sales->tax)/100);
                        $total += $tax;
                    @endphp
                @endif
                @if ($sales->tax==null && $sales->discount==null)
                    @php
                        $total = $amount;
                    @endphp
                @endif
                <tr>
                    <td class="attendance-cell"  colspan="4"></td>
                    <td class="attendance-cell" ><b>TOTAL</b></td>
                    <td class="attendance-cell" >{{$total}}</td>
                </tr>
                <tr>
                    <td class="attendance-cell"  colspan="4"></td>
                    <td class="attendance-cell" ><b>PAID AMOUNT</b></td>
                    <td class="attendance-cell" >{{$th}}</td>
                </tr>
                <tr>
                    <td class="attendance-cell"  colspan="4"></td>
                    <td class="attendance-cell" ><b>LAST PAYABLE</b></td>
                    <td class="attendance-cell" >{{$balance}}</td>
                </tr>
                <tr>
                    <td class="attendance-cell"  colspan="4"></td>
                    <td class="attendance-cell" ><b>BALANCE</b></td>
                    <td class="attendance-cell" >{{$total - $th + $balance}}</td>
                </tr>
            </tbody>

        </table>

    </div>
  </body>
</html>
