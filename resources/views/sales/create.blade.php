@extends('layouts.master')
@section('top-styles')
@toastr_css
<style>
    thead {
	    background-color: #5F9EA0;
    }
    .modal-header{
        background: #5F9EA0 !important;
    }
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
@section('sidebar-name1')
<li class="breadcrumb-item"><a href="{{ url('')}}/sales">Sales</a></li>
<li class="breadcrumb-item"><span>{{$isEdit ? 'Edit' : 'Add'}} Sales</span></li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="card" id="form_wizard_1">
                <div class="card-header">
                    <div class="caption">
                        <i class=" fa fa-credit-card font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Sales</span>
                    </div>

                </div>
                <div class="card-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('sales.update',$sale->id) : route('sales.store')}} " class="form-horizontal" method="POST" enctype="multipart/form-data" id="saleForm">
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Sale Number</label>
                                        <input value="{{$id}}" class="form-control" type="text" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Sale Date<span class="text-danger">*</span></label>
                                        <input value="{{ $sale->sale_date ?? $date ??  old('sale_date') }}" class="form-control" type="date" placeholder="Enter Sale Date" name="sale_date" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Reference Number<span class="text-danger">*</span></label>
                                        <input value="{{ $sale->ref_no ??  old('ref_no')}}" class="form-control" type="text" placeholder="Enter Reference Number" name="ref_no" >
                                        <span class="text-danger">{{$errors->first('ref_no') ? 'Reference Number already exist OR Enter reference no' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Warehouse<span class="text-danger">*</span></label>
                                        <select class="form-control js-example-basic-single" name="w_id" id="w_id" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                            @foreach ($ware as $s)
                                            <option {{$s->id == $sale->w_id ? 'selected' : $s->id}} value="{{$s->id}}">{{$s->w_name}} - {{$s->w_type}}</option>
                                            @endforeach
                                        @else
                                            @foreach ($ware as $s)
                                            <option value="{{$s->id}}" {{$s->id == old('w_id') ? 'selected' : null}}>{{$s->w_name}} - {{$s->w_type}}</option>
                                            @endforeach
                                        @endif
                                        </select>
                                        <span class="text-danger">{{$errors->first('w_id') ? 'Select anyone' : null}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="{{in_array('Add Customer',$permissions) ? 'col-sm-5' : 'col-sm-6'}}">
                                    <div class="form-outline">
                                        <label >Customer<span class="text-danger">*</span></label>
                                        <select class="form-control js-example-basic-single" id="cm_id" name="c_id" >
                                            <option value="" disabled selected>Select...</option>
                                            @if (in_array('Add Customer',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                            @foreach ($customer as $s)
                                            <option {{$s->id == $sale->c_id ? 'selected' : $s->id}} value="{{$s->id}}">{{$s->name}}</option>
                                            @endforeach
                                            @else
                                            @foreach ($customer as $s)
                                            <option value="{{$s->id}}" {{$s->id == old('c_id') ? 'selected' : null}}>{{$s->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <span class="text-danger">{{$errors->first('c_id') ? 'Select anyone' : null}}</span>
                                    </div>
                                </div>
                                @if ( in_array('Add Customer',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 30px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#customerModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Address From<span class="text-danger">*</span></label>
                                        <select class="form-control js-example-basic-single"  id="address" name="address_type">
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                                <option {{$sale->address_type == 'Current' ? 'selected' : null}} value="Current">Current</option>
                                                <option {{$sale->address_type == 'Shipment' ? 'selected' : null}} value="Shipment">Shipment</option>
                                                @else
                                                <option value="Current" {{old('address_type') == 'Current' ? 'selected' : null}}>Current</option>
                                                <option value="Shipment" {{old('address_type') == 'Shipment' ? 'selected' : null}}>Shipment</option>
                                            @endif
                                        </select>
                                        <span class="text-danger">{{$errors->first('address_type') ? 'Select anyone' : null}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Current Address<span class="text-danger">*</span></label>
                                        <input class="form-control" type="text" name="c_address" {{!$isEdit ? '' : ($sale->address_type == 'Current' ? '' : 'readonly') }} id="c_address" value="{{!$isEdit ? old('c_address')  :  ($sale->address_type == 'Current' ? $sale->s_address : '')}}" >

                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Shipment Address <small>(optional)</small></label>
                                        <input class="form-control" type="text" name="s_address" id="s_address" {{$isEdit ? '' :  'readonly'}}   {{$isEdit ? ($sale->address_type == 'Shipment' ? '' : 'readonly') : ''}} id="s_address" value="{{$isEdit ? ($sale->address_type == 'Shipment' ? $sale->s_address : '') : old('s_address')}}">

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Expected Delivery Date</label>
                                        <input class="form-control" value="{{$sale->expected_date ??  old('expected_date') }}"  type="date"  name="expected_date">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Attach Document <small>(optional)</small></label>
                                        <input class="form-control" type="file" placeholder="Enter Product Name" name="doc" accept=".pdf, .docx, .txt">
                                    </div>
                                </div>
                                @if ($isEdit)
                                    @if ($sale->doc != null)
                                        <div class="row">
                                            <div class="col-sm-6"></div>
                                            <div class="col-sm-5">
                                                <input type="hidden" name="doc1" value="{{$sale->doc}}">
                                                <label style="margin-top: 10px" ><b> Document1 </b></label>
                                            </div>
                                            <div class="col-sm-1">
                                                <a href="{{route('sales.document',$sale->id)}}" class='btn green invoice' style="margin-top: 10px" ><i class='fa fa-download'></i></a>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                                <div class="{{in_array('Add Biller',$permissions) ? 'col-sm-5' : 'col-sm-6'}}">
                                    <div class="form-outline">
                                        <label >Accountant<span class="text-danger">*</span></label>
                                        <select class="form-control js-example-basic-single" id="b_id"  name="b_id" >
                                            <option value="" disabled selected>Select...</option>
                                            @if (in_array('Add Biller',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                            @foreach ($biller as $s)
                                            <option {{$s->id == $sale->b_id ? 'selected' : $s->id}} value="{{$s->id}}">{{$s->name}}</option>
                                            @endforeach
                                            @else
                                            @foreach ($biller as $s)
                                            <option value="{{$s->id}}"  {{$s->id == old('b_id') ? 'selected' : null}}>{{$s->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <span class="text-danger">{{$errors->first('b_id') ? 'Select anyone' : null}}</span>
                                    </div>
                                </div>
                                @if (in_array('Add Biller',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 30px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#billerModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="{{in_array('Add Sale Person',$permissions) ? 'col-sm-5' : 'col-sm-6'}}">
                                    <div class="form-outline">
                                        <label >Sale Person<span class="text-danger">*</span></label>
                                        <select class="form-control js-example-basic-single" id="sp_id" name="sp_id" >
                                            <option value="" disabled selected>Select...</option>
                                            @if (in_array('Add Sale Person',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                            @foreach ($person as $s)
                                            <option {{$s->id == $sale->sp_id ? 'selected' : $s->id}} value="{{$s->id}}">{{$s->name}}</option>
                                            @endforeach
                                            @else
                                            @foreach ($person as $s)
                                            <option value="{{$s->id}}"  {{$s->id == old('sp_id') ? 'selected' : null}}>{{$s->name}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                        <span class="text-danger">{{$errors->first('sp_id') ? 'Select anyone' : null}}</span>
                                    </div>
                                </div>
                                @if (in_array('Add Sale Person',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 30px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#personModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif



                                <div class="{{ in_array('Add Product',$permissions) ? 'col-sm-10' : 'col-sm-11'}}">
                                    <div class="form-outline">
                                        <label >Products<span class="text-danger">*</span></label>
                                        <select class="form-control js-example-basic-single"  id="p_id" >
                                            @if ($isEdit)
                                                <option disabled selected>Select..</option> --}}
                                                 @foreach ($product as $s)
                                                    @if ($s->type == 0)
                                                        <option value="{{$s->p_id.'.'.$s->type}}">{{$s->products->pro_code.' - '.$s->products->pro_name.' - '.$s->products->unit->u_name.' - '.$s->products->brands->b_name}}</option>
                                                    @else
                                                        <option value={{$s->variant->id.'.'.$s->type}}>{{$s->variant->name}}</option>
                                                    @endif
                                                @endforeach

                                            @endif
                                        </select>
                                        <span class="text-danger">{{$errors->first('p_id') ? 'Select anyone' : null}}</span>
                                    </div>
                                </div>
                                @if ( in_array('Add Product',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 30px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#productModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-1">
                                    <div class="form-control" style="margin-top: 30px;  width:50px">
                                        <a id="all">
                                            <i class="fa fa-2x fa-list addIcon font-green"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example" class="table table-striped table-bordered table-responsive" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Code - Name - Weight - Brand - Category</th>
                                                @if (in_array('show cost',$permissions))
                                                <th>Cost</th>
                                                @endif
                                                <th>Rate</th>
                                                <th>Available Quantity</th>
                                                <th>Quantity</th>
                                                <th>Discount In %</th>
                                                <th>Discount In Amount</th>
                                                <th>Sub total</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $a=1;
                                                $subtotal=0;
                                                $quantity=0;
                                            @endphp
                                            @if ($isEdit)
                                                @foreach ($saledetail as $p)
                                                    @if ($p->type == 1)
                                                        @php
                                                            $currentstock = \App\ProductVariants::with(['currentstocks' => function($query) use ($sale) {
                                                                $avg = $query->select(DB::raw('*'))->where('w_id',$sale->w_id);
                                                            }])
                                                            ->where('id',$p->p_id)->first();
                                                            $subtotal+=$p->sub_total;
                                                        @endphp
                                                        <tr>
                                                            <td><input name="p_id[]" type="text" class="form-control" readonly value="{{$p->p_id}}"></td>
                                                            <td>{{$p->variant->name}} - {{$p->variant->product->unit->u_name}} - {{$p->variant->product->brands->b_name}} - {{$p->variant->product->category->cat_name}}</td>
                                                            @if (in_array('show cost',$permissions))
                                                            <td><input type="text" readonly class="form-control costs cost{{$a}}" min="1" id="cost{{$a}}" data-id="{{$a}}" name="cost[]" value="{{$p->variant->cost}}"></td>
                                                            @endif
                                                            <td><input type="text" class="form-control price price{{$a}}" min="1" id="price{{$a}}" data-id="{{$a}}" name="price[]" value="{{$p->price}}"></td>
                                                            <td><input type="text" class="form-control" readonly value="{{$currentstock->currentstocks[0]->quantity}}"></td>
                                                            <td>
                                                                <input type="hidden" name="type[]" value="{{$p->type}}">
                                                                <input type="text" class="form-control quantity" min="1" max="{{$currentstock->currentstocks[0]->quantity}}" id="{{$a}}" name="quantity[]" value="{{$p->quantity}}"></td>
                                                            <td><input type="text" data-id="{{$a}}" class="form-control discountp percent{{$a}}"  id="percent{{$a}}" name="discount_percent[]" value="{{$p->discount_percent}}"></td>
                                                            <td><input type="text" data-id="{{$a}}" class="form-control discountr rate{{$a}}"  id="rate{{$a}}" name="discounted_amount[]" value="{{$p->discounted_amount}}"></td>
                                                            <td><input type="text" readonly class="form-control abc sub{{$a}}" name="sub_total[]" id="sub{{$a}}" value="{{$p->sub_total}}"></td>
                                                            <td><button type="button" id="{{$a}}" class="btn red delete"><i class="fa fa-trash"></i></button></td>
                                                        </tr>
                                                    @else
                                                        @php
                                                            $currentstock = \App\Products::with(['currentstocks' => function($query) use ($sale) {
                                                                $avg = $query->select(DB::raw('*'))->where('w_id',$sale->w_id);
                                                            }])
                                                            ->where('id',$p->p_id)->first();
                                                            $subtotal+=$p->sub_total;
                                                            // dd($currentstock);
                                                        @endphp
                                                         <tr>
                                                            <td><input name="p_id[]" type="text" class="form-control" readonly value="{{$p->p_id}}"></td>
                                                            <td>{{$p->products->pro_code}} - {{$p->products->pro_name}} - {{$p->products->unit->u_name}} - {{$p->products->brands->b_name}} - {{$p->products->category->cat_name}}</td>
                                                            @if (in_array('show cost',$permissions))
                                                            <td><input type="text" readonly class="form-control costs cost{{$a}}" min="1" id="cost{{$a}}" data-id="{{$a}}" name="cost[]" value="{{$p->products->cost}}"></td>
                                                            @endif
                                                            <td><input type="text" class="form-control price price{{$a}}" min="1" id="price{{$a}}" data-id="{{$a}}" name="price[]" value="{{$p->price}}"></td>
                                                            <td><input type="text" class="form-control" readonly value="{{$currentstock->currentstocks[0]->quantity}}"></td>
                                                            <td>
                                                                <input type="hidden" name="type[]" value="{{$p->type}}">
                                                                <input type="text" class="form-control quantity" min="1" max="{{$currentstock->currentstocks[0]->quantity}}" id="{{$a}}" name="quantity[]" value="{{$p->quantity}}"></td>
                                                            <td><input type="text" data-id="{{$a}}" class="form-control discountp percent{{$a}}"  id="percent{{$a}}" name="discount_percent[]" value="{{$p->discount_percent}}"></td>
                                                            <td><input type="text" data-id="{{$a}}" class="form-control discountr rate{{$a}}"  id="rate{{$a}}" name="discounted_amount[]" value="{{$p->discounted_amount}}"></td>
                                                            <td><input type="text" readonly class="form-control abc sub{{$a}}" name="sub_total[]" id="sub{{$a}}" value="{{$p->sub_total}}"></td>
                                                            <td><button type="button" id="{{$a}}" class="btn red delete"><i class="fa fa-trash"></i></button></td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endif

                                        </tbody>


                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                {{-- <div class="col-sm-6">
                                    <label for=""><b>TOTAL PRICE:</b> </label>
                                    <label id="total">0</label>
                                </div> --}}
                                <div class="col-sm-6">
                                    <label for=""><b>TOTAL AMOUNT:</b> </label>
                                    <label hidden id="amount">{{$isEdit ? $subtotal : '0'}}</label>
                                    <input readonly type="text" style="border: 0px" name="final" id="final" value="{{ $isEdit ? $sale->total : 0}}">
                                </div>
                            </div>
                            <input type="hidden" name="l_amount" id="l_amount">
                            <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-outline">
                                            <label >Sales Tax Status<span class="text-danger">*</span></label>
                                            <select class="form-control " name="tax_status" required id="tax_status">
                                                <option value="" disabled selected>Select...</option>

                                                @if ($isEdit)
                                                <option {{$sale->tax_status == 'No' ? 'selected' : null}} >No</option>
                                                <option {{$sale->tax_status == 'Yes' ? 'selected' : null}} >Yes</option>
                                                @else
                                                <option {{old('tax_status') == 'Yes' ? 'No' : null}}>No</option>
                                                <option {{old('tax_status') == 'Yes' ? 'selected' : null}}>Yes</option>
                                                @endif
                                            </select>
                                            {{-- <span class="text-danger">{{$errors->first('tax_status') ? 'Select anyone' : null}}</span> --}}
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-outline">
                                            <label >Discount Type<small>(optional)</small></label>
                                            <select class="form-control" name="discount_type" id="discount_type" >
                                                <option value="" disabled selected>Select...</option>
                                                @if ($isEdit)
                                                <option {{$sale->discount_type == 'Trade' ? 'selected' : null}} value="Trade">Trade</option>
                                                <option {{$sale->discount_type == 'Cash' ? 'selected' : null}} value="Cash">Cash</option>
                                                @else
                                                <option value="Trade" {{old('discount_type') == 'Trade' ? 'selected' : null}}>Trade</option>
                                                <option value="Cash"  {{old('discount_type') == 'Cash' ? 'selected' : null}}>Cash</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                @if (in_array('Tax',$permissions))
                                    <div class="col-sm-6">
                                        <div class="form-outline">
                                            <label >Sales Tax <small>In %</small></label>
                                            <input readonly value="{{$isEdit ? $sale->tax : ''}}" class="form-control" type="text" min="0" placeholder="Enter Sales Tax" name="tax" id="tax">
                                            {{-- <span class="text-danger">{{$errors->first('tax') ? 'Enter Tax' : null}}</span> --}}
                                        </div>
                                    </div>

                                @endif
                                    <div class="col-sm-6">
                                        <div class="form-outline">
                                            <label >Advance Payment<span class="text-danger">*</span></label>
                                            <select class="form-control " name="advance" id="adv" >
                                                <option value="" disabled selected>Select...</option>
                                                @if ($isEdit)
                                                <option {{$sale->advance == 'Yes' ? 'selected' : null}} value="Yes">Yes</option>
                                                <option {{$sale->advance == 'No' ? 'selected' : null}} value="No">No</option>
                                                @else
                                                <option value="Yes" {{old('adv') == 'Yes' ? 'selected' : null}}>Yes</option>
                                                <option value="No" {{old('adv') == 'No' ? 'selected' : null}}>No</option>
                                                @endif
                                            </select>
                                            <span class="text-danger">{{$errors->first('advance') ? 'Select anyone' : null}}</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-outline">
                                            <label >Payment Term Type<span class="text-danger">*</span></label>
                                            <select class="form-control" name="pay_type" id="pay_type" required>
                                                <option value=""  selected>Select...</option>
                                                @if ($isEdit)
                                                <option {{$sale->pay_type == 'Bill To Bill' ? 'selected' : null}} value="Bill To Bill">Bill To Bill</option>
                                                <option {{$sale->pay_type == 'Days' ? 'selected' : null}} value="Days">Days</option>
                                                <option {{$sale->pay_type == 'Cash on Delivery' ? 'selected' : null}} value="Cash on Delivery">Cash on Delivery</option>
                                                @else
                                                <option value="Bill To Bill" {{old('pay_type') == 'Bill To Bill' ? 'selected' : null}}>Bill To Bill</option>
                                                <option value="Days" {{old('pay_type') == 'Days' ? 'selected' : null}}>Days</option>
                                                <option value="Cash on Delivery" {{old('pay_type') == 'Cash on Delivery' ? 'selected' : null}}>Cash on Delivery</option>
                                                @endif
                                            </select>
                                            {{-- <span class="text-danger">{{$errors->first('pay_type') ? 'Select anyone' : null}}</span> --}}
                                        </div>
                                    </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Payment Term<span class="text-danger">*</span></label>
                                        <input class="form-control" readonly type="text" id="note" name="editor1" value="{{$isEdit ?  $sale->note : ''}}">
                                        <span class="text-danger">{{$errors->first('eeditor1') ? 'Enter Days' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Payment Status<span class="text-danger">*</span></label>
                                        <select class="form-control" {{$isEdit ? '' : 'disabled'}} name="p_status" id="p_status" required>
                                            <option value=""  selected>Select...</option>
                                            @if ($isEdit)
                                            <option {{$sale->p_status == 'Pending' ? 'selected' : null}} value="Pending">Pending</option>
                                            <option {{$sale->p_status == 'Partial' ? 'selected' : null}} value="Partial">Partial</option>
                                            <option {{$sale->p_status == 'Paid' ? 'selected' : null}} value="Paid">Paid</option>
                                            @else
                                            {{-- <option value="Pending">Pending</option> --}}
                                            <option value="Partial">Partial</option>
                                            <option value="Paid">Paid</option>
                                            @endif
                                        </select>
                                        {{-- <span class="text-danger">{{$errors->first('p_status') ? 'Select anyone' : null}}</span> --}}
                                        <input type="hidden" name="p_status1" value="Pending">
                                    </div>
                                </div>

                            </div>
                            <br>
                            <div id="pending" class="tableview" {{$isEdit ? '' : 'hidden'}}>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-outline">
                                            <label >Amount</label>
                                            <input readonly value="{{$isEdit ? $sale->total : ''}}" class="form-control" type="text" min="0"  name="p_total" id="p_total">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="payment" class="tableview" hidden>
                                <div class="row">
                                    {{-- <div class="col-sm-4">
                                        <div class="form-outline">
                                            <label >Payment Reference Number</label>
                                            <input class="form-control" type="text" min="0" placeholder="Enter Payment Reference Number" name="p_ref_no" id="p_ref_no">
                                        </div>
                                    </div> --}}
                                    <div class="col-md-4">
                                        <div class="form-outline">
                                            <label >Amount<span class="text-danger">*</span></label>
                                            <input  value="" class="form-control" type="text"  name="p_total1" id="p_total1">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-outline">
                                            <label >Paying By<span class="text-danger">*</span></label>
                                            <select class="form-control " name="paid_by" id="paid_by" >
                                                <option value="" disabled selected>Select...</option>
                                                <option value="Cash">Cash</option>
                                                <option value="Gift">Gift Card</option>
                                                <option value="Credit">Credit Card</option>
                                                <option value="Cheque">Cheque</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div id="cheque" style="display: none">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-outline">
                                                <label >Cheque Number<span class="text-danger">*</span></label>
                                                <input class="form-control" type="text" placeholder="Enter Cheque Number"  name="cheque_no">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-outline">
                                                <label >Bank<span class="text-danger">*</span></label>
                                                <select class="form-control js-example-basic-single" id="bank_id" name="bank_id" >
                                                    <option value="" disabled selected>Select...</option>
                                                        @foreach ($bank as $s)
                                                        <option value="{{$s->id}}">{{$s->name}} - {{$s->branch}}</option>
                                                        @endforeach
                                                        <option>other</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="gift" style="display: none">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-outline">
                                                <label >Gift Card Number<span class="text-danger">*</span></label>
                                                <input class="form-control" type="text" placeholder="Enter Gift Card Number"  name="gift_no">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="credit" style="display: none">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-outline">
                                                <label >Credit Card Number<span class="text-danger">*</span></label>
                                                <input class="form-control" type="text" placeholder="Enter Credit Card Number"  name="cc_no">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-outline">
                                                <label >Holder Name<span class="text-danger">*</span></label>
                                                <input class="form-control" type="text" placeholder="Enter Holder Name"  name="cc_holder">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-outline">
                                                <label >Bank<span class="text-danger">*</span></label>
                                                <select class="form-control " id="bank_id" data-live-search="true" name="bank_id" >
                                                    <option value="" disabled selected>Select...</option>
                                                        @foreach ($bank as $s)
                                                        <option value="{{$s->id}}">{{$s->name}} - {{$s->branch}}</option>
                                                        @endforeach
                                                        <option>other</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-outline">
                                            <label >Note <small>(optional)</small></label>
                                            <br>
                                            <textarea name="editor2" id="editor2" rows="5" cols="60">
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
@section('modal')
    @include('modals.product')
    @include('modals.biller')
    @include('modals.bank')
    @include('modals.saleperson')
    @include('modals.customer')
    @include('modals.allproducts')
@endsection

@endsection
@section('custom-script')
@toastr_js
@toastr_render
<script>
    $('#saleForm').validate({
        rules: {
            ref_no: {
                required: true,
            },
            w_id: {
                required: true,
            },
            c_id: {
                required: true,
            },
            address_type: {
                required: true,
            },
            c_address: {
                required: true,
            },
            b_id: {
                required: true,
            },

            sp_id: {
                required: true,
            },
            tax_status: {
                required: true,
            },
            advance: {
                required: true,
            },
            pay_type: {
                required: true,
            },

        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>
<script>

    $(document).on('change','#address',function(){
        var val = $(this).val();
        if(val == 'Current')
        {
            $('#s_address').prop('readonly',true);
            // $('#s_address').prop('required',true);
            $('#c_address').prop('readonly',false);
            $('#c_address').prop('required',true);
        }
        else
        {
            $('#s_address').prop('readonly',false);
            $('#s_address').prop('required',true);
            $('#c_address').prop('readonly',true);
            $('#c_address').prop('required',false);
        }
    });

    $(document).on('change','#tax_status',function(){
        var val = $(this).val();
        if(val == 'Yes')
        {
            $('#tax').prop('readonly',false);
        }
        else
        {
            $('#tax').prop('readonly',true);
        }
    })

</script>
<script>
    $(document).on('change','#w_id',function(){
        var id = $(this).val();
        $("#p_id").empty();

        $.ajax({
            url:"{{url('')}}/stocks/search/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                $('#p_id').append('<option disabled selected>Select..</option>');
                for(var i = 0 ; i < data.length ; i++)
                {
                    if(data[i].type == 0)
                    {
                        if(data[i].products.weight == null)
                        {
                            weight='';
                        }
                        else
                        {
                            weight = data[i].products.weight ;
                        }
                        $("#p_id").append('<option value='+data[i].p_id+'.'+data[i].type+'>'+data[i].products.pro_code+' - '+data[i].products.pro_name+' - '+weight+data[i].products.unit.u_name+'- '+data[i].products.brands.b_name+' - '+data[i].products.category.cat_name+'</option>');
                    }
                    else
                    {
                        $("#p_id").append('<option value='+data[i].variant.id+'.'+data[i].type+'>'+data[i].variant.name+'</option>');
                    }
                }

            }
        });
    });

    $(document).on('change','#adv',function(){
        var val = $(this).val();
        console.log(val);
        if(val == 'Yes')
        {
            $('#p_status').prop('disabled',false);
        }
        else
        {
            $('#p_status').prop('disabled',true);
            $('#payment').prop('hidden',true);
        }
    });


    $(document).on('click','#all',function(){
        var table = $('#example6').DataTable();
        table.destroy();
        var w_id = $('#w_id').val();
        if(w_id == null)
        {
            alert('Select warehouse first');
        }
        else
        {
            $('#allproduct').modal("show");
            var table = $('#example6').DataTable({
                processing: true,
                serverSide: true,
                scrollX: true,
                ajax: '{{url('')}}/product/modal/'+w_id,
                "columns": [{
                        "data": "",
                        "defaultContent": "",
                        "render": function (data, type, row, meta) {
                            if (row.variant == null)
                            {
                                return  row.p_id;
                            }
                            else
                            {
                                return row.variant.id
                            }
                        },
                    },
                    {
                        "data": "pro_name",
                        "defaultContent": "",
                        "render": function (data, type, row, meta) {
                            if (row.variant == null)
                            {
                                return  row.products.pro_code+ ` - ` +row.products.pro_name;
                            }
                            else
                            {
                                return row.variant.name;
                            }
                        },
                    },
                    {
                        "data": "",
                        "defaultContent": "",
                        "render": function (data, type, row, meta) {
                            if (row.variant == null)
                            {
                                if(row.products.weight == null)
                                {
                                    return row.products.unit.u_name;
                                }
                                else
                                {
                                    return row.products.weight+row.products.unit.u_name;
                                }
                            }
                            else
                            {
                                if(row.variant.product.weight == null)
                                {
                                    return row.variant.product.unit.u_name;
                                }
                                else
                                {
                                    return row.variant.product.weight+row.variant.product.unit.u_name;
                                }
                            }

                        },
                    },
                    {
                        "data": "products.brands.b_name",
                        "defaultContent": "",
                        "render": function (data, type, row, meta) {
                            if (row.variant == null)
                            {
                                return  row.products.brands.b_name;
                            }
                            else
                            {
                                return row.variant.product.brands.b_name
                            }
                        },
                    },
                    {
                        "data": "products.category.cat_name",
                        "defaultContent": "",
                        "render": function (data, type, row, meta) {
                            if (row.variant == null)
                            {
                                return  row.products.category.cat_name;
                            }
                            else
                            {
                                return row.variant.product.category.cat_name
                            }
                        },
                    },
                    {
                        "data": "p_id",
                        "defaultContent": ""
                    },
                ],
                "columnDefs": [{
                        "targets": 'no-sort',
                        "orderable": false,
                    },
                    {
                        "targets": 0,
                        "render": function (data, type, row, meta) {
                            return meta.row + 1;
                        },
                    },
                    {
                        "targets": -1,
                        "render": function (data, type, row, meta) {
                            return ` <button type="button"  class="btn green add" id="add`+row.p_id +`.`+row.type+`">
                                <i class="fa fa-plus"></i>
                            </button>`;
                        },
                    },
                ],
            });
        }
    });
</script>
<script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
        $('.js-example-basic-multiple').select2();
        $('#b_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#billerModal').modal("show"); //Open Modal
            }
        });
        $('#bank_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#bankModal').modal("show"); //Open Modal
            }
        });
        $('#sp_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#personModal').modal("show"); //Open Modal
            }
        });
        $('#cm_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#customerModal').modal("show"); //Open Modal
            }
            else
            {
                $.ajax({
                    url:"{{url('')}}/customer/balance/"+opval,
                    method:"GET",
                    error: function (request, error) {
                                alert(" Can't do because: " + error +request);
                            },
                    success:function(data){
                        console.log(data);
                        if(data[0] == 0 && data[1] == 0)
                        {
                            $('#c_address').val(data[2].address);
                        }
                        else
                        {

                            if(data[0] < data[1])
                            {

                            }
                            else
                            {
                                alert('Sale Cannot be created of this customer, as its balance exceeds its limit.')
                            }
                        }
                    }
                });
            }
        });


        $('#example6').on('click','.add',function(){
            var rowCount = $('#example tr').length;
            var id=$(this).attr('id');
            id = id.substring(3);
            var w_id = $('#w_id').val();
            $.ajax({
                url:"{{url('')}}/product/salesProduct/"+id+'.'+w_id,
                method:"GET",
                error: function (request, error) {
                            alert(" Can't do because: " + error +request);
                        },
                success:function(data){
                    console.log(data);


                    if($('.'+data[0].id).length){
                        // console.log('dsd');
                    }
                    else
                    {
                        if(data[0].product == null)
                        {
                            if(data[0].total_quantity == null)
                            {
                                total_quantity=0;
                            }
                            else
                            {
                                total_quantity = data[0].total_quantity;
                            }

                            if(data[0].weight == null)
                            {
                                weight='';
                            }
                            else
                            {
                                weight = data[0].weight;
                            }
                            $("#example").append("<tr><td><input name='p_id[]' type='text' class='form-control "+data[0].id+"' readonly value='"+data[0].id+"'></td><td>"+data[0].pro_code+" - "+data[0].pro_name+" - "+weight+" - "+data[0].brands.b_name+" - "+data[0].category.cat_name+"</td>@if (in_array('show cost',$permissions))<td><input type='text' readonly class='form-control costs cost"+rowCount+"' min='1' id='cost"+rowCount+"' data-id='"+rowCount+"' name='cost[]' value='"+data[0].cost+"' readonly></td>@endif<td><input required type='text' class='form-control price price"+rowCount+"' min='1' id='price"+rowCount+"' data-id='"+rowCount+"' name='price[]' value='"+data[0].price+"'></td><td><input type='number' class='form-control' readonly  value='"+total_quantity+"'></td><td><input type='hidden' name='type[]' value='"+data[1]+"'><input type='number' class='form-control quantity' required min='1' id='"+rowCount+"' max='"+total_quantity+"' name='quantity[]' value='1'></td><td><input type='text' data-id='"+rowCount+"' class='form-control discountp percent"+rowCount+"' name='discount_percent[]' id='percent"+rowCount+"'></td><td><input type='text' data-id='"+rowCount+"' class='form-control discountr rate"+rowCount+"' name='discounted_amount[]' id='rate"+rowCount+"'></td><td><input type='text' readonly class='form-control abc sub"+rowCount+"' name='sub_total[]' id='sub"+rowCount+"' value='"+data[0].price+"'></td><td><button type='button' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                        }
                        else
                        {
                            console.log(data);
                            if(data[0].total_quantity == null)
                            {
                                total_quantity=0;
                            }
                            else
                            {
                                total_quantity = data[0].total_quantity;
                            }

                            $("#example").append("<tr><td><input name='p_id[]' type='text' class='form-control' readonly value='"+data[0].id+"'></td><td>"+data[0].name+" - "+data[0].product.unit.u_name+" - "+data[0].product.brands.b_name+" - "+data[0].product.category.cat_name+"</td>@if (in_array('show cost',$permissions))<td><input type='text' readonly class='form-control costs cost"+rowCount+"' min='1' id='cost"+rowCount+"' data-id='"+rowCount+"' name='cost[]' value='"+data[0].cost+"' readonly></td>@endif<td><input required type='text' class='form-control price price"+rowCount+"' min='1' id='price"+rowCount+"' data-id='"+rowCount+"' name='price[]' value='"+data[0].price+"'></td><td><input type='number' class='form-control' readonly  value='"+total_quantity+"'></td><td><input type='hidden' name='type[]' value='"+data[1]+"'><input type='number' class='form-control quantity' required min='1' id='"+rowCount+"' max='"+total_quantity+"' name='quantity[]' value='1'></td><td><input type='text' data-id='"+rowCount+"' class='form-control discountp percent"+rowCount+"' name='discount_percent[]' id='percent"+rowCount+"'></td><td><input type='text' data-id='"+rowCount+"' class='form-control discountr rate"+rowCount+"' name='discounted_amount[]' id='rate"+rowCount+"'></td><td><input type='text' readonly class='form-control abc sub"+rowCount+"' name='sub_total[]' id='sub"+rowCount+"' value='"+data[0].price+"'></td><td><button type='button' data-id='"+rowCount+"' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                        }
                        var amount=0;
                        var total=0;
                        var rowCount1 = $('#example tr').length;
                        for (let i = 1; i < rowCount1; i++) {
                            amount = +amount + +$('.sub'+i).val();
                            total = +total + +$('.price'+i).val();
                        }
                        $('#amount').html(amount);
                        $('#l_amount').val(amount);
                        $('#final').val(amount);
                        $('#p_total1').val(amount);

                    }
                }
            });
        });
    });
    $(document).on('change','#p_status',function(){
        var p=$(this).val();
        if(p == 'Partial')
        {
            $('#payment').prop('hidden',false);
            // $('#pending').hide();
            var amount = $('#final').val();
            $('#p_total1').val(amount);
            $('#p_total1').attr('max',amount);
            $('#p_total1').attr('min','1');
            $('#p_total1').attr('readonly',false);
        }
        if(p == 'Paid')
        {
            $('#payment').prop('hidden',false);
            // $('#pending').hide();
            var amount = $('#final').val();
            $('#p_total1').val(amount);
            $('#p_total1').attr('max',amount);
            $('#p_total1').attr('min',amount);
            $('#p_total1').attr('readonly',true);
        }

        if(p == 'Pending')
        {
            $('#payment').hide();
            $('#pending').show();
            var amount = $('#final').val();
            $('#p_total').val(amount);
            $('#p_total').attr('max',amount);
            $('#p_total').attr('min',amount);
            $('#p_total').attr('readonly',true);
        }
    });
    $(document).on('change','#paid_by',function(){
        var p=$(this).val();
        if(p == 'Cheque')
        {
            $('#cheque').show();
            $("#cheque_no").prop('required',true);
            $('#gift').hide();
            $("#gift_no").prop('required',false);
            $('#credit').hide();
            $("#cc_no").prop('required',false);
            $("#cc_holder").prop('required',false);
        }
        if(p == 'Gift')
        {
            $('#gift').show();
            $("#gift_no").prop('required',true);
            $('#cheque').hide();
            $("#cheque_no").prop('required',false);
            $('#credit').hide();
            $("#cc_no").prop('required',false);
            $("#cc_holder").prop('required',false);
        }
        if(p == 'Cash')
        {
            $('#gift').hide();
            $("#gift_no").prop('required',false);
            $('#cheque').hide();
            $("#cheque_no").prop('required',false);
            $('#credit').hide();
            $("#cc_no").prop('required',false);
            $("#cc_holder").prop('required',false);
        }
        if(p == 'Credit')
        {
            $('#gift').hide();
            $("#gift_no").prop('required',false);
            $('#cheque').hide();
            $("#cheque_no").prop('required',false);
            $('#credit').show();
            $("#cc_no").prop('required',true);
            $("#cc_holder").prop('required',true);
        }
    });

    $(document).on('change','#pay_type',function(){
        var p=$(this).val();
        console.log(p);
        if(p == 'Days')
        {
            $('#note').prop('readonly',false);
            $('#note').prop('required',true);
        }
        else
        {
            $('#note').prop('readonly',true);
            $('#note').prop('required',false);
        }
    });

    $(document).on('change','#p_id',function(){
        var rowCount = $('#example tr').length;
        var id=$(this).val();
        var w_id = $('#w_id').val();
        $.ajax({
            url:"{{url('')}}/product/salesProduct/"+id+'.'+w_id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);


                if($('#'+data[0].id).length){
                    // console.log('dsd');
                }
                else
                {
                    if(data[0].product == null)
                    {
                        if(data[0].total_quantity == null)
                        {
                            total_quantity=0;
                        }
                        else
                        {
                            total_quantity = data[0].total_quantity;
                        }

                        if(data[0].weight == null)
                        {
                            weight='';
                        }
                        else
                        {
                            weight = data[0].weight;
                        }


                        $("#example").append("<tr><td><input style='width: fit-content;' name='p_id[]' type='text' class='form-control' readonly value='"+data[0].id+"'></td><td>"+data[0].pro_code+" - "+data[0].pro_name+" - "+data[0].brands.b_name+" - "+data[0].category.cat_name+"</td>@if (in_array('show cost',$permissions))<td><input style='width: fit-content;' type='text' readonly class='form-control costs cost"+rowCount+"' min='1' id='cost"+rowCount+"' data-id='"+rowCount+"' name='cost[]' value='"+data[0].cost+"' readonly></td>@endif<td><input style='width: fit-content;' required type='text' class='form-control price price"+rowCount+"' min='1' id='price"+rowCount+"' data-id='"+rowCount+"' name='price[]' value='"+data[0].price+"'></td><td><input style='width: fit-content;' type='number' class='form-control' readonly  value='"+total_quantity+"'></td><td><input type='hidden' name='type[]' value='"+data[1]+"'><input style='width: fit-content;' type='number' class='form-control quantity' required min='1' id='"+rowCount+"' max='"+total_quantity+"' name='quantity[]' value='1'></td><td><input style='width: fit-content;' type='text' data-id='"+rowCount+"' class='form-control discountp percent"+rowCount+"' name='discount_percent[]' id='percent"+rowCount+"'></td><td><input style='width: fit-content;' type='text' data-id='"+rowCount+"' class='form-control discountr rate"+rowCount+"' name='discounted_amount[]' id='rate"+rowCount+"'></td><td><input style='width: fit-content;' type='text' readonly class='form-control abc sub"+rowCount+"' name='sub_total[]' id='sub"+rowCount+"' value='"+data[0].price+"'></td><td><button type='button' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                    }
                    else
                    {
                        console.log(data);
                        if(data[0].total_quantity == null)
                        {
                            total_quantity=0;
                        }
                        else
                        {
                            total_quantity = data[0].total_quantity;
                        }

                        $("#example").append("<tr><td><input name='p_id[]' type='text' class='form-control' readonly value='"+data[0].id+"'></td><td>"+data[0].name+" - "+data[0].product.unit.u_name+" - "+data[0].product.brands.b_name+" - "+data[0].product.category.cat_name+"</td>@if (in_array('show cost',$permissions))<td><input type='text' readonly class='form-control costs cost"+rowCount+"' min='1' id='cost"+rowCount+"' data-id='"+rowCount+"' name='cost[]' value='"+data[0].cost+"' readonly></td>@endif<td><input required type='text' class='form-control price price"+rowCount+"' min='1' id='price"+rowCount+"' data-id='"+rowCount+"' name='price[]' value='"+data[0].price+"'></td><td><input type='number' class='form-control' readonly  value='"+total_quantity+"'></td><td><input type='hidden' name='type[]' value='"+data[1]+"'><input type='number' class='form-control quantity' required min='1' id='"+rowCount+"' max='"+total_quantity+"' name='quantity[]' value='1'></td><td><input type='text' data-id='"+rowCount+"' class='form-control discountp percent"+rowCount+"' name='discount_percent[]' id='percent"+rowCount+"'></td><td><input type='text' data-id='"+rowCount+"' class='form-control discountr rate"+rowCount+"' name='discounted_amount[]' id='rate"+rowCount+"'></td><td><input type='text' readonly class='form-control abc sub"+rowCount+"' name='sub_total[]' id='sub"+rowCount+"' value='"+data[0].price+"'></td><td><button type='button' data-id='"+rowCount+"' id='"+rowCount+"' class='btn red delete' ><i class='fa fa-trash'></i></button></td></tr>");
                    }
                    var amount=0;
                    var total=0;
                    var rowCount1 = $('#example tr').length;
                    for (let i = 1; i < rowCount1; i++) {
                        amount = +amount + +$('.sub'+i).val();
                        total = +total + +$('.price'+i).val();
                    }
                    $('#amount').html(amount);
                    $('#l_amount').val(amount);
                    $('#final').val(amount);
                    $('#p_total1').val(amount);

                }
            }
        });
    });

    $('table').on('click', '.delete', function(e){
        var id=$(this).attr('id');
        // var rowNum=$(this).attr('data-id');
        var price=$('#price'+id).val();
        var sub=$('#sub'+id).val();
        var total=$('#total').html();
        var amount=$('#amount').html();
        console.log(amount,sub);
        total = -price - -total;
        amount = -sub - -amount;
        $(this).closest('tr').remove();
        $(".abc").each(function (i){
           $(this).removeClass('sub'+(i+2));
           $(this).addClass('sub'+(i+1));
        });
        $(".price").each(function (i){
           $(this).removeClass('price'+(i+2));
           $(this).addClass('price'+(i+1));
        });
        $(".delete").each(function (i){
            i=+i + +1;
            $(this).attr('id',i);
        });
        $('#amount').html(amount);
        $('#l_amount').val(amount);
        if($('#tax').val()=='')
        {
            $('#final').val(amount);
            if($('#p_total').val() == '')
            {
                $('#p_total1').val(amount);
            }
            else
            {
                $('#p_total').val(amount);
            }
        }
        if($('#tax').val()!='')
        {
            var amount=0;
            var rowCount1 = $('#example tr').length;
            for (let i = 1; i < rowCount1; i++) {
                amount = +amount + +$('.sub'+i).val();
                total = +total + +$('.price'+i).val();
            }
            var tax = $('#tax').val();
            var final = (amount * tax) / 100;
            amount = +amount + +final;
            $('#final').val(amount);
            if($('#p_total').val() == '')
            {
                $('#p_total1').val(amount);
            }
            else
            {
                $('#p_total').val(amount);
            }
        }

    });

    $('table').on('change','.quantity',function(){
        var a=$(this).val();
        var id=$(this).attr('id');
        var price=$('#price'+id).val();
        var rate = $('#rate'+id).val();
        var percent = $('#percent'+id).val();
        if(rate == '')
        {
            var sub= a*price;
        }
        if(rate != '')
        {
            var disc = (a * price * percent) / 100;
            $('.rate'+id).val(disc);
            var sub = (a * price) - disc;
        }

        $('#sub'+id).val(sub);
        var amount=0;
        var rowCount = $('#example tr').length;
        for (let i = 1; i < rowCount; i++) {
            amount = +amount + +$('.sub'+i).val();
        }
        $('#amount').html(amount);
        $('#l_amount').val(amount);
        if($('#tax').val()=='')
        {
            $('#final').val(amount);
            if($('#p_total').val() == 0)
            {
                $('#p_total1').val(amount);
            }
            else
            {
                $('#p_total').val(amount);
            }
        }
        if($('#tax').val()!='')
        {
            var tax = $('#tax').val();
            var final = (amount * tax) / 100;
            amount = +amount + +final;
            $('#final').val(amount);
            if($('#p_total').val() == 0)
            {
                $('#p_total1').val(amount);
            }
            else
            {
                $('#p_total').val(amount);
            }
        }
    });

    $('table').on('keyup','.price',function(){
        var id=$(this).attr('data-id');
        var price=$(this).val();
        var quan=$('#'+id);
        quan=quan.val();
        var rate = $('.rate'+id).val();
        var percent = $('.percent'+id).val();
        if(rate == '')
        {
            var sub=quan * price;
        }
        if(rate != '')
        {
            var disc = (quan * price * percent) / 100;
            $('.rate'+id).val(disc);
            var sub = (quan * price) - disc;
        }


        $('#sub'+id).val(sub);
        var amount=0;
        var total=0;
        var rowCount1 = $('#example tr').length;
        for (let i = 1; i < rowCount1; i++) {
            amount = +amount + +$('.sub'+i).val();
            console.log(i+' '+$('.sub'+i).val());
            total = +total + +$('.price'+i).val();
        }
        $('#amount').html(amount);
        if($('#tax').val()=='' )
        {
            $('#final').val(amount);
            if($('#p_total').val() == '')
            {
                $('#p_total1').val(amount);
            }
            else
            {
                $('#p_total').val(amount);
            }
        }
        if($('#tax').val()!='')
        {
            var tax = $('#tax').val();
            var final = (amount * tax) / 100;
            amount = +amount + +final;
            $('#final').val(amount);
            if($('#p_total').val() == '')
            {
                $('#p_total1').val(amount);
            }
            else
            {
                $('#p_total').val(amount);
            }
        }
    });

    $(document).on('keyup','#tax',function(){
        var amount =   $('#amount').html();
        var tax = $(this).val();
        var final = (amount * tax) / 100;
        amount = +amount + +final;
        if($('#p_total').val() == '')
        {
            $('#p_total1').val(amount);
        }
        else
        {
            $('#p_total').val(amount);
        }
        $('#final').val(amount);
    });

    $('table').on('keyup','.discountp',function(){
        var id=$(this).attr('data-id');
        var percent=$(this).val();
        var price=$('.price'+id).val();
        var quan=$('#'+id);
        quan=quan.val();
        console.log(price,quan,percent);
        var sub=quan * price;
        var disc = (sub * percent)/100;
        sub = sub - disc;
        $('#sub'+id).val(sub);
        $('.rate'+id).val(disc);
        var amount=0;
        var total=0;
        var rowCount1 = $('#example tr').length;
        for (let i = 1; i < rowCount1; i++) {
            amount = +amount + +$('.sub'+i).val();
            total = +total + +$('.price'+i).val();
        }
        $('#amount').html(amount);
        if($('#tax').val()=='' )
        {
            $('#final').val(amount);
            if($('#p_total').val() == '')
            {
                $('#p_total1').val(amount);
            }
            else
            {
                $('#p_total').val(amount);
            }
        }
        if($('#tax').val()!='')
        {
            var tax = $('#tax').val();
            var final = (amount * tax) / 100;
            amount = +amount + +final;
            $('#final').val(amount);
            if($('#p_total').val() == '')
            {
                $('#p_total1').val(amount);
            }
            else
            {
                $('#p_total').val(amount);
            }
        }
    });


    $('table').on('keyup','.discountr',function(){
        var id=$(this).attr('data-id');
        var disc=$(this).val();
        var price=$('.price'+id).val();
        var quan=$('#'+id);
        quan=quan.val();
        var sub=quan * price;
        var percent = Math.round((disc * 100) /sub);
        sub = sub - disc;
        $('#sub'+id).val(sub);
        $('.percent'+id).val(percent);
        var amount=0;
        var total=0;
        var rowCount1 = $('#example tr').length;
        for (let i = 1; i < rowCount1; i++) {
            amount = +amount + +$('.sub'+i).val();
            total = +total + +$('.price'+i).val();
        }
        $('#amount').html(amount);
        if($('#tax').val()=='' )
        {
            $('#final').val(amount);
            if($('#p_total').val() == '')
            {
                $('#p_total1').val(amount);
            }
            else
            {
                $('#p_total').val(amount);
            }
        }
        if($('#tax').val()!='')
        {
            var tax = $('#tax').val();
            var final = (amount * tax) / 100;
            amount = +amount + +final;
            $('#final').val(amount);
            if($('#p_total').val() == '')
            {
                $('#p_total1').val(amount);
            }
            else
            {
                $('#p_total').val(amount);
            }
        }
    });

</script>
@endsection
