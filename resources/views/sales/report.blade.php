@extends('layouts.master')
@section('top-styles')
@toastr_css
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #5F9EA0;
    }
    .modal-header{
        background: #5F9EA0 !important;
    }
    .form-check-inline {
        display: inline-flex;
        align-items: center;
        padding-left: 0;
        margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
        position: static;
        margin-top: 0;
        margin-right: .3125rem;
        margin-left: 0;
    }
</style>
@toastr_css

@section('sidebar-name1')
<li class="breadcrumb-item"><a href="{{ url('')}}/sales">Sales</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="card">
                <div class="card-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="card-header" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fa fa-credit-card font-white"></i>View Sales
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">
                                                @if ($index == 0)
                                                    <a style="margin-left:200px;color: white;font-size: 25px;"  href="{{route('sales.excel')}}">
                                                        <i class="fas fa-file-excel  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 1)
                                                    <a style="margin-left:200px;color: white;font-size: 25px;"  href="{{url('')}}/sales/search/{{$year}}/{{$c}}/{{$sp}}/{{$ss}}/{{$ps}}/{{$check}}/excel/year">
                                                        <i class="fas fa-file-excel  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 2)
                                                    <a style="margin-left:200px;color: white;font-size: 25px;"  href="{{url('')}}/sales/search/{{$month}}/{{$c}}/{{$sp}}/{{$ss}}/{{$ps}}/{{$check}}/excel/month">
                                                        <i class="fas fa-file-excel  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 3)
                                                    <a style="margin-left:200px;color: white;font-size: 25px;"  href="{{url('')}}/sales/search/{{$date}}/{{$c}}/{{$sp}}/{{$ss}}/{{$ps}}/{{$check}}/excel/date">
                                                        <i class="fas fa-file-excel  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 4)
                                                    <a style="margin-left:200px;color: white;font-size: 25px;"  href="{{url('')}}/sales/search/{{$from}}/{{$to}}/{{$c}}/{{$sp}}/{{$ss}}/{{$ps}}/{{$check}}/excel/date">
                                                        <i class="fas fa-file-excel  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 5)
                                                    <a style="margin-left:200px;color: white;font-size: 25px;"  href="{{url('')}}/sales/search/{{$c}}/{{$sp}}/{{$ss}}/{{$check}}/excel/ss">
                                                        <i class="fas fa-file-excel  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 6)
                                                    <a style="margin-left:200px;color: white;font-size: 25px;"  href="{{url('')}}/sales/search/{{$c}}/{{$sp}}/{{$ps}}/{{$check}}/excel/ps">
                                                        <i class="fas fa-file-excel  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 7)
                                                    <a style="margin-left:200px;color: white;font-size: 25px;"  href="{{url('')}}/sales/search/{{$c}}/excel/c">
                                                        <i class="fas fa-file-excel  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 8)
                                                    <a style="margin-left:200px;color: white;font-size: 25px;"  href="{{url('')}}/sales/search/{{$sp}}/excel/sp">
                                                        <i class="fas fa-file-excel  font-white"></i>
                                                    </a>
                                                @endif


                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="custom_datatable">

                                            <form action="{{url('')}}/sales/search" method="POST" id="advanceSearch">
                                                @csrf
                                                {{-- <input type="hidden" value="{{$menu_id}}" id="menuid" name="menuid"> --}}
                                                <div class="tableview">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio2">
                                                            <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Sale Date
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="Status">By Sale Status
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="PStatus">By Payment Status
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="customer">By Customer
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="saleperson">By Sale Person
                                                        </label>
                                                    </div>
                                                <div class="row">

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Year</label>
                                                            <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Month</label>
                                                        <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">From</label>
                                                            <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">To</label>
                                                            <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Customer Name</label>
                                                            <select class="form-control js-example-basic-single" name="c_id" id="c_id" disabled >
                                                                <option value=""  selected>Select...</option>
                                                                @foreach ($customer as $u)
                                                                <option  value="{{$u->id}}">{{$u->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Sale Persons Name</label>
                                                            <select class="form-control js-example-basic-single" name="sp_id" id="sp_id" disabled >
                                                                <option value=""  selected>Select...</option>
                                                                @foreach ($saleperson as $u)
                                                                <option  value="{{$u->id}}">{{$u->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Sales Status</label>
                                                            <select name="status" id="status" class="form-control js-example-basic-single" disabled>
                                                                <option disabled selected>No Filter</option>
                                                                <option>Pending</option>
                                                                <option>Approved</option>
                                                                <option>Partial</option>
                                                                <option>Complete</option>
                                                                <option>Delivered</option>
                                                                <option>Return</option>
                                                                <option>Closed</option>
                                                                <option>Cancel</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Payment Status</label>
                                                            <select name="p_status" id="p_status" class="form-control " disabled>
                                                                <option disabled selected>No Filter</option>
                                                                <option>Pending</option>
                                                                <option>Partial</option>
                                                                <option>Paid</option>
                                                                <option>Return</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                        {{-- <div class="col-md-8"></div> --}}
                                                        <div class="col-md-4">
                                                        <div class="form-group">
                                                            {{-- <label for="">Unit Name</label> --}}
                                                            <label for="" style="visibility: hidden">.</label>
                                                            <button id="search" disabled style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                            <i class="fa fa-search pr-1"></i> Search</button>
                                                        </div>
                                                        </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Sales Date</th>
                                                            <th>Expected Delivery Date</th>
                                                            <th>Warehouse</th>
                                                            <th>Accountant</th>
                                                            <th>Customer</th>
                                                            <th>Customer Remaining Balance</th>
                                                            <th>Sale Person</th>
                                                            <th>Total</th>
                                                            <th >Paid</th>
                                                            <th >Balance</th>
                                                            <th>Tax</th>
                                                            <th>Sale Status</th>
                                                            <th>Payment Status</th>
                                                            <th>Return Status</th>
                                                            <th>Sale Type</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $crb=0;
                                                            $ttl=0;
                                                            $bln=0;
                                                            $paid=0;
                                                            $taxx=0;
                                                        @endphp
                                                        @foreach ($sales as $s)
                                                            {{-- @if ($counter == null) --}}
                                                                <tr>
                                                                    <td>
                                                                        {{$s->id}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->sale_date}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->expected_date == null ? 'no' : $s->expected_date}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->warehouse->w_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->biller->name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->customer->name}}
                                                                    </td>
                                                                    <td>
                                                                        @php
                                                                            $credit = 0;
                                                                            $debit = 0;
                                                                            $account=\App\AccountDetails::with('generalLedger')->where('name_of_account',$s->customer->company.' - '.$s->customer->name)
                                                                            ->where('Code','like','CA-01%')
                                                                            ->first();
                                                                            // dd($account);
                                                                            if($account!=null)
                                                                            {
                                                                                for($k = 0 ; $k < count($account->generalLedger) ; $k++)
                                                                                {
                                                                                    $credit += $account->generalLedger[$k]->credit;

                                                                                    $debit += $account->generalLedger[$k]->debit;
                                                                                }
                                                                                $balance =  $debit - $credit;
                                                                            }

                                                                        @endphp
                                                                        {{$balance}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->saleperson == null ? '-' : $s->saleperson->name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->total - ($s->return == null ? 0 : $s->return->total)}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->total_amount == 0 ? '0' : $s->total_amount }}
                                                                    </td>

                                                                    <td>
                                                                        <input  width="10"  type="text" class="form-control" readonly id="amount" value="{{($s->total )- ($s->return == null ? 0 : $s->return->total) - ($s->total_amount == 0 ? '0' : $s->total_amount) }}">
                                                                    </td>
                                                                    <td>
                                                                        {{$s->tax == null ? 0 : $s->tax}}
                                                                    </td>
                                                                    @if($s->s_status=='Pending')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-warning" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->s_status=='Approved')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-primary" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->s_status=='Partial')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-info" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->s_status=='Complete')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-success" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->s_status=='Return')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-default" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->s_status=='Closed')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs"  style="background-color:BurlyWood" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->s_status=='Delivered')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-danger" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->s_status=='Cancel')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-warning" id="{{$s->id}}">
                                                                                {{$s->s_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->p_status=='Pending')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-warning" id="{{$s->id}}">
                                                                                {{$s->p_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->p_status=='Partial')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-primary" id="{{$s->id}}">
                                                                                {{$s->p_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->p_status=='Paid')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-success" id="{{$s->id}}">
                                                                                {{$s->p_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->p_status=='Return')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-info" id="{{$s->id}}">
                                                                                {{$s->p_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->return_status=='Partial')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-info" id="{{$s->id}}">
                                                                                {{$s->return_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->return_status=='No Return')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-success" id="{{$s->id}}">
                                                                                {{$s->return_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->return_status=='Complete')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-default" id="{{$s->id}}">
                                                                                {{$s->return_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    @if($s->return_status=='Requested')
                                                                        <td>
                                                                            <button type="button" class="btn btn-xs  btn-warning" id="{{$s->id}}">
                                                                                {{$s->return_status}}
                                                                            </button>
                                                                        </td>
                                                                    @endif
                                                                    <td>
                                                                        {{$s->s_type}}
                                                                    </td>
                                                                </tr>
                                                            @php
                                                                $crb+=$balance;
                                                                $ttl+=$s->total;
                                                                $bln+=(($s->total - $s->total_amount)+$s->returns);
                                                                $paid+=($s->total_amount - $s->returns) ;
                                                                $taxx+=$s->tax == null ? 0 : $s->tax;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th>TOTAL</th>
                                                            <th>{{$crb}}</th>
                                                            <th></th>
                                                            <th>{{$ttl}}</th>
                                                            <th>{{$paid}}</th>
                                                            <th>{{$bln}}</th>
                                                            <th>{{$taxx}}</th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                    </tfoot>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('modal')


        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Sales</h4>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Sale Date</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Name" id="sale_date" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Reference No</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Code" id="ref_no" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Warehouse</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Weight" id="w_name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Customer</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="c_name" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Accountant</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="b_name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Sale Person</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="s_name" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Tax Status</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="tax_status" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Advance payment</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="advance" readonly>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-1">
                                <a href="" id="edithref">
                                    <button type="button"  id="edit" class="btn btn-sm  btn-info" >Edit</button>
                                </a>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-responsive table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="5%">S.No</th>
                                            <th width="35%">Code - Name</th>
                                            <th>Unit</th>
                                            <th>Brand</th>
                                            {{-- @if (in_array('price',$permissions)) --}}
                                                <th width="7%">Price</th>
                                            {{-- @endif --}}
                                            <th width="5%">Quantity</th>
                                            <th width="5%">Delivered Quantity</th>
                                            <th width="5%">Return Quantity</th>
                                            {{-- @if (in_array('percent',$permissions))
                                                <th width="5%">Discount Percent</th> --}}
                                            {{-- @endif --}}
                                            {{-- @if (in_array('amount',$permissions)) --}}
                                                <th width="5%">Discounted Amount</th>
                                            {{-- @endif --}}
                                            {{-- @if (in_array('subtotal',$permissions)) --}}
                                                <th width="10%">Sub total</th>
                                            {{-- @endif --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>

                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    @endsection
    @section('custom-script')
    <script>
        CKEDITOR.replace( 'editor2' );
   </script>
    @toastr_js
    @toastr_render
<script type="text/javascript">

    function view(id)
    {
        $("#example1 tbody").empty();
        $('#example1 tfoot').empty();
        $.ajax({
            url:"{{url('')}}/sales/"+id,
            method:"GET",
            error: function (request, error) {
                lert(" Can't do because: " + error +request);
            },
            success:function(data){
                console.log(data);
                $('#sale_date').val(data[0].sale_date);
                $('#ref_no').val(data[0].ref_no);
                $('#w_name').val(data[0].warehouse.w_name);
                $('#c_name').val(data[0].customer.name);
                $('#b_name').val(data[0].biller.name);
                $('#s_name').val(data[0].saleperson.name);
                $('#advance').val(data[0].advance);
                $('#tax_status').val(data[0].tax_status);
                if(data[0].s_status == "Pending")
                {
                    $('#edit').attr('disabled',false);
                }
                else
                {
                    $('#edit').attr('disabled',true);
                }
                $('#edithref').attr('href','{{url('')}}/sales/'+id+'/edit');
                var tp =0;
                var tq =0;
                var dq =0;
                var st =0;

                for (let i = 0; i < data[1].length; i++) {
                    if(data[1][i].type == 1)
                    {
                        var subtotal = 0 ;
                        var sumqty = 0;

                        sumqty = (data[1][i].quantity - data[1][i].returnQty) ;


                        if(sumqty == 0)
                        {
                            st = 0;
                            subtotal = 0;
                        }
                        else
                        {
                            if(data[1][i].discounted_amount != null || data[1][i].discounted_amount != 0)
                            {
                                var discount = data[1][i].discounted_amount ;
                                var totalPrice = data[1][i].price;
                                var total_discount = discount / data[1][i].quantity;
                                var totalDis = (totalPrice - total_discount) * sumqty;
                                subtotal = +subtotal + +(totalDis);
                                st = +st + +totalDis;
                            }
                            else
                            {
                                subtotal = ((sumqty) * data[1][i].price) ;
                                st = +st + +(((sumqty) * data[1][i].price));
                            }
                        }


                        $("#example1").append("<tr><td>"+data[1][i].id+"</td><td>"+data[1][i].variant.name+"</td><td>"+data[1][i].variant.product.unit.u_name+"</td><td>"+data[1][i].variant.product.brands.b_name+"</td> <td>"+data[1][i].price+"</td><td>"+data[1][i].quantity+"</td><td>"+data[1][i].delivered_quantity+"</td><td>"+data[1][i].quantity+"</td> <td>"+data[1][i].discounted_amount+"</td>  <td>"+subtotal+"</td>  </tr>");
                        tp = +tp + +data[1][i].price;
                        tq = +tq + +data[1][i].quantity;
                        dq = +dq + +data[1][i].delivered_quantity;

                    }
                    else
                    {
                        var subtotal = 0 ;
                        var sumqty = 0;

                        sumqty = (data[1][i].quantity - data[1][i].returnQty) ;


                        if(sumqty == 0)
                        {
                            st = 0;
                            subtotal = 0;
                        }
                        else
                        {
                            if(data[1][i].discounted_amount != null || data[1][i].discounted_amount != 0)
                            {
                                var discount = data[1][i].discounted_amount ;
                                var totalPrice = data[1][i].price;
                                var total_discount = discount / data[1][i].quantity;
                                var totalDis = (totalPrice - total_discount) * sumqty;
                                subtotal = +subtotal + +(totalDis);
                                st = +st + +totalDis;
                            }
                            else
                            {
                                subtotal = ((sumqty) * data[1][i].price) ;
                                st = +st + +(((sumqty) * data[1][i].price));
                            }
                        }
                        $("#example1").append("<tr><td>"+data[1][i].id+"</td><td>"+data[1][i].products.pro_code+" - "+data[1][i].products.pro_name+"</td><td>"+data[1][i].products.unit.u_name+"</td><td>"+data[1][i].products.brands.b_name+"</td> <td>"+data[1][i].price+"</td> <td>"+data[1][i].quantity+"</td><td>"+data[1][i].delivered_quantity+"</td><td>"+data[1][i].returnQty+"</td>  <td>"+data[1][i].discounted_amount+"</td>   <td>"+subtotal+"</td> </tr>");
                        tp = +tp + +data[1][i].price;
                        tq = +tq + +data[1][i].quantity;
                        dq = +dq + +data[1][i].delivered_quantity;
                    }
                }
                $('#example1 tfoot').append(`<tr><td>TOTAL</td><td></td><td></td><td></td><td>`+tp+`</td><td>`+tq+`</td><td>`+dq+`</td><td></td><td></td><td>`+st+`</td></tr>`);
                $('#myModal').modal("show");
                $('#example1').DataTable();

            }
        });
    }
    $(document).ready(function () {
        $('.js-example-basic-single').select2();
        var table = $('#example').DataTable({
            scrollX: true,
            order: [[ 0, "desc" ]]
        });
        $('input:radio[name="optradio"]').change(function(){
            if ($(this).is(':checked')) {
                $('#search').prop('disabled',false);
                var val = $(this).val();
                if(val == 'Year')
                {
                    $('#year').prop('disabled',false);
                    $('#year').attr('required',true);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#p_status').prop('disabled',false);
                    $('#p_status').prop('required',false);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('required',false);
                    $('#status').attr('disabled',false);
                }
                if(val == 'Month')
                {
                    $('#month').prop('disabled',false);
                    $('#month').attr('required',true);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('required',false);
                    $('#status').attr('disabled',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#p_status').prop('disabled',false);
                    $('#p_status').prop('required',false);
                }
                if(val == 'Date')
                {
                    $('#from').prop('disabled',false);
                    $('#from').attr('required',true);
                    $('#to').prop('disabled',false);
                    $('#to').attr('required',false);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#status').attr('required',false);
                    $('#status').attr('disabled',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#p_status').prop('disabled',false);
                    $('#p_status').prop('required',false);
                }
                if(val == 'Status')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('required',true);
                    $('#status').attr('disabled',false);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#p_status').prop('disabled',true);
                    $('#p_status').prop('required',false);
                }

                if(val == 'PStatus')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('disabled',true);
                    $('#status').attr('required',false);
                    $('#p_status').prop('disabled',false);
                    $('#p_status').attr('required',true);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                }
                if(val == 'customer')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('disabled',true);
                    $('#status').attr('required',false);
                    $('#p_status').prop('disabled',true);
                    $('#p_status').attr('required',false);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',true);
                    $('#sp_id').prop('disabled',true);
                    $('#sp_id').prop('required',false);
                }
                if(val == 'saleperson')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('disabled',true);
                    $('#status').attr('required',false);
                    $('#p_status').prop('disabled',true);
                    $('#p_status').attr('required',false);
                    $('#c_id').prop('disabled',true);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',true);
                }
            }
        });
    });



    $('#example').on('click','td', function () {
        var col_index = $(this).index();
        var row_index = $(this).parent().index();
        var count = document.getElementById('example').rows[row_index].cells.length - 1;
        if(col_index <count)
        {
            var $tr = $(this).closest('tr');
            var id = $tr.find("td:eq(0)").text();
            view(id);
        }
    });
</script>

    @endsection
@endsection
