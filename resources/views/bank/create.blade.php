@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
    <li class="breadcrumb-item"><a href="{{ url('')}}/bank">Banks</a></li>
    <li class="breadcrumb-item"><span>{{$isEdit ? 'Edit' : 'Add'}} Bank</span></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="card" id="form_wizard_1">
                <div class="card-header">
                    <div class="caption">
                        <i class=" fa fa-landmark font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Bank</span>
                    </div>

                </div>
                <div class="card-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('bank.update',$bank->id) :  route('bank.store')}} " class="form-horizontal" method="POST"  id="bankForm">
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Bank Name<span class="text-danger">*</span></label>
                                        <input value="{{$bank->name ?? old('name')}}" class="form-control" type="text" placeholder="Enter Name" name="name" >
                                        <span class="text-danger">{{$errors->first('name') ?? null}}</span>
                                    </div>
                                </div>
                                <div class="{{$isEdit ? 'col-sm-6' :  (in_array('Add City',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >City <small>(optional)</small></label>
                                        <select id="c_id" class="form-control js-example-basic-single" name="c_id" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add City',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($city as $s)
                                                <option {{$s->id == $bank->c_id ? 'selected' : null}} value="{{$s->id}}">{{$s->c_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($city as $s)
                                                <option value="{{$s->id}}"  {{$s->id == old('c_id') ? 'selected' : null}}>{{$s->c_name}}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add City',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 30px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#cityModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Address<span class="text-danger">*</span></label>
                                        <input value="{{$bank->address ?? old('address')}}" class="form-control" type="text" placeholder="Enter Address" name="address" >
                                        <span class="text-danger">{{$errors->first('address') ?? null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Contact No <small>(optional)</small> </label>
                                        <input autocomplete="off" value="{{$bank->c_no ?? old('c_no')}}" class="form-control" type="text" placeholder="Enter Contact Number" name="c_no" >
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Branch<span class="text-danger">*</span></label>
                                        <input value="{{$bank->branch ?? old('branch')}}"  class="form-control" type="text" placeholder="Enter Branch" name="branch" >
                                        <span class="text-danger">{{$errors->first('branch') ?? null}}</span>
                                    </div>
                                </div>

                            </div>
                            <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
    @include('modals.city')
@endsection
@section('custom-script')
@toastr_js
@toastr_render
<script>
     $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });

    $('#bankForm').validate({
        rules: {
            name: {
                required: true,
            },
            address:{
                required: true,
            },
            branch:{
                required: true,
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>
<script>
    $(document).ready(function(){
        $('#c_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#cityModal').modal("show"); //Open Modal
            }
        });
    });
</script>
@endsection
