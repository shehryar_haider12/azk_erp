@extends('layouts.master')
@section('top-styles')
<style>
    thead {
	    background-color: #5F9EA0;
    }
    .form-check-inline {
        display: inline-flex;
        align-items: center;
        padding-left: 0;
        margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
        position: static;
        margin-top: 0;
        margin-right: .3125rem;
        margin-left: 0;
    }
</style>
@toastr_css
@section('sidebar-name1')
<li class="breadcrumb-item"><a href="{{ url('')}}/accountdetails">All Accounts</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="card">
                <div class="card-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="card-header"  style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-money-bill-alt font-white"></i>General Ledger
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="custom_datatable">
                                            {{-- <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Source</label>
                                                        <select class="form-control selectpicker" data-live-search="true" name="source" id="source" >
                                                            <option value=""  selected>Select...</option>
                                                            <option>Manual</option>
                                                            <option>Automated</option>
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">From Accounting Date</label>
                                                            <input type="date" name="fa_date" id="fa_date"  class="form-control" placeholder="Head Category Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">To Accounting Date</label>
                                                            <input type="date" name="ta_date" id="ta_date"  class="form-control" placeholder="Head Category Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Against Account</label>
                                                            <select class="form-control selectpicker" data-live-search="true" name="a_id" id="a_id" >
                                                                <option value=""  selected>Select...</option>
                                                                @foreach ($hcat as $u)
                                                                <option  value="{{$u->name_of_account}}">{{$u->name_of_account}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Posted Date</label>
                                                            <input type="date" name="p_date"  class="form-control" placeholder="Head Category Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Period</label>
                                                            <input type="month" name="period"  class="form-control" placeholder="Head Category Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Reference No</label>
                                                            <input type="text" name="ref"  class="form-control" placeholder="Reference Number" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8"></div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="" style="visibility: hidden">.</label>
                                                        <button style="background: #32c5d2" id="search" style="color: " class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form> --}}
                                            <form action="#" method="POST" id="advanceSearch">
                                                {{-- @csrf --}}
                                                {{-- <input type="hidden" value="{{$menu_id}}" name="menuid"> --}}
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Source</label>
                                                            <select class="form-control " data-live-search="true" name="source" id="source" >
                                                                <option value=""  selected>Select...</option>
                                                                <option>Manual</option>
                                                                <option>Automated</option>
                                                            </select>
                                                        </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">From Accounting Date</label>
                                                                <input type="date" name="fa_date" id="fa_date"  class="form-control" placeholder="Head Category Code" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">To Accounting Date</label>
                                                                <input type="date" name="ta_date" id="ta_date"  class="form-control" placeholder="Head Category Code" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Against Account</label>
                                                                <select class="form-control js-example-basic-single" name="a_id" id="a_id" >
                                                                    <option value=""  selected>Select...</option>
                                                                    @foreach ($hcat as $u)
                                                                    <option  value="{{$u->name_of_account}}">{{$u->name_of_account}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Posted Date</label>
                                                                <input type="date" name="p_date"  class="form-control" placeholder="Head Category Code" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Period</label>
                                                                <input type="month" name="period"  class="form-control" placeholder="Head Category Code" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Reference No</label>
                                                                <input type="text" name="ref"  class="form-control" placeholder="Reference Number" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="" style="visibility: hidden">.</label>
                                                            <button style="background: #32c5d2" id="search" style="color: " class="btn btn-light-theme btn-block waves-effect waves-light">
                                                            <i class="fa fa-search pr-1"></i> Search</button>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive" style="overflow-x: hidden">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Source</th>
                                                            <th>Account Code</th>
                                                            <th>Account Name</th>
                                                            <th>Description</th>
                                                            <th>Created By</th>
                                                            <th>Accounting Date</th>
                                                            <th>Against Account</th>
                                                            <th>Posted Date</th>
                                                            <th>Period</th>
                                                            <th>Reference No</th>
                                                            @if ($id == 'CA-02')
                                                                <th>Debit</th>
                                                                <th>Credit</th>
                                                                <th>In Stock</th>
                                                                <th>Out Stock</th>
                                                                <th>Net Quantity</th>
                                                                <th>Balance</th>
                                                                <th>Amount</th>
                                                            @else
                                                            @if ($id == 'EXP-05')
                                                                <th>Debit</th>
                                                                <th>Credit</th>
                                                                <th>Net Value</th>
                                                                <th>Balance</th>
                                                                <th>Percentage</th>
                                                            @else
                                                                <th>Debit</th>
                                                                <th>Credit</th>
                                                                <th>Net Value</th>
                                                                <th>Balance</th>
                                                            @endif
                                                            @endif
                                                            {{-- <th>Action</th> --}}
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $debit = 0;
                                                            $credit = 0;
                                                            $in = 0;
                                                            $out = 0;
                                                            $net = 0;
                                                            $amount = 0;
                                                            $total = 0;
                                                        @endphp
                                                        @foreach ($ledger as $a)
                                                        @if ($a->account_code == 'CL-02-001')
                                                            @php
                                                                $gl = App\GeneralLedger::where('link_id',$a->link_id)
                                                                ->where('account_code','!=',$a->account_code)
                                                                ->where('account_code','not like','CA-01%')
                                                                ->first();
                                                            @endphp
                                                        @else
                                                            @php
                                                                $gl = App\GeneralLedger::where('link_id',$a->link_id)
                                                                ->where('account_code','!=',$a->account_code)
                                                                ->first();
                                                            @endphp
                                                        @endif
                                                        @php
                                                            $debit += $a->debit;
                                                            $credit += $a->credit;
                                                            $in += $a->stock_in;
                                                            $out += $a->stock_out;
                                                            $net += $a->net_value;
                                                            $amount += $a->amount;
                                                        @endphp
                                                            <tr>
                                                                <td>
                                                                    {{$a->id}}
                                                                </td>
                                                                <td>
                                                                    {{$a->source}}
                                                                </td>
                                                                <td>
                                                                    {{$a->account_code}}
                                                                </td>
                                                                <td>
                                                                    {{$a->account_name}}
                                                                </td>
                                                                <td>
                                                                    {{$a->description}}
                                                                </td>
                                                                <td>
                                                                    @if ($a->createUser == null)
                                                                        -
                                                                    @else
                                                                        {{$a->createUser->name}}
                                                                    @endif

                                                                </td>
                                                                @php
                                                                    $period = Carbon\Carbon::parse($a->period)
                                                                    ->format('Y-m');
                                                                @endphp
                                                                <td>
                                                                    {{$a->accounting_date}}
                                                                </td>
                                                                <td>
                                                                    {{$gl == null ? '-' :  $gl->account_name}}
                                                                </td>
                                                                <td>
                                                                    {{$a->posted_date}}
                                                                </td>
                                                                <td>
                                                                    {{$period}}
                                                                </td>
                                                                <td>
                                                                    {{$a->transaction_no}}
                                                                </td>
                                                                <td>
                                                                    {{$a->debit}}
                                                                </td>
                                                                <td>
                                                                    {{$a->credit}}
                                                                </td>
                                                                @if ($id == 'CA-02')
                                                                    <td>
                                                                        {{$a->stock_in}}
                                                                    </td>
                                                                    <td>
                                                                        {{$a->stock_out}}
                                                                    </td>
                                                                @endif
                                                                <td>
                                                                    {{$a->net_value}}
                                                                </td>
                                                                <td>
                                                                    {{$a->balance}}
                                                                </td>
                                                                @if ($id == 'CA-02')
                                                                    @php
                                                                        $total += $a->amount;
                                                                    @endphp
                                                                    <td>{{$a->amount}}</td>
                                                                @else
                                                                    @if ($id == 'EXP-05')
                                                                    <td>{{$a->amount}}</td>
                                                                    @endif
                                                                @endif
                                                                {{-- <td>
                                                                    <a href="{{url('')}}/generalLedger/{{$a->link_id}}/edit">
                                                                        <button type="button" class="btn blue view">
                                                                            <i class="fa fa-refresh"></i>
                                                                        </button>
                                                                    </a>
                                                                </td> --}}

                                                            </tr>
                                                        @endforeach
                                                    </tbody>

                                                </table>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <label  style="font-weight: bold;">

                                                            @if ($id == 'CA-02')
                                                                Total Debit: {{$debit}} &emsp;
                                                                Total Credit: {{$credit}} &emsp;
                                                                Total In Stock: {{$in}}&emsp;
                                                                Total Out Stock: {{$out}}&emsp;
                                                                Total Balance: {{$in - $out}} &emsp;
                                                                Total Amount: {{$total}}
                                                            @else
                                                                Total Debit: {{$debit}} &emsp;
                                                                Total Credit: {{$credit}} &emsp;
                                                                Total Balance: {{$debit - $credit}} &emsp;
                                                            @endif
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
    @toastr_js
    @toastr_render
    {{-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.11.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="https://cdn.datatables.net/datetime/1.1.1/js/dataTables.dateTime.min.js"></script> --}}

        <script type="text/javascript">
            $(document).ready(function () {
                $('.js-example-basic-single').select2();
                // minDate = new DateTime($('#fa_date'), {
                //     format: 'MMMM Do YYYY'
                // });
                // maxDate = new DateTime($('#ta_date'), {
                //     format: 'MMMM Do YYYY'
                // });
                var table = $('#example').DataTable({
                    scrollX: true
                });

                // $('#fa_date, #ta_date').on('change', function () {
                //     table.draw();
                // });

                $('#advanceSearch').submit(function(e){
                e.preventDefault();


                table.columns(1).search($('select[name="source"]').val());
                table.columns(7).search($('select[name="a_id"]').val());
                table.columns(8).search($('input[name="p_date"]').val());
                table.columns(9).search($('input[name="period"]').val());
                table.columns(10).search($('input[name="ref"]').val());
                table.draw();
            });
        });

        </script>
    @endsection
@endsection
