<html>
  <head>
    {{-- <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> --}}
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
      .attendance-table table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #000;
         font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .blank-cell{

        min-width: 50px;


        }

        .attendance-cell{

        padding: 8px;


        }

        .attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
            border: 1px solid #000;
        }
        h2,h4 {
            color: #00CCFF;
        }
        .table-inv
        {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 50%;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }
        .table-inv thead
        {
            background-color: #ADD8E6;
        }
        .table-inv td,th
        {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        header
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            font-size: 30px;
            text-align: center;
            padding-top: 15px;
            background: #F0F8FF;
            height: 50px;
            font-weight: bold;
        }
        .label
        {
            background: #F0F8FF;
            font-size: 15px;
            text-align: left;
            color: #000;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            height: 60px;
            padding-left: 30px;
            /* font-weight: bold; */
        }
        .row
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            padding-left: 30px;
        }
        .col
        {
            font-size: 18px;
            background: #F0FFFF;
            height: 28px;
        }
        @page
        {
            margin: 0;
            size: A3;
        }
  </style>
  <body>

        <div class="attendance-table" style="margin-top: 25px">
            <h3 style="text-align: center">Product Transafer History Report</h3>
        <table class="table table-striped table-bordered">

            <thead>
                <tr>
                    <th class="attendance-cell">S.No</th>
                    <th class="attendance-cell">Transfer Date</th>
                    <th class="attendance-cell">From Warehouse</th>
                    <th class="attendance-cell">To Warehouse</th>
                    <th class="attendance-cell">Code - Product Name</th>
                    <th class="attendance-cell">Brand</th>
                    <th class="attendance-cell">Category</th>
                    <th class="attendance-cell">Quantity</th>
                    <th class="attendance-cell">Cost</th>
                    <th class="attendance-cell">Price</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $a = 1;
                @endphp

                    @foreach ($ptransfer as $s)
                        <tr>
                            <td class="attendance-cell" >
                                {{$a}}
                            </td>
                            <td class="attendance-cell" >
                                {{$s->transfer_date}}
                            </td>
                            <td class="attendance-cell" >
                                {{$s->fromWarehouse->w_name}}
                            </td>
                            <td class="attendance-cell" >
                                {{$s->toWarehouse->w_name}}
                            </td>
                            @if ($p->type == 1)
                                <td class="attendance-cell" >{{$p->variant->name}} </td>
                            @else
                                <td class="attendance-cell" >{{$p->products->pro_code}} - {{$p->products->pro_name}}</td>
                            @endif
                            <td class="attendance-cell" >
                                {{$s->products->brands->b_name}}
                            </td>
                            <td class="attendance-cell" >
                                {{$s->products->category->cat_name}}
                            </td>
                            <td class="attendance-cell" >
                                {{$s->quantity}}
                            </td>
                            <td class="attendance-cell" >
                                {{$s->products->cost}}
                            </td>
                            <td class="attendance-cell" >
                                {{$s->products->price}}
                            </td>
                        </tr>
                        @php
                            $a++;
                        @endphp
                    @endforeach

            </tbody>

        </table>

    </div>
  </body>
</html>
