@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li class="breadcrumb-item"><a href="{{ url('')}}/receivableVoucher">Receivable Voucher</a></li>
<li class="breadcrumb-item"><span>{{$isEdit ? 'Edit' : 'Add'}} Receivable Voucher</span></li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="card" id="form_wizard_1">
                <div class="card-header">
                    <div class="caption">
                        <i class=" fa fa-sticky-note font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Receivable Voucher</span>
                    </div>

                </div>
                <div class="card-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('receivableVoucher.update',$rcv->id) :  route('receivableVoucher.store')}} " class="form-horizontal" method="POST" id="cashVoucherForm">
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Voucher Number<span class="text-danger">*</span></label>
                                        <input value="{{$rcv->id ?? $id}}" readonly class="form-control" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Accounting Date<span class="text-danger">*</span></label>
                                        <input value="{{$rcv->accounting_date ?? $date}}" name="accounting_date" class="form-control" type="date"  >
                                        <span class="text-danger">{{$errors->first('accounting_date') ? 'Select Date' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Posted Date<span class="text-danger">*</span></label>
                                        <input value="{{$rcv->posted_date ?? $date}}" name="posted_date" class="form-control" type="date"  >
                                        <span class="text-danger">{{$errors->first('posted_date') ? 'Select Date' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Period<span class="text-danger">*</span></label>
                                        <input value="{{$rcv->period ?? old('period')}}" name="period" class="form-control" type="month"  >
                                        <span class="text-danger">{{$errors->first('period') ? 'Select Period' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Created By</label>
                                        <input value="{{$rcv->createdUser->name ?? $cby}}" class="form-control" type="text" readonly  >
                                        <input value="{{$rcv->created_by ?? $u_id}}" name="created_by" hidden >
                                    </div>
                                </div>
                            <input type="hidden" name="v_type" value="Receivable">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Customers<span class="text-danger">*</span></label>
                                        <select name="account2" class="form-control js-example-basic-single" id="customer">
                                            <option value="" selected disabled> Select (Company-Name)</option>
                                            @foreach ($account as $a)
                                                @if ($isEdit)
                                                    @if ($a->headCategory->name == 'Receivables')
                                                        <option {{$rcv->account2 == $a->name_of_account ? 'selected' : ''}}  >{{$a->name_of_account}}</option>
                                                    @endif
                                                @else
                                                    @if ($a->headCategory->name == "Receivables")
                                                        <option value="{{$a->name_of_account}}" {{old('account2') == $a->name_of_account ? 'selected' : null}}>{{$a->name_of_account}}</option>
                                                    @endif
                                                @endif

                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{$errors->first('account2') ? 'Select Debit Account' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Type<span class="text-danger">*</span></label>
                                        <select name="type" id="type" class="form-control js-example-basic-single">
                                            <option value="" selected disabled>Select</option>
                                            @if ($isEdit)
                                                <option {{$rcv->type == 'Cash' ? 'selected' : ''}}>Cash</option>
                                                <option {{$rcv->type == 'Bank' ? 'selected' : ''}}>Bank</option>
                                            @else
                                                <option {{old('type') == 'Cash' ? 'selected' : null}}>Cash</option>
                                                <option {{old('type') == 'Bank' ? 'selected' : null}}>Bank</option>
                                            @endif
                                        </select>
                                        <span class="text-danger">{{$errors->first('type') ? 'Select Type' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Cash or Bank<span class="text-danger">*</span></label>
                                        <select name="account1" class="form-control js-example-basic-single">
                                            <option value="" selected disabled> Select</option>
                                            @foreach ($account as $a)
                                                @if ($isEdit)
                                                    @if ($a->headCategory->name == 'Cash' || $a->headCategory->name == "Bank")
                                                        <option {{$rcv->account1 == $a->name_of_account ? 'selected' : ''}}  >{{$a->name_of_account}}</option>
                                                    @endif
                                                @else
                                                    @if ($a->headCategory->name == 'Cash' || $a->headCategory->name == "Bank")
                                                        <option value="{{$a->name_of_account}}" {{old('account1') == $a->name_of_account ? 'selected' : null}}>{{$a->name_of_account}}</option>
                                                    @endif
                                                @endif

                                            @endforeach
                                        </select>
                                        <span class="text-danger">{{$errors->first('account1') ? 'Select Credit Account' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Description<span class="text-danger">*</span></label>
                                        <input type="text" class="form-control" value="{{$rcv->description ?? old('description')}}" name="description">
                                        <span class="text-danger">{{$errors->first('description') ? 'Enter description' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Balance</label>
                                        <input type="text" class="form-control" readonly id="balance">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Amount<span class="text-danger">*</span></label>
                                        <input type="text" id="amount" class="form-control" value="{{$rcv->debit ?? old('debit')}}" name="debit">
                                        <span class="text-danger">{{$errors->first('debit') ? 'Enter Amount' : null}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" id="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>


@endsection
@section('custom-script')
@toastr_js
@toastr_render

<script>
    $('#cashVoucherForm').validate({
        rules: {
            accounting_date: {
                required: true,
            },
            posted_date: {
                required: true,
            },
            period: {
                required: true,
            },
            account1: {
                required: true,
            },
            type: {
                required: true,
            },
            account2: {
                required: true,
            },
            description: {
                required: true,
            },
            debit: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    function customer($this)
    {
        var name = $this;
        $.ajax({
            url:"{{url('')}}/receivableVoucher/customer/"+name,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                if(data.balance == 0)
                {
                    $('#submit').attr('disabled',true);
                }
                else
                {
                    $('#submit').attr('disabled',false);
                }
                $('#balance').val(data.balance);
                $('#amount').attr('max',data.balance);
            }
        });
    }

    $(document).on('change','#customer',function(){
        var name = $(this).val();
        customer(name);
    });

    var name = $('#customer').val();
    if(name != 'null'){
        customer(name);
    }
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });

</script>

@endsection
