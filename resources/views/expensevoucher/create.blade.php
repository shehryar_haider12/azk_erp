@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li class="breadcrumb-item"><a href="{{ url('')}}/expenseVoucher">Expense Voucher</a></li>
<li class="breadcrumb-item"><span>{{$isEdit ? 'Edit' : 'Add'}} Expense Voucher</span></li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="card" id="form_wizard_1">
                <div class="card-header">
                    <div class="caption">
                        <i class=" fa fa-sticky-note font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Expense Voucher</span>
                    </div>

                </div>
                <div class="card-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('expenseVoucher.update',$exp->id) :  route('expenseVoucher.store')}} " class="form-horizontal" method="POST" id="voucherForm">
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Voucher Number<span class="text-danger">*</span></label>
                                        <input value="{{$exp->id ?? $id}}" readonly class="form-control" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Voucher Date<span class="text-danger">*</span></label>
                                        <input value="{{$exp->v_date ?? $date}}" class="form-control" type="date" required name="v_date">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Prepared By<span class="text-danger">*</span></label>
                                        <input value="{{$exp->preparedUser->name ?? $pby}}" readonly class="form-control" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Category<span class="text-danger">*</span></label>
                                        <select required name="c_id" id="c_id" class="form-control js-example-basic-single">
                                            <option disabled selected> Select Anyone</option>
                                            @if ($isEdit)
                                                @foreach ($cat as $c)
                                                    <option {{$exp->c_id == $c->id ? 'selected' : ''}} value="{{$c->id}}">{{$c->name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($cat as $c)
                                                    <option value="{{$c->id}}" {{old('c_id') == $c->id ? 'selected' : null}}>{{$c->name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Sub Category<span class="text-danger">*</span></label>
                                        <select name="sc_id" required id="sc_id" class="form-control js-example-basic-single">
                                            <option disabled selected> Select Anyone</option>
                                            @if ($isEdit)
                                                @foreach ($account as $a)
                                                    <option {{$exp->sc_id == $a->Code ? 'selected' : ''}} value="{{$a->Code}}">{{$a->name_of_account}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Description<span class="text-danger">*</span></label>
                                        <input value="{{$exp->description ?? old('description')}}" required class="form-control" type="text" name="description" placeholder="Enter Description">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Amount<span class="text-danger">*</span></label>
                                        <input value="{{$exp->amount ?? old('amount')}}" required class="form-control" type="text" name="amount" placeholder="Enter Amount">
                                    </div>
                                </div>
                            </div>
                            <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>


@endsection
@section('custom-script')
@toastr_js
@toastr_render
<script>
    $('#voucherForm').validate({
        rules: {
            c_id: {
                required: true,
            },
            sc_id: {
                required: true,
            },
            description: {
                required: true,
            },
            amount: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>
<script>
     $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });

    $(document).on('change','#c_id',function(){
        var id=$(this).val();
        $('#sc_id').empty();
        $.ajax({
            url:"{{url('')}}/expenseVoucher/account/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                $('#sc_id').append(`<option disabled selected> Select Anyone</option>`);
                for (let k = 0; k < data.length; k++) {
                    $('#sc_id').append(`<option value="`+data[k].Code+`">`+data[k].name_of_account+`</option>`);
                }
            }
        });
    });
</script>
@endsection
