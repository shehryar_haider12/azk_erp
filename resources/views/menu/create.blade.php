@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li class="breadcrumb-item"><a href="{{url('')}}/menu">User Menu</a></li>
<li class="breadcrumb-item"><span>{{$isEdit ? 'Edit' : 'Add'}} User Menu</span></li>

@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="card" id="form_wizard_1">
                <div class="card-header">
                    <h3 class="mb-0">
                        <i class=" fa fa-user font-green"></i>
                        {{$isEdit ? 'Edit' : 'Add'}} User Menu</h3>
                  </div>
                <div class="card-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('menu.update',$menu->id) :  route('menu.store')}} " class="form-horizontal" method="POST" >
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-outline">
                                        <label for="">Parent Menu<span class="text-danger">*</span></label>
                                        <select class="form-control js-example-basic-single"  name="p_id" id="p_id" >
                                            <option value=""  selected disabled>Select...</option>
                                            @if ($isEdit)
                                                @if ($menu->p_id == 0)
                                                    <option value="0" selected>No Parent</option>
                                                    @foreach ($menus as $m)
                                                        <option value="{{$m->id}}">{{$m->name}}</option>
                                                    @endforeach
                                                @else
                                                <option value="0">No Parent</option>
                                                @foreach ($menus as $m)
                                                    <option value="{{$m->id}}" {{$menu->p_id == $m->id ? 'selected' : ''}} >{{$m->name}}</option>
                                                @endforeach
                                                @endif
                                            @else
                                                <option value="0">No Parent</option>
                                                @foreach ($menus as $m)
                                                    @if ($m->p_id != 0)
                                                        <option value="{{$m->id}}">&emsp; {{$m->name}}</option>
                                                    @else
                                                        <option value="{{$m->id}}">{{$m->name}}</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        </select>
                                        <span class="text-danger">{{$errors->first('p_id') ? 'Select parent Menu' : null}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Name<span class="text-danger">*</span></label>
                                        <input value="{{$menu->name ?? old('name')}}" class="form-control" type="text" placeholder="Enter Menu Name" name="name" >
                                        <span class="text-danger">{{$errors->first('name') ? 'Menu already exist or Enter name' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Icon Class</label>
                                        <input value="{{$menu->icon ?? old('icon')}}" class="form-control" type="text" placeholder="Enter Icon Class" name="icon">
                                        <span class="text-success">Leave an empty value if saving as child</span>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Route</label>
                                        <input value="{{$menu->route ?? old('route')}}" class="form-control" type="text" placeholder="Enter Route Name" name="route" >
                                        <span class="text-success">Leave an empty value if this menu has children</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Sort<span class="text-danger">*</span></label>
                                        <input value="{{$menu->sort ?? old('sort')}}" class="form-control" type="text" min="1" placeholder="" name="sort">
                                        <span class="text-danger">{{$errors->first('sort') ? 'Enter sort number' : null}}</span>
                                    </div>
                                </div>

                            </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>

@endsection
@section('custom-script')
<script>
     $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
</script>
@toastr_js
@toastr_render
@endsection
