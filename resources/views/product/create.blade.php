@extends('layouts.master')
@section('top-styles')
    @toastr_css
@section('sidebar-name1')
    <li class="breadcrumb-item"><a href="{{ url('')}}/product">Product</a></li>
    <li class="breadcrumb-item"><span>{{$isEdit ? 'Edit' : 'Add'}} Product</span></li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="card" id="form_wizard_1">
                <div class="card-header">
                    <div class="caption">
                        <i class=" fa fa-list-alt font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Product</span>
                    </div>

                </div>
                <div class="card-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('product.update',$product->id) :  route('product.store')}} " class="form-horizontal" method="POST" enctype="multipart/form-data" id="productForm">
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type='file' name="image" id="imageUpload1" placeholder="add image"
                                            accept=".png, .jpg, .jpeg" />
                                            <label for="imageUpload1"><span>Featured Image <small>(optional)</small> </span></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview1"
                                                style="background-image : url({{url('').'/uploads/'}}{{$isEdit ? $product->image : 'placeholder.jpg'}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Product Name<span class="text-danger">*</span></label>
                                        <input value="{{$product->pro_name ?? old('pro_name')}}" {{$isEdit ? 'readonly' : ''}} class="form-control" type="text" placeholder="Enter Product Name" name="pro_name" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Barcode <small>(optional)</small> </label>
                                        <select {{$isEdit ? 'disabled' : null}} id="barcode" class="form-control " >
                                            {{-- <option disabled selected>Select</option> --}}
                                            <option selected value="No">Auto Generator</option>
                                            <option   value="Yes">Custom</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Product Code<span class="text-danger">*</span></label>
                                        <input readonly value="{{$product->pro_code ??old('pro_code') ?? $code }}" autofocus class="form-control" type="text" placeholder="Enter Product Code" id="pro_code" name="pro_code" required>
                                        {{-- <span class="text-danger">{{$errors->first('pro_code') ? 'Product Code already exist' : null}}</span> --}}
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Product Type<span class="text-danger">*</span></label>
                                        <select class="form-control js-example-basic-single" name="p_type" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                            <option {{$product->p_type == 'Raw Material' ? 'selected' : null }}>Raw Material</option>
                                            <option {{$product->p_type == 'Packaging' ? 'selected' : null }}>Packaging</option>
                                            <option {{$product->p_type == 'Finished' ? 'selected' : null }}>Finished</option>
                                            @else
                                            <option>Raw Material {{old('p_type') == 'Raw Material' ? 'selected' : null}}</option>
                                            <option>Packaging {{old('p_type') == 'Packaging' ? 'selected' : null}}</option>
                                            <option>Finished {{old('p_type') == 'Finished' ? 'selected' : null}}</option>

                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Unit',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Product Unit<span class="text-danger">*</span></label>
                                        <select class="form-control js-example-basic-single" name="unit_id" required id="unit_id">
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Unit',$permissions))
                                                <option>other</option>
                                            @endif
                                            {{-- @php
                                                $unitName = \App\Unit::find($product->unit_id);
                                            @endphp --}}
                                            @if ($isEdit)
                                                @foreach ($unit as $u)
                                                    <option {{$product->unit_id == $u->id ? 'selected' : null}} value="{{$u->u_name}}">{{$u->u_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($unit as $u)
                                                    <option  value="{{$u->u_name}}" {{old('unit_id') == $u->u_name ? 'selected' : null}}>{{$u->u_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Unit',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 30px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#unitModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" ></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Product Weight <small>(optionall)</small></label>
                                        <input min="0" value="{{$product->weight ?? old('weight')}}" class="form-control" type="text" placeholder="Enter Product Weight" name="weight" id="weight">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Case<span class="text-danger">*</span></label>
                                        <input  value="{{$product->case ?? old('case')}}" class="form-control" type="text" placeholder="Enter Product Case" name="case" id="case">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Cost<span class="text-danger">*</span></label>
                                        <input min="1" value="{{$product->cost ?? old('cost')}}" class="form-control" type="text" placeholder="Enter Product Cost" name="cost" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Price<span class="text-danger">*</span></label>
                                        <input min="1" value="{{$product->price ?? old('price')}}" class="form-control" type="text" placeholder="Enter Product Price" autocomplete="off" name="price" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Description <small>(optional)</small></label>
                                        <input value="{{$product->description ?? old('description')}}" class="form-control" type="text" placeholder="Enter Product Description" name="description">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Alert Quantity<span class="text-danger">*</span></label>
                                        <input min="1" value="{{$product->alert_quantity ?? old('alert_quantity')}}" class="form-control" type="text" placeholder="Enter Product Alert Quantity" name="alert_quantity" >
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Brand',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Brand<span class="text-danger">*</span></label>
                                        <select class="form-control js-example-basic-single" name="brand_id" id="brand_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Brand',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($brands as $u)
                                                    <option {{$product->brand_id == $u->id ? 'selected' : null}} value="{{$u->id}}">{{$u->b_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($brands as $u)
                                                    <option  value="{{$u->id}}" {{old('brand_id') == $u->id ? 'selected' : null}}>{{$u->b_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Brand',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 30px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#brandModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" ></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Category',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Category<span class="text-danger">*</span></label>
                                        <select class="form-control js-example-basic-single" name="cat_id" id="cat_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Category',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($cat as $u)
                                                    <option {{$product->cat_id == $u->id ? 'selected' : null}} value="{{$u->id}}">{{$u->cat_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($cat as $u)
                                                    <option  value="{{$u->id}}" {{old('cat_id') == $u->id ? 'selected' : null}} >{{$u->cat_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Category',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 30px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#catModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" ></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="row">
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Sub Category',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Sub Category<span class="text-danger">*</span></label>
                                        <select class="form-control js-example-basic-single" name="s_cat_id" id="s_cat_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Sub Category',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($sub as $u)
                                                    <option {{$product->s_cat_id == $u->id ? 'selected' : null}} value="{{$u->id}}">{{$u->s_cat_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($sub as $u)
                                                    <option  value="{{$u->id}}" {{old('s_cat_id') == $u->id ? 'selected' : null}} >{{$u->s_cat_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Sub Category',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 30px;  width:50px">
                                            <a href="#" id="subcat" data-toggle="modal" data-target="#subModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" ></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Visible to POS <small>(optional)</small></label>
                                        <select class="form-control " name="visibility" >
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                            <option {{$product->visibility == '1' ? 'selected' : null }} value="1">Yes</option>
                                            <option {{$product->visibility == '0' ? 'selected' : null }} value="0">No</option>
                                            @else
                                            <option value="1" {{old('visibility') == '1' ? 'selected' : null}}>Yes</option>
                                            <option value="0" {{old('visibility') == '0' ? 'selected' : null}}>No</option>

                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Company Name <span class="text-danger">*</span> </label>
                                        <input value="{{$product->company_name ?? old('company_name')}}" type="text" placeholder="Enter Company Name" class="form-control" name="company_name">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Supplier Name<span class="text-danger">*</span></label>
                                        @php
                                            $status1 = false;
                                        @endphp
                                        <select multiple class="form-control js-example-basic-multiple" multiple="multiple" id="s_id" name="s_id[]" required>
                                            <option disabled >Select...</option>
                                            @foreach ($supplier as $s)
                                                @if ($isEdit)

                                                    @if (count($ps) > 0)
                                                        @foreach ($ps as $p)
                                                            @if ($p->s_id == $s->id)
                                                                @php
                                                                    $status1 = true;
                                                                @endphp

                                                            @break
                                                            @else
                                                                @php
                                                                    $status1 = false;
                                                                @endphp

                                                            @endif
                                                        @endforeach
                                                        @if ($status1 == true)
                                                        <option selected value="{{$s->id}}">{{$s->name}}  </option>
                                                        @else
                                                        <option value="{{$s->id}}">{{$s->name}} </option>
                                                        @endif
                                                    @else
                                                        <option value="{{$s->id}}">{{$s->name}} </option>
                                                    @endif
                                                @else
                                                    <option value="{{$s->id}}">{{$s->name}} </option>
                                                @endif

                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>MRP <small>(optional)</small> </label>
                                        <input type="text" value="{{$product->mrp ?? old('mrp')}}" placeholder="Enter MRP" class="form-control" name="mrp">
                                    </div>
                                </div>
                            </div>
                            <br>
                            {{-- <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-outline">
                                        <button class="btn btn-info variant" type="button">Add Variant</button>
                                    </div>
                                </div>
                            </div> --}}
                            <div class="variants" id="variants">
                                @if ($isEdit)
                                    @php
                                        $count = 1;
                                    @endphp
                                    @foreach ($p_variant as $va)
                                        @php
                                            $name = explode('-',$va->name);
                                            $status = false;
                                        @endphp
                                        <br>
                                        <div class="row addvariants" id="{{$count}}" >
                                            @foreach($variant->where('p_id',0) as $v)
                                                <div class="col-sm-3">
                                                    <div class="form-outline">
                                                        <label><b> {{$v->name}} </b>
                                                        </label>
                                                        <select name="variants[{{$count}}][]" class="form-control va ">
                                                            <option selected disabled> select</option>
                                                            @foreach($variant->where('p_id',$v->id) as $c)
                                                                @foreach ($name as $n)
                                                                    @if ($n == $c->name)
                                                                        @php
                                                                            $status = true;
                                                                        @endphp
                                                                    @break
                                                                    @else
                                                                        @php
                                                                            $status = false;
                                                                        @endphp
                                                                    @endif
                                                                @endforeach
                                                                @if ($status == true)
                                                                    <option selected>{{$c->name}}</option>
                                                                @else
                                                                    <option>{{$c->name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                            @endforeach
                                                <div class="col-sm-2">
                                                    <div class="form-outline">
                                                        <label><b> Cost </b>
                                                        </label>
                                                        <input type="text" class="form-control co" value="{{$va->cost}}" name="costv[{{$count}}]">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-outline">
                                                        <label><b> Price </b>
                                                        </label>
                                                        <input type="text" class="form-control pr" value="{{$va->price}}" name="pricev[{{$count}}]">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-outline">
                                                        <label><b> MRP </b>
                                                        </label>
                                                        <input type="text" class="form-control mrp" value="{{$va->mrp}}" name="mrpV[{{$count}}]">
                                                    </div>
                                                </div>
                                                <input type="hidden" name="v_id[{{$count}}]" class="vid" value="{{$va->id}}">
                                                <div class="col-sm-2"> <button type="button" style="margin-top:25px" id="{{$count}}" class="btn red delete d{{$count}}" ><i class="fa fa-trash"></i></button></div>
                                        </div>
                                        @php
                                            $count++;
                                        @endphp
                                    @endforeach
                                    {{-- {{dd($name)}} --}}
                                @endif
                            </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0" >
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
        @include('modals.unit')
        @include('modals.category')
        @include('modals.subcategory')
        @include('modals.brand')
@endsection


@section('custom-script')
@toastr_js
@toastr_render
    <script>
        $(document).ready(function(){ //Make script DOM ready
            $('.js-example-basic-single').select2();
            $('.js-example-basic-multiple').select2();

            $('#unit_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            if(opval=="other"){ //Compare it and if true
                $('#unitModal').modal("show"); //Open Modal
            }
        });
        $('#cat_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            if(opval=="other"){ //Compare it and if true
                $('#catModal').modal("show"); //Open Modal
            }
        });
        $('#s_cat_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            if(opval=="other"){ //Compare it and if true
                $('#subModal').modal("show"); //Open Modal
            }
        });
        $('#brand_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            if(opval=="other"){ //Compare it and if true
                $('#brandModal').modal("show"); //Open Modal
            }
        });


        function readURL(input, number) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview' + number).hide();
                    $('#imagePreview' + number).fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
            $("#imageUpload1").change(function () {
                readURL(this, 1);
        });
    });



    $(document).on('click','.variant',function(){
        var a = 0;
        var parentDiv = document.getElementById("variants");
        if (parentDiv.hasChildNodes())
        {
            var last = 0;
            $(".addvariants").each(function(){
                last = ($(this).attr("id"));
            });
            a = +last + +1;
        }
        else
        {
            a = 1;
        }
        $('.variants').append(`<br><div class="row addvariants" id="`+a+`" >
            @foreach($variant->where('p_id',0) as $v)
            <div class="col-sm-3">
                <div class="form-outline">
                    <label><b> {{$v->name}} </b>
                    </label>
                    <select name="variants[`+a+`][]" class="form-control va v`+a+`">
                        <option selected disabled> select</option>
                        @foreach($variant->where('p_id',$v->id) as $c)
                        <option>{{$c->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            @endforeach
            <div class="col-sm-2"><div class="form-outline">
                <label><b> Cost </b>
                </label>
                <input type="text" class="form-control co c`+a+`" name="costv[`+a+`]">
            </div></div>
            <div class="col-sm-2"><div class="form-outline">
                <label><b> Price </b>
                </label>
                <input type="text" class="form-control pr p`+a+`" name="pricev[`+a+`]">
            </div></div>
            <div class="col-sm-2"><div class="form-outline">
                <label><b> MRP </b>
                </label>
                <input type="text" class="form-control mrp mrp`+a+`" name="mrpV[`+a+`]">
            </div></div>
            <div class="col-sm-2"> <button type="button" style="margin-top:25px" id="`+a+`" class="btn red delete d`+a+`" ><i class="fa fa-trash"></i></button></div>
            </div>

        `);
    });

    $(document).on('click','.delete',function(){
        var id=$(this).attr('id');
        $(this).closest('.addvariants').remove();
        $(".va").each(function (i){
            i=+i + +1;
            $(this).attr('name','variants['+i+'][]');
        });
        $(".co").each(function (i){
            i=+i + +1;
            $(this).attr('name','costv['+i+']');
        });
        $(".pr").each(function (i){
            i=+i + +1;
            $(this).attr('name','pricev['+i+']');
        });
        $(".mrp").each(function (i){
            i=+i + +1;
            $(this).attr('name','mrp['+i+']');
        });
        $(".vid").each(function (i){
            i=+i + +1;
            $(this).attr('name','v_id['+i+']');
        });
        $(".delete").each(function (i){
            i=+i + +1;
            $(this).attr('id',i);
        });
    });

    $(document).on('change','#barcode',function(){
        var val = $(this).val();
        var code =  Math.floor((Math.random() * 10000000) + 1);
        if(val == "Yes")
        {
            $('#pro_code').val('');
            $('#pro_code').attr('readonly',false);
        }
        else{
            $('#pro_code').val(code);
            $('#pro_code').attr('readonly',true);
        }
    });


    </script>
<script>
    $('#productForm').validate({
        rules: {
            pro_name: {
                required: true,
            },
            company_name: {
                required: true,
            },
            case: {
                required: true,
            },
            pro_code: {
                required: true,
            },
            p_type: {
                required: true,
            },
            unit_id: {
                required: true,
            },
            cost: {
                required: true,
            },
            price: {
                required: true,
            },
            brand_id: {
                required: true,
            },
            cat_id: {
                required: true,
            },
            s_cat_id: {
                required: true,
            },
            s_id: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>
@endsection

