<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="{{url('')}}/dashboard">
              <img src="{{url('')}}/uploads/logo-1.png" style="margin-top: -10px; max-height:68rem"  >
            </a>
          </div>
          <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
              <!-- Nav items -->
                <ul class="navbar-nav">
                    @php
                        $role_menu_id = \App\RoleMenu::with(['menu.children'])
                        ->where('r_id',Auth::user()->r_id)
                        ->get()->pluck('m_id')->toArray();
                        $menus = \App\UserMenu::whereIn('id',$role_menu_id)->get();
                    @endphp
                    @foreach ($menus as $kwy => $m)
                        @php
                            $child_menus[] =  $m->m_id;
                        @endphp
                        @if ($m->p_id == 0)
                            <li class="nav-item {{Route::currentRouteName() == $m->route ? 'active' : null}}">
                                <a href="{{!empty($m->route) ? route($m->route) : 'javascript:;'}}" data-id = "{{$m->id}}" class="nav-link {{!empty($m->route) ? '' : 'collapsed'}} {{!empty($m->route) ? '' : 'main'}}" data-toggle="{{!empty($m->route) ? '' : 'collapse'}}"  aria-controls="{{!empty($m->route) ? '' : 'navbar-'.str_replace(' ', '', $m->name)}}" aria-expanded="false">
                                    <i class=" {{$m->icon}}"></i>
                                    <span class="nav-link-text">{{$m->name}}</span>
                                    <span class="{{Route::currentRouteName() == $m->route ? 'selected' : 'arrow'}}"></span>
                                    <!-- <span class="arrow open"></span> -->
                                </a>
                                @if ($m->name == 'Chart of Accounts')
                                    @php
                                        $coa = \App\HeadofAccounts::all();
                                    @endphp
                                    <div class="collapse" id="navbar-{{str_replace(' ', '', $m->name)}}" data-id="{{$m->id}}">
                                    <ul class="nav nav-sm flex-column">
                                        @foreach ($coa as $c)
                                            @php
                                                $hc = App\HeadCategory::where('a_id',$c->id)
                                                ->get();
                                            @endphp
                                            <li class="nav-item  ">
                                                <a href="javascript:;" class="nav-link nav-toggle">
                                                    <span class="title">{{$c->name}}</span>
                                                    <span class="arrow"></span>
                                                </a>
                                                <ul class="nav nav-sm flex-column">
                                                    @foreach ($hc as $h)

                                                        <li class="nav-item  ">
                                                            <a href="{{url('')}}/headcategory/history/{{$h->code}}" class="nav-link ">
                                                                <span class="nav-link-text">{{$h->name}}</span>
                                                            </a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </li>
                                        @endforeach
                                    </ul>
                                    </div>
                                @else

                                    <div class="collapse" id="navbar-{{str_replace(' ', '', $m->name)}}" data-id="{{$m->id}}">
                                        <ul class="nav nav-sm flex-column">
                                            @foreach ($menus as $child_menu)
                                                @if ($m->id == $child_menu->p_id)
                                                    <li class="nav-item">
                                                    <a href="{{route($child_menu->route)}}" class="nav-link">
                                                        <span class="sidenav-normal"> {{$child_menu->name}} </span>
                                                    </a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
  </nav>
