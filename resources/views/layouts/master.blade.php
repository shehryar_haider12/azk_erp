<!DOCTYPE html>

<html lang="en">
    <!-- BEGIN HEAD -->
    @include('layouts.head')
    @yield('top-styles')
    <body>
        @include('layouts.sidebar')
        <div class="main-content" id="panel">
            @include('layouts.topNav')
            <div class="header pb-6" >
                <div class="container-fluid">
                    <div class="header-body">
                        <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                @yield('sidebar-name1')
                            </ol>
                            </nav>
                        </div>
                        </div>
                        @yield('header-content')
                    </div>
                </div>
            </div>
            <div class="container-fluid mt--6">
                @yield('content')
                @include('modals.dailyPL')
                @yield('modal')
                @include('layouts.footer')
            </div>
        </div>


        @include('layouts.scripts')

        @yield('page-scripts')
        @yield('custom-script')
        <script>
            $(document).on('click','.main',function(){
                if($(this).hasClass('collapsed'))
                {
                    $(this).removeClass('collapsed');
                    $(this).addClass('active');
                    $(this).attr('aria-expanded',true);
                    var name = $(this).attr('aria-controls');
                    $('#'+name).addClass('show');
                }
                else
                {
                    $(this).addClass('collapsed');
                    $(this).removeClass('active');
                    $(this).attr('aria-expanded',false);
                    var name = $(this).attr('aria-controls');
                    $('#'+name).removeClass('show');
                }
            })
        </script>
    </body>

</html>
