<!-- Topnav -->
<nav class="navbar navbar-top navbar-expand navbar-dark border-bottom" style="background-color: #5F9EA0">
    <div class="container-fluid">
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Navbar links -->
        <ul class="navbar-nav align-items-center  ml-md-auto ">
          <li class="nav-item d-xl-none">
            <!-- Sidenav toggler -->
            <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
              <div class="sidenav-toggler-inner">
                <i class="sidenav-toggler-line"></i>
                <i class="sidenav-toggler-line"></i>
                <i class="sidenav-toggler-line"></i>
              </div>
            </div>
          </li>
          <li class="nav-item d-sm-none">
            <a class="nav-link" href="#" data-action="search-show" data-target="#navbar-search-main">
              <i class="ni ni-zoom-split-in"></i>
            </a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="ni ni-bell-55"></i>
              <span class="badge badge-default"> {{auth()->user()->unreadNotifications->count()}}  </span>
            </a>
            <div class="dropdown-menu dropdown-menu-xl  dropdown-menu-right  py-0 overflow-hidden">
              <!-- Dropdown header -->
              <div class="px-3 py-3">
                  @if (auth()->user()->unreadNotifications->count() == 0)
                      <h6 class="text-sm text-muted m-0">You have <strong class="text-primary"></strong> no new notifications.</h6>
                  @else
                      <h6 class="text-sm text-muted m-0">You have <strong class="text-primary">{{auth()->user()->unreadNotifications->count()}}</strong> notifications.</h6>
                  @endif
              </div>
              <a href="{{route('markAllRead')}}" class="dropdown-item text-center text-primary font-weight-bold py-3">Mark All As Read</a>
              <!-- List group -->
              <div class="list-group list-group-flush">
                  @foreach (auth()->user()->unreadNotifications()->get() as $n)
                  @php
                      $timeago = \illuminate\Support\Carbon::createFromTimeStamp(strtotime($n->created_at))->diffForHumans();
                  @endphp
                      <a href="{{route('markReadSingle',["$n->id"])}}" class="list-group-item list-group-item-action">
                          <div class="row align-items-center">
                          <div class="col-auto">
                              <!-- Avatar -->
                              <span class="label label-sm label-icon label-success">
                                  <i class="fa fa-check"></i>
                              </span>
                          </div>
                          <div class="col ml--2">
                              <div class="d-flex justify-content-between align-items-center">
                              <div class="text-right text-muted">
                                  <small>{{$timeago}}</small>
                              </div>
                              </div>
                              <p class="text-sm mb-0"> {{$n->data['notification']}}</p>
                          </div>
                          </div>
                      </a>
                  @endforeach

              </div>
              <!-- View all -->
              <a href="{{route('viewAllNotifications')}}" class="dropdown-item text-center text-primary font-weight-bold py-3">View all</a>
            </div>
          </li>
          <li class="nav-item dropdown">
              <a style="color: antiquewhite;" href="{{route('pos')}}"  data-close-others="true">
                  <i style="color: antiquewhite;" class="fa fa-bars"></i>
                  POS
              </a>
          </li>
          <li class="nav-item dropdown">
              <a style="color: antiquewhite;"  id="daily" data-close-others="true" style="text-decoration: none; margin-top: 5px" >
                  <i style="color: antiquewhite; margin-left: 20px;" class="fa fa-hourglass"></i>

              </a>
          </li>
        </ul>
        <ul class="navbar-nav align-items-center  ml-auto ml-md-0 ">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="{{url('')}}/style-lik/assets/layouts/layout/img/avatar3_small.jpg">
                </span>
                <div class="media-body  ml-2  d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold">{{Auth::user()->name}}</span>
                </div>
              </div>
            </a>
          @php
              $permission = \App\Permissions::where('route', 'users.editProfile')->first();
              $perm = \App\RolePermission::where('r_id',Auth::user()->r_id)
              ->where('p_id', $permission->id)->first();
              $href = url('').'/users/editProfile/'.Auth::user()->id;
          @endphp
            <div class="dropdown-menu  dropdown-menu-right ">
              <a href="{{$perm == null ? '#' : $href}}" class="dropdown-item">
                <i class="ni ni-single-02"></i>
                <span>My profile</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="{{route('logout')}}" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="ni ni-user-run"></i>
                <span>Logout</span>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                  @csrf
              </form>
              </a>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </nav>
<script>
    $(document).on('click','#daily',function(){
        $('#w_id').empty();
        $('#example5 tbody').empty();
        $.ajax({
            url:"{{url('')}}/dailyPLreport",
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                $('#title').text('DAY PROFIT AND/OR LOSS REPORT('+data[0]+')');
                $('#ware_id').append(`<option selected value="All"> All Warehouses</option>`)
                for (let i = 0; i < data[1].length; i++) {
                    $('#ware_id').append(`<option value="`+data[1][i].id+`">`+data[1][i].w_name+` - `+data[1][i].w_type+`</option>`)
                }


                var discount = 0;
                var rev = 0;
                var cost = 0;
                for(let j = 0; j < data[2].length; j++)
                {
                    discount+=data[2][j].total_quantity;
                    rev+=data[2][j].salecount;
                    cost+=data[2][j].purchase;
                }

                var total = rev - discount - cost - data[3];
                var status = 'Loss';
                if(total > 0)
                {
                    status = "Profit";
                }
                else
                {
                    status = 'Loss';
                }
                $('#example5').append(`<tr><td>Products Revenue</td><td>`+rev+`</td></tr>
                <tr><td>Order Discount</td><td>`+discount+`</td></tr><tr><td>Products Cost</td><td>`+cost+`</td></tr>
                <tr><td>Expenses</td><td>`+data[3]+`</td> </tr>
                <tr style="font-weight:bold"><td>`+status+`</td><td>`+total+`</td></tr>`);


                $('#dailyPLReportModal').modal("show");
            }
        });
    });

    $(document).on('change','#ware_id',function(){
        var id = $(this).val();
        $('#example5 tbody').empty();
        $.ajax({
            url:"{{url('')}}/dailyPLreport/warehouse/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                var discount = 0;
                var rev = 0;
                var cost = 0;
                for(let j = 0; j < data[0].length; j++)
                {
                    discount+=data[0][j].total_quantity;
                    rev+=data[0][j].salecount;
                    cost+=data[0][j].purchase;
                }
                var total = rev - discount - cost - data[1];
                var status = 'Loss';
                if(total >= 0)
                {
                    status = "Profit";
                }
                else
                {
                    status = 'Loss';
                }
                $('#example5').append(`<tr><td>Products Revenue</td><td>`+rev+`</td></tr>
                <tr><td>Order Discount</td><td>`+discount+`</td></tr><tr><td>Products Cost</td><td>`+cost+`</td></tr>
                <tr><td>Expense</td><td>`+data[1]+`</td></tr>
                <tr style="font-weight:bold"><td>`+status+`</td><td>`+total+`</td></tr>`);
            }
        });
    });

</script>
