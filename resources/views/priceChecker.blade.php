@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #5F9EA0;
    }
    .modal-header{
        background: #5F9EA0 !important;
    }
</style>
@toastr_css

@section('sidebar-name1')
<li class="breadcrumb-item"><a href="{{ url('')}}/price-checker">Price Checker</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="card">
                <div class="card-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="card-header"  style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-check font-white"></i>Price Checker
                                            </div>
                                            <div class="col-md-5 col-sm-5"></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="custom_datatable">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <select name="pid" id="pid"  class="form-control js-example-basic-single">
                                                        <option selected="" disabled value="">Select</option>
                                                        @foreach ($product as $p)
                                                            <option value={{$p->id}}>
                                                                {{$p->pro_code}} - {{$p->pro_name}}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <input type="text" id="p_id" placeholder="Scan Code" class="form-control" autofocus>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label hidden id="nameLbl" style="font-size: 20px"><B>Name</B></label>
                                                </div>
                                                <div class="col-sm-2"><label id="codeLbl" hidden style="font-size: 20px"><B>Name</B></label></div>
                                                <div class="col-sm-2"><img id="pimage" hidden src="" width="auto" height="120px"></div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-sm-2" id="mrp" hidden>
                                                    <img src="{{url('')}}/uploads/pc1.jpg" alt="">
                                                    <label style="margin-left: -90px; font-size: 15px; position: absolute;top:130px;font-weight: bold">MRP</label>
                                                    <label style="margin-left: -90px; font-size: 15px; position: absolute;top:153px;font-weight: bold" id="mrpLbl"></label>
                                                </div>
                                                <div class="col-sm-2" id="op" hidden>

                                                    <img src="{{url('')}}/uploads/pc1.jpg" alt="">
                                                    <label style="margin-left: -110px; font-size: 15px; position: absolute;top:130px;font-weight: bold">OUR PRICE</label>
                                                    <label style="margin-left: -80px; font-size: 15px; position: absolute;top:153px;font-weight: bold" id="opLbl"></label>
                                                </div>
                                                <div class="col-sm-2" id="ys" hidden>

                                                    <img src="{{url('')}}/uploads/pc1.jpg" alt="">
                                                    <label style="margin-left: -110px; font-size: 15px; position: absolute;top:130px;font-weight: bold">YOU SAVE</label>
                                                    <label style="margin-left: -80px; font-size: 15px; position: absolute;top:153px;font-weight: bold" id="ysLbl"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
    @toastr_js
    @toastr_render
    <script type="text/javascript" src="{{url('')}}/style-lik/jQuery-Scanner-Detection-master/jquery.scannerdetection.js"></script>
    <script>


        $(document).ready(function() {
            $('.js-example-basic-single').select2();
        });


    $(document).on('change','#pid',function(){
        var p_id = $(this).val();
        $.ajax({
            url:"{{url('')}}/product/productsearch/"+p_id,
            method:"GET",
            data:
            {
                p_id:p_id,
            },
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                console.log(data);
                $('#nameLbl').show();
                $('#nameLbl').text('Product Name: '+data.pro_name);
                $('#codeLbl').show();
                $('#codeLbl').text('BarCode: '+data.pro_code);
                if (data.image == null) {
                    $('#pimage').attr('src','{{url('')}}/uploads/noimg.png');
                    $('#pimage').show();
                } else {

                    $('#pimage').attr('src','{{url('')}}/uploads/'+data.image);
                    $('#pimage').show();
                }
                if (data.mrp == null) {
                    $('#op').show();
                    $('#ys').hide();
                    $('#mrp').hide();
                    $('#opLbl').text(data.price);
                } else {
                    $('#op').show();
                    $('#opLbl').text(data.price);
                    $('#mrp').show();
                    $('#mrpLbl').text(data.mrp);
                    $('#ys').show();
                    $('#ysLbl').text(data.mrp - data.price);
                }
            }
        });
    });
    function code(c)
    {
        var rowCount = $('#example tr').length ;
        var length = c.length;
        if(length > 6)
        {
            $.ajax({
                url:"{{url('')}}/pos-detail/product/code/"+c,
                method:"GET",
                error: function (request, error) {
                            console.log(" Can't do because: " + error +request);
                        },
                success:function(data){
                    $('#nameLbl').show();
                    $('#nameLbl').text('Product Name: '+data.pro_name);
                    $('#codeLbl').show();
                    $('#codeLbl').text('BarCode: '+data.pro_code);
                    if (data.image == null) {
                        $('#pimage').attr('src','{{url('')}}/uploads/noimg.png');
                        $('#pimage').show();
                    } else {

                        $('#pimage').attr('src','{{url('')}}/uploads/'+data.image);
                        $('#pimage').show();
                    }
                    if (data.mrp == null) {
                        $('#op').show();
                        $('#ys').hide();
                        $('#mrp').hide();
                        $('#opLbl').text(data.price);
                    } else {
                        $('#op').show();
                        $('#opLbl').text(data.price);
                        $('#mrp').show();
                        $('#mrpLbl').text(data.mrp);
                        $('#ys').show();
                        $('#ysLbl').text(data.mrp - data.price);
                    }
                }
            });
        }
    }

    $(window).scannerDetection();
    $(window).bind('scannerDetectionComplete',function(e,data){
        console.log('complete '+data.string);
        $("#pid").val(data.string);
        code(data.string);
        $('#pid').val('');
    })
    .bind('scannerDetectionError',function(e,data){
        console.log('detection error '+data.string);
    })
    .bind('scannerDetectionReceive',function(e,data){
        console.log('Recieve');
        console.log(data.evt.which);
    });
    </script>


    @endsection
@endsection
