@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #5F9EA0;
    }
    .modal-header{
        background: #5F9EA0 !important;
    }
</style>
@toastr_css

@section('sidebar-name1')
<li class="breadcrumb-item"><a href="{{ url('')}}/stocks">Stock</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="card">
                <div class="card-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="card-header"  style="background: #32c5d2;">

                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-tasks font-white"></i>Current Stock
                                            </div>
                                            <div class="col-md-5 col-sm-5"></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                                @if(in_array('excel',$permissions))
                                                    <a style="margin-left:-20px;color: white;font-size: 25px;"  href="{{route('stocks.excel')}}">
                                                        <i class="fas fa-file-excel  font-white"></i>
                                                    </a>
                                                @endif
                                                @if(in_array('pdf',$permissions))
                                                    <a style="margin-left:-65px;color: white;font-size: 25px;" href="{{route('stocks.pdf')}}">
                                                        <i class="fas fa-file-pdf  font-white"></i>
                                                    </a>
                                                @endif
                                                @if(in_array('Add',$permissions))
                                                    <a id="GFG" href="{{route('stocks.create')}}">
                                                        <button style="margin-left:20px; margin-top:-40px" type="button" class="btn btn-block btn-info btn-md ">Add Stock</button>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Product Name</label>
                                                            <select class="form-control js-example-basic-single" name="p_name" >
                                                                <option value=""  selected>Select...</option>
                                                                @foreach ($product as $s)
                                                                    @if ($s->type == 0)
                                                                        <option value="{{$s->products->pro_code}} - {{$s->products->pro_name}}">{{$s->products->pro_code}} - {{$s->products->pro_name}} - {{$s->products->p_type}} - {{$s->products->brands->b_name}}</option>
                                                                    @else
                                                                        {{-- @foreach ($s->variants as $v) --}}
                                                                        <option value="{{$s->variant->name}}">{{$s->variant->name}} - {{$s->products->p_type}} - {{$s->variant->product->brands->b_name}}</option>
                                                                        {{-- @endforeach --}}
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Warehouse Name</label>
                                                            <select class="form-control js-example-basic-single" name="w_name" >
                                                                <option value="" selected>Select...</option>
                                                                @foreach ($warehouse as $u)
                                                                <option  value="{{$u->w_name}}">{{$u->w_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">WareHouse Type</label>
                                                            <select class="form-control js-example-basic-single" name="w_type" id="w_type" >
                                                                <option value="" selected>Select...</option>
                                                                <option value="Standard">Standard</option>
                                                                <option value="Finished">Finished</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Code</label>
                                                            <input type="text" name="code"  class="form-control" placeholder="Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Brand Name</label>
                                                            <select class="form-control js-example-basic-single" name="b_name" >
                                                                <option value="" selected>Select...</option>
                                                                @foreach ($brand as $u)
                                                                <option  value="{{$u->b_name}}">{{$u->b_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Category Name</label>
                                                            <select class="form-control js-example-basic-single" name="c_name" id="cat_id" >
                                                                <option value="" selected>Select...</option>
                                                                @foreach ($cat as $u)
                                                                <option  value="{{$u->cat_name}}">{{$u->cat_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Product Type</label>
                                                            <select name="ptype" class="form-control js-example-basic-single ">
                                                            <option selected="" value="">No Filter</option>
                                                            <option>Raw Material</option>
                                                            <option>Packaging</option>
                                                            <option>Finished</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        {{-- <label for="">Unit Name</label> --}}
                                                        <label for="" style="visibility: hidden">.</label>
                                                        <button style="background: #32c5d2" id="search" style="color: " class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Warehouse Name</th>
                                                            <th>Warehouse Type</th>
                                                            <th>Code - Product Name</th>
                                                            <th>Product Type</th>
                                                            <th>Weight</th>
                                                            <th>Brand</th>
                                                            <th>Category</th>
                                                            <th>Sub-Category</th>
                                                            <th>Quantity</th>
                                                            <th>Cost</th>
                                                            <th>Price</th>
                                                            <th>MRP</th>
                                                            <th>Total Cost</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a = 1;
                                                            $cost = 0 ;
                                                            $count = 0 ;
                                                            $price = 0 ;
                                                            $count1 = 0 ;
                                                        @endphp
                                                        @foreach ($stock as $s)
                                                            <tr>
                                                                <td>
                                                                    {{$a}}
                                                                </td>
                                                                <td>
                                                                    {{$s->warehouse->w_name}}
                                                                </td>
                                                                <td>
                                                                    {{$s->warehouse->w_type}}
                                                                </td>
                                                                @if ($s->type == 1)
                                                                    <td>
                                                                        {{$s->variant->name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->p_type}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->variant->product->weight == null ? $s->variant->product->unit->u_name : $s->variant->product->weight.$s->variant->product->unit->u_name }}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->variant->product->brands->b_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->variant->product->category->cat_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->variant->product->subcategory->s_cat_name}}
                                                                    </td>
                                                                @else
                                                                    <td>
                                                                        {{$s->products->pro_code}} - {{$s->products->pro_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->p_type}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->weight == null ? $s->products->unit->u_name : $s->products->weight.$s->products->unit->u_name }}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->brands->b_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->category->cat_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->subcategory->s_cat_name}}
                                                                    </td>
                                                                @endif
                                                                <td>
                                                                    {{$s->quantity}}
                                                                </td>
                                                                <td>
                                                                    @if ($s->stock->isEmpty())
                                                                        {{$s->type == 1 ? $s->variant->cost : $s->products->cost}}
                                                                    @else
                                                                        @php
                                                                            $count = count($s->stock);
                                                                        @endphp
                                                                        @foreach ($s->stock as $p)
                                                                            @php
                                                                                $cost+=$p->cost;
                                                                            @endphp
                                                                        @endforeach
                                                                        @if ($count > 0)
                                                                        @php
                                                                            $finalcost = $cost/$count;
                                                                        @endphp
                                                                        {{$cost/$count}}
                                                                        @else
                                                                        {{0}}
                                                                        @php
                                                                            $finalcost = $s->type == 1 ? $s->variant->cost : $s->products->cost;
                                                                        @endphp
                                                                        @endif
                                                                    @endif
                                                                </td>
                                                                @if ($s->sales->isEmpty())
                                                                    <td>
                                                                        {{$s->type == 1 ? $s->variant->price : $s->products->price}}
                                                                    </td>
                                                                @else
                                                                    @php
                                                                        $count1 = count($s->sales);
                                                                    @endphp
                                                                    @foreach ($s->sales as $t)
                                                                        @php
                                                                            $price+=$t->price;
                                                                        @endphp
                                                                    @endforeach
                                                                    <td>
                                                                        @if ($count1 > 0)
                                                                        {{round($price/$count1)}}
                                                                        @else
                                                                        {{0}}
                                                                        @endif

                                                                    </td>
                                                                @endif
                                                                <td>{{$s->type == 1 ? $s->variant->mrp : $s->products->mrp}}</td>
                                                                <td>{{$s->quantity * ($finalcost )}}</td>
                                                            </tr>
                                                            @php
                                                                $a++;
                                                                $count = 0 ;
                                                                $count1 = 0 ;
                                                                $price = 0 ;
                                                                $cost = 0 ;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th style="font-weight: bold">TOTAL</th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                    </tfoot>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
    @toastr_js
    @toastr_render
        <script type="text/javascript">
            $(document).ready(function () {
                $('.js-example-basic-single').select2()
                var table = $('#example').DataTable({
                    scrollX: true,
                    "footerCallback": function ( row, data, start, end, display ) {
                        var api = this.api(), data;

                        // Remove the formatting to get integer data for summation
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };

                        // Total over all pages
                        totalqty = api
                            .column( 9 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );
                        totalcost = api
                            .column( 10 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );
                        totalprice = api
                            .column( 11 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

                            tc= api
                            .column( 13 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );


                            mrp= api
                            .column( 12 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );

                        // Update footer
                        $( api.column( 9 ).footer() ).html(totalqty);
                        $( api.column( 10 ).footer() ).html(totalcost);
                        $( api.column( 11 ).footer() ).html(totalprice);
                        $( api.column( 12 ).footer() ).html(mrp);
                        $( api.column( 13 ).footer() ).html(tc);
                    }
                });

                $('#advanceSearch').submit(function(e){
                e.preventDefault();

                table.columns(1).search($('select[name="w_name"]').val());
                table.columns(2).search($('select[name="w_type"]').val());
                table.columns(3).search($('input[name="code"]').val());
                table.columns(3).search($('select[name="p_name"]').val());
                table.columns(4).search($('select[name="ptype"]').val());
                table.columns(6).search($('select[name="b_name"]').val());
                table.columns(7).search($('select[name="c_name"]').val());
                table.draw();
            });
        });
    </script>
    @endsection
@endsection
