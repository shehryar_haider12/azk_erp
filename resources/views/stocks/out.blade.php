@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #5F9EA0;
    }
    .modal-header{
        background: #5F9EA0 !important;
    }
    .form-check-inline {
    display: inline-flex;
    align-items: center;
    padding-left: 0;
    margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
    position: static;
    margin-top: 0;
    margin-right: .3125rem;
    margin-left: 0;
    }
</style>
@toastr_css

@section('sidebar-name1')
<li class="breadcrumb-item"><a href="{{ url('')}}/stockout">Stock Out History</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="card">
                <div class="card-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="card-header"  style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-tasks font-white"></i>Stock Out History

                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="custom_datatable">
                                            <form action="{{url('')}}/stockout/stockOutSearch" method="POST" id="advanceSearch">
                                                @csrf

                                                <div class="tableview">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio2">
                                                            <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Date
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Product Name</label>
                                                                <select class="form-control js-example-basic-single" name="p_id" id="p_name" disabled>
                                                                    <option value=""  selected>Select...</option>
                                                                    @foreach ($product as $s)
                                                                        @if ($s->vstatus == 0)
                                                                            <option value="{{$s->id.'-'.'0'}}">{{$s->pro_code}} - {{$s->pro_name}} - {{$s->p_type}} - {{$s->brands->b_name}}</option>
                                                                        @else
                                                                            @foreach ($s->variants as $v)
                                                                                <option value="{{$v->id.'-'.'1'}}">{{$v->name}} - {{$s->p_type}} - {{$s->brands->b_name}}</option>
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Year</label>
                                                                <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Month</label>
                                                                <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">From</label>
                                                                <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">To</label>
                                                                <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="" style="visibility: hidden">.</label>
                                                                <button disabled id="search" style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                                <i class="fa fa-search pr-1"></i> Search</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Code - Product Name</th>
                                                            <th>Price</th>
                                                            <th>Warehouse</th>
                                                            <th>Brand</th>
                                                            <th>Category</th>
                                                            <th>Out Quantity</th>
                                                            <th>Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a = 1;
                                                            $tp=0;
                                                            $tq=0;
                                                        @endphp
                                                        @foreach ($stock as $s)
                                                            <tr>
                                                                <td>
                                                                    {{$a}}
                                                                </td>
                                                                @if ($s->type == 1)
                                                                    <td>
                                                                        {{$s->variant->name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->price}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->warehouse->w_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->variant->product->brands->b_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->variant->product->category->cat_name}}
                                                                    </td>
                                                                @else
                                                                    <td>
                                                                        {{$s->products->pro_code}} - {{$s->products->pro_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->price}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->warehouse->w_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->brands->b_name}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s->products->category->cat_name}}
                                                                    </td>
                                                                @endif
                                                                <td>
                                                                    {{$s->quantity}}
                                                                </td>
                                                                <td>
                                                                    {{$s->stock_date}}
                                                                </td>
                                                            </tr>
                                                            @php
                                                                $a++;
                                                                $tp+=$s->price;
                                                                $tq+=$s->quantity;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td>TOTAL</td>
                                                            <td></td>
                                                            <td>{{$tp}}</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>{{$tq}}</td>
                                                            <td></td>
                                                        </tr>
                                                    </tfoot>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
    @toastr_js
    @toastr_render
    <script type="text/javascript">
        $(document).ready(function () {
            $('.js-example-basic-single').select2();
            var table = $('#example').DataTable({
                order: [[ 0, "desc" ]],
                scrollX: true,
            });
            $('input:radio[name="optradio"]').change(function(){
                if ($(this).is(':checked')) {
                    $('#search').prop('disabled',false);
                    var val = $(this).val();
                    if(val == 'Year')
                    {
                        $('#year').prop('disabled',false);
                        $('#year').attr('required',true);
                        $('#month').attr('required',false);
                        $('#month').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').prop('disabled',true);
                        $('#to').attr('required',false);
                        $('#p_name').prop('disabled',false);
                        $('#c_name').prop('disabled',false);
                    }
                    if(val == 'Month')
                    {
                        $('#month').prop('disabled',false);
                        $('#c_name').prop('disabled',false);
                        $('#p_name').prop('disabled',false);
                        $('#month').attr('required',true);
                        $('#year').prop('disabled',true);
                        $('#from').prop('disabled',true);
                        $('#to').prop('disabled',true);
                        $('#from').attr('required',false);
                        $('#to').attr('required',false);
                        $('#year').attr('required',false);
                    }
                    if(val == 'Date')
                    {
                        $('#from').prop('disabled',false);
                        $('#from').attr('required',true);
                        $('#to').prop('disabled',false);
                        $('#to').attr('required',false);
                        $('#c_name').prop('disabled',false);
                        $('#p_name').prop('disabled',false);
                        $('#month').attr('required',false);
                        $('#year').prop('disabled',true);
                        $('#month').prop('disabled',true);
                        $('#year').attr('required',false);
                    }
                }
            });

        });

    </script>
    @endsection
@endsection
