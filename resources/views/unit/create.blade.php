@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
    <li class="breadcrumb-item"><a href="{{ url('')}}/unit">Unit</a></li>
    <li class="breadcrumb-item"><span>{{$isEdit ? 'Edit' : 'Add'}} Unit</span></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="card" id="form_wizard_1">
                <div class="card-header">
                    <div class="caption">
                        <i class=" fa fa-balance-scale font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Unit</span>
                    </div>

                </div>
                <div class="card-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('unit.update',$unit->id) :  route('unit.store')}} " class="form-horizontal" method="POST"  id="unitForm">
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-outline">
                                        <label >Unit Name<span class="text-danger">*</span></label>
                                        <input value="{{$unit->u_name ?? old('u_name')}}" class="form-control" type="text" placeholder="Enter Unit Name" name="u_name" >
                                        <span class="text-danger">{{$errors->first('u_name') ? 'Unit already exist or Enter unit' : null}}</span>
                                    </div>
                                </div>
                            </div>
                            <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>

@endsection
@section('custom-script')
<script>
    $('#unitForm').validate({
        rules: {
            u_name: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>
@toastr_js
@toastr_render
@endsection
