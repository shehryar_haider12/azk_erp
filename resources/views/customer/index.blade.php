@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #5F9EA0;
    }
    .modal-header{
        background: #5F9EA0 !important;
    }
</style>
@toastr_css">

@section('sidebar-name1')
<li class="breadcrumb-item"><a href="{{ url('')}}/customer">Customer</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="card">
                <div class="card-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="card-header"  style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-user font-white"></i>View Customers
                                            </div>
                                            <div class="col-md-5 col-sm-5"></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                                @if(in_array('excel',$permissions))
                                                    <a style="margin-left:-20px;color: white;font-size: 25px;"  href="{{route('customer.excel')}}">
                                                        <i class="fas fa-file-excel font-white"></i>
                                                    </a>
                                                @endif
                                                @if(in_array('pdf',$permissions))
                                                    <a id="GFG" style="margin-left:-65px;color: white;font-size: 25px;" href="{{route('customer.pdf')}}">
                                                        <i class="fas fa-file-pdf  font-white"></i>
                                                    </a>
                                                @endif
                                                @if(in_array('Add',$permissions))
                                                    <a id="GFG" href="{{route('customer.create')}}" >
                                                        <button style="margin-left:20px; margin-top:-40px" type="button"  class="btn btn-block btn-info btn-md ">Add Customer</button>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Customer Name</label>
                                                        <input type="text" name="v_name" id="autocomplete-ajax1" class="form-control" placeholder="Customer Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Serial No</label>
                                                            <input type="text" name="sno" id="autocomplete-ajax1" class="form-control" placeholder="Serial No" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                        </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Area Name</label>
                                                            <select class="form-control js-example-basic-single" name="a_name" id="a_name" >
                                                                <option value=""  selected>Select...</option>
                                                                @foreach ($area as $u)
                                                                <option  value="{{$u->name}}">{{$u->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8"></div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        {{-- <label for="">Unit Name</label> --}}
                                                        {{-- <label for="" style="visibility: hidden">.</label> --}}
                                                        <button style="background: #32c5d2" id="search" style="color: " class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="examples" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Serial#</th>
                                                            <th>Name</th>
                                                            <th>Company</th>
                                                            <th>Area</th>
                                                            <th width="15%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

    @section('modal')
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Customer</h4>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Customer Name</label>
                                    <input class="form-control" type="text"  id="name" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Balance</label>
                                    <input class="form-control" type="text" id="balance" readonly >
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Customer Group</label>
                                    <input class="form-control" type="text"  id="cgroup" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Price Group</label>
                                    <input  class="form-control" type="text" id="pgroup" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Company</label>
                                    <input class="form-control" type="text" id="company" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Address</label>
                                    <input  class="form-control" type="text" id="address" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Contact Number</label>
                                    <input class="form-control" type="text" id="c_no" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Country</label>
                                    <input  class="form-control" type="text" id="country" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >NTN</label>
                                    <input class="form-control" type="text"  id="vat" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >GST</label>
                                    <input  class="form-control" type="text"  id="gst" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >State</label>
                                    <input class="form-control" type="text"  id="state" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Email</label>
                                    <input  class="form-control" type="text"  id="email" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Postal Code</label>
                                    <input class="form-control" type="text"  id="pc" readonly >
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >City</label>
                                    <input  class="form-control" type="text"  id="city" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
            <!--End Modal-->

    @endsection
    @section('custom-script')
    @toastr_js
    @toastr_render
        <script>
            function view(id)
            {
                $.ajax({
                    url:"{{url('')}}/customer/"+id,
                    method:"GET",
                    error: function (request, error) {
                                alert(" Can't do because: " + error +request);
                            },
                    success:function(data){
                        console.log(data);
                        $('#name').val(data.name);
                        $('#company').val(data.company);
                        $('#address').val(data.address);
                        $('#c_no').val(data.c_no);
                        $('#country').val(data.country);
                        $('#vat').val(data.VAT);
                        $('#balance').val(data.balance);
                        $('#gst').val(data.GST);
                        $('#email').val(data.email);
                        $('#state').val(data.state);
                        $('#pc').val(data.postalCode);
                        $('#city').val(data.city == null ? '' : data.city.c_name);
                        $('#cgroup').val(data.cgroup.name);
                        $('#pgroup').val(data.pgroup.name);
                    }
                });
            }
            $(document).on('click','.view',function(){
                var id=$(this).attr("id");
                view(id);
            });

            $(document).ready(function () {
                $('.js-example-basic-single').select2();

                var table = $('#examples').DataTable({
                    processing: true,
                    order: [[ 0, "desc" ]],
                    serverSide: true,
                    ajax: '{{route("customer.datatable")}}',
                    "columns": [{
                            "data": "id",
                            "defaultContent": ""
                        },
                        {
                            "data": "serial_no",
                            "defaultContent": ""
                        },
                        {
                            "data": "name",
                            "defaultContent": ""
                        },
                        {
                            "data": "company",
                            "defaultContent": ""
                        },
                        {
                            "data": "area.name",
                            "defaultContent": ""
                        },
                        {
                            "data": "id",
                            "defaultContent": ""
                        },
                    ],
                    "columnDefs": [{
                            "targets": 'no-sort',
                            "orderable": false,
                        },
                        {
                            "targets": 0,
                            "render": function (data, type, row, meta) {
                                var id = meta.row + 1;
                                return `<input class="form-control" type="text" readonly data-id="`+row.id+`" style="background: transparent;border: none;" value="`+id+`">`;
                            },
                        },
                        {
                            "targets": -1,
                            "render": function (data, type, row, meta) {
                                var edit = '{{route("customer.edit",[":id"])}}';
                                edit = edit.replace(':id', row.id );
                                var checked = row.status == 1 ? 'checked' : null;
                                return `
                                @if(in_array('edit',$permissions))
                                    <a id="GFG" href="` + edit + `" class="text-info p-1">
                                        <button type="button" class="btn blue edit" >
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </a>
                                @endif
                                @if(in_array('show',$permissions))
                                    <button type="button" data-target="#myModal" data-toggle="modal"  class="btn green view" id="`+row.id +`">
                                        <i class="fa fa-eye"></i>
                                    </button>
                                @endif
                                @if(in_array('status',$permissions))
                                    <input class="status" type="checkbox" data-plugin="switchery" data-color="#005CA3" data-size="small" ` +
                                    checked + ` value="` + row.id + `">
                                @endif
                                `;
                            },
                        },
                    ],
                    "drawCallback": function (settings) {
                    var elems = Array.prototype.slice.call(document.querySelectorAll('.status'));
                    if (elems)
                    {
                        elems.forEach(function (html)
                        {
                            var switchery = new Switchery(html,
                            {
                                color: '#007bff',
                                 secondaryColor: '#dfdfdf',
                                 jackColor: '#fff',
                                 jackSecondaryColor: null,
                                 className: 'switchery',
                                 disabled: false,
                                 disabledOpacity: 0.5,
                                 speed: '0.1s',
                                 size: 'small'
                            });

                        });
                    }
                    $('.status').change(function () {
                        var $this = $(this);
                        var id = $this.val();
                        var status = this.checked;

                        if (status) {
                            status = 1;
                        } else {
                            status = 0;
                        }
                        // console.log(status);
                        axios
                            .post('{{route("customer.status")}}', {
                            _token: '{{csrf_token()}}',
                            _method: 'post',
                            id: id,
                            status: status,
                            })
                            .then(function (responsive) {
                            console.log('responsive');
                            // location.reload();
                            })
                            .catch(function (error) {
                            console.log(error);
                            });
                        });

                    },

                });
                $('#advanceSearch').submit(function(e){
                    e.preventDefault();
                    table.columns(1).search($('input[name="sno"]').val());
                    table.columns(2).search($('input[name="v_name"]').val());
                    table.columns(4).search($('select[name="a_name"]').val());
                    table.draw();
                });
            });
            $('#examples').on('click','td', function () {
                var col_index = $(this).index();
                var row_index = $(this).parent().index();
                var count = document.getElementById('examples').rows[row_index].cells.length - 1;
                if(col_index <count)
                {
                    var $tr = $(this).closest('tr');
                    var id = $tr.find("td:eq(0) input[type='text']").attr('data-id');
                    view(id);
                    $('#myModal').modal('show');
                }
            });
        </script>
    @endsection
@endsection
