@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li class="breadcrumb-item"><a href="{{ url('')}}/headofaccounts">Head Of Accounts</a></li>
<li class="breadcrumb-item"><span>{{$isEdit ? 'Edit' : 'Add'}} Head Of Accounts</span></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="card" id="form_wizard_1">
                <div class="card-header">
                    <div class="caption">
                        <i class="  fa fa-money-bill-alt font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Head Of Accounts</span>
                    </div>

                </div>
                <div class="card-body">
                    <!-- BEGIN FORM-->

                    <form action="{{$isEdit ? route('headofaccounts.update',$hoa->id) :  route('headofaccounts.store')}}" class="form-horizontal" method="POST" id="HoAForm">
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-outline">
                                        <label >Head of Account Name<span class="text-danger">*</span></label>
                                        <input value="{{$hoa->name ?? old('name')}}" class="form-control" type="text" placeholder="Enter HOA Name" name="name" >
                                        <span class="text-danger">{{$errors->first('name') ? 'Already exist or Enter name' : null}}</span>
                                    </div>
                                </div>
                            </div>
                            <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0 ">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>

@endsection
@section('custom-script')
<script>
    $('#HoAForm').validate({
        rules: {
            name: {
                required: true,
            },

        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>
@toastr_js
@toastr_render
@endsection
