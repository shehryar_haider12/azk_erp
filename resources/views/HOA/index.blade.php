@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #5F9EA0;
    }
    .modal-header{
        background: #5F9EA0 !important;
    }
</style>
@toastr_css

@section('sidebar-name1')
<li class="breadcrumb-item"><a href="{{ url('')}}/headofaccounts">Head Of Accounts</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="card">
                <div class="card-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="card-header" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fa fa-money-bill-alt font-white"></i>View Head Of Accounts
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="custom_datatable">
                                            <div class="table-responsive">
                                            <table id="example" class="table  table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Name</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($hoa as $a)
                                                            <tr>
                                                                <td>
                                                                    {{$a->id}}
                                                                </td>
                                                                <td>
                                                                    {{$a->name}}
                                                                </td>
                                                                <td>
                                                                    @if(in_array('edit',$permissions))
                                                                    <a id="GFG" href="{{url('')}}/headofaccounts/{{$a->id}}/edit" class="text-info p-1">
                                                                        <button type="button" class="btn blue edit" >
                                                                            <i class="fa fa-edit"></i>
                                                                        </button>
                                                                    </a>
                                                                    @else
                                                                        <button disabled type="button" class="btn blue edit" >
                                                                            <i class="fa fa-edit"></i>
                                                                        </button>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
@section('custom-script')
@toastr_js
@toastr_render
    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('#example').DataTable();
        });

    </script>
@endsection
@endsection
