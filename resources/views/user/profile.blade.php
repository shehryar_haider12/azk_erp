@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
    <li class="breadcrumb-item"><a href="#">Users</a></li>
    <li class="breadcrumb-item"><span>Update Profile</span></li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="card" id="form_wizard_1">
                <div class="card-header">
                    <h3 class="mb-0">
                        <i class=" fa fa-user font-green"></i>
                        Update Profile</h3>
                  </div>
                <div class=" card-body">
                    <!-- BEGIN FORM-->
                    <form action="{{ route('users.update',$user->id) }} " class="form-horizontal" method="PUT" >
                        @csrf
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >User Name<span class="text-danger">*</span></label>
                                        <input readonly value="{{$user->name ?? old('name')}}"  class="form-control" type="text" placeholder="Enter User Name" name="name" >
                                        <span class="text-danger">{{$errors->first('name') ?? null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Email<span class="text-danger">*</span></label>
                                        <input readonly value="{{$user->email ?? old('email')}}" class="form-control" type="email" placeholder="Enter Email" name="email" >
                                        <span class="text-danger">{{$errors->first('email') ?? null}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Password<span class="text-danger">*</span></label>
                                        <input type="password" name="password" class="form-control"  >
                                    </div>
                                </div>
                            </div>
                            <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>


@endsection
@section('custom-script')
@toastr_js
@toastr_render
@endsection
