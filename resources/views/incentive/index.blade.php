@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #5F9EA0;
    }
    .modal-header{
        background: #5F9EA0 !important;
    }
</style>
@toastr_css

@section('sidebar-name1')
<li class="breadcrumb-item"><a href="{{ url('')}}/incentives">Sale Incentives</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="card">
                <div class="card-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="card-header"  style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-users font-white"></i>View Sale Incentives
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">

                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Sale Persons</label>
                                                        <select  name="sp" class="form-control js-example-basic-single">
                                                            <option selected="" value="">No Filter</option>
                                                            @foreach ($vendor as $v)
                                                                <option>{{$v->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Month</label>
                                                            <input type="month" name="month" id="autocomplete-ajax1" class="form-control" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                        </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        {{-- <label for="">Unit Name</label> --}}
                                                        <label for="" style="visibility: hidden">.</label>
                                                        <button style="background: #32c5d2" id="search" style="color: " class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Sale Person Name</th>
                                                            <th>Month</th>
                                                            <th>Amount</th>
                                                            <th>Status</th>
                                                            <th width="15%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td>TOTAL</td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                    </tfoot>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('modal')
        <div id="Sale" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Sales History</h4>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example2" class="table table-responsive table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Sale No</th>
                                            <th>Sale Date</th>
                                            <th>Total</th>
                                            <th>Warehouse</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>


                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

    @endsection


    @section('custom-script')
    @toastr_js
@toastr_render

        <script type="text/javascript">
            $(document).ready(function () {
                $('.js-example-basic-single').select2();

                var table = $('#example').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: '{{route("incentives.datatable")}}',
                    "columns": [{
                            "data": "id",
                            "defaultContent": ""
                        },
                        {
                            "data": "saleperson.name",
                            "defaultContent": ""
                        },
                        {
                            "data": "month",
                            "defaultContent": "",
                        },
                        {
                            "data": "amount",
                            "defaultContent": ""
                        },
                        {
                            "data": "status",
                            "defaultContent": "",
                            "render": function (data, type, row, meta) {
                                if(row.status == 'Pending')
                                {
                                    return `<button type="button" class="btn btn-xs  btn-warning" >
                                        `+row.status+`
                                        </button>`;
                                }
                                else
                                {
                                    return `<button type="button" class="btn btn-xs  btn-success" >
                                        `+row.status+`
                                        </button>`;
                                }
                            },
                        },
                        {
                            "data": "id",
                            "defaultContent": ""
                        },
                    ],
                    "columnDefs": [{
                            "targets": 'no-sort',
                            "orderable": false,
                        },
                        {
                            "targets": 0,
                            "render": function (data, type, row, meta) {
                                return meta.row + 1;
                            },
                        },
                        {
                            "targets": -1,
                            "render": function (data, type, row, meta) {
                                if(row.status == "Pending")
                                {
                                    return `
                                    <select style="margin-top:5px" data-month="`+row.month+`" class="form-control action" id="`+row.sp_id +`" >
                                    <option >Actions</option>
                                    <option >Sale Details</option>
                                    <option >Approved</option>

                                    </select>`;
                                }
                                else
                                {
                                    return `
                                    <select style="margin-top:5px" data-month="`+row.month+`" class="form-control action" id="`+row.sp_id +`" >
                                    <option >Actions</option>
                                    <option >Sale Details</option>

                                    </select>`;
                                }
                            },
                        },
                    ],
                    "footerCallback": function( tfoot, data, start, end, display ) {
                        var amount = 0;
                        // console.log(data);
                        for (var i = 0; i < data.length; i++) {
                            amount = amount + parseFloat(data[i]['amount']);
                        }
                        $( table.column( 3 ).footer() ).html(Number.isNaN(amount) ? '0' : amount);
                    },

                });

                $('#advanceSearch').submit(function(e){
                e.preventDefault();
                $month = moment($('input[name="month"]').val()).format('MMM-YY');
                table.columns(2).search($month);
                table.columns(1).search($('select[name="sp"]').val());
                table.draw();
            });
        });

        $(document).on('change','.action',function(){
        var val=$(this).val();

        if(val == 'Sale Details')
        {
            var id=$(this).attr("id");
            var month=$(this).attr("data-month");
            $("#example2 tbody").empty();
            $.ajax({
                url:"{{url('')}}/saleperson/saledetails/"+month+"/"+id,
                method:"GET",
                error: function (request, error) {
                    lert(" Can't do because: " + error +request);
                },
                success:function(data){
                    // console.log(data);
                    for (let i = 0; i < data.length; i++) {
                        $("#example2").append("<tr><td>"+data[i].id+"</td><td>"+data[i].sale_date+"</td><td>"+data[i].total+"</td><td>"+data[i].warehouse.w_name+"</td></tr>");
                    }
                    $('#Sale').modal("show");
                    $('.action').val('Actions');
                    $('#example2').DataTable();

                }
            });
        }
        if(val == 'Approved')
        {
            var id=$(this).attr("id");
            var month=$(this).attr("data-month");
            var status = val;
            axios
            .post('{{route("incentives.status")}}', {
                _token: '{{csrf_token()}}',
                _method: 'post',
                id: id,
                status: status,
                month: month,
                })
                .then(function (responsive) {
                console.log('responsive');
                location.reload();
                })
                .catch(function (error) {
                console.log(error);
            });
        }
    });

        </script>
    @endsection
@endsection
