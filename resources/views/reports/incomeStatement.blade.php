@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #5F9EA0;
    }
    .modal-header{
        background: #5F9EA0 !important;
    }
    .form-check-inline {
    display: inline-flex;
    align-items: center;
    padding-left: 0;
    margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
    position: static;
    margin-top: 0;
    margin-right: .3125rem;
    margin-left: 0;
    }
</style>
@toastr_css

@section('sidebar-name1')
<li class="breadcrumb-item"><a>Reports</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="card">
                <div class="card-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="card-header" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fas fa-chart-bar font-white"></i>INCOME STATEMENT
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">
                                                @if ($index == 0)
                                                <a style="margin-left:200px; color: white;font-size: 25px"  href="{{route('reports.incomeStatement.excel')}}">
                                                    <i class="fas fa-file-excel  font-white"></i>
                                                </a>
                                                <a style="margin-left:-50px; font-size: 25px; color: white"  href="{{route('reports.incomeStatement.pdf')}}">
                                                    <i class="fas fa-file-pdf  font-white"></i>
                                                </a>
                                                @endif

                                                @if ($index == 1)
                                                <a style="margin-left:200px; color: white;font-size: 25px"  href="{{url('')}}/reports/incomeStatementsearch/{{$year}}/excel/year">
                                                    <i class="fas fa-file-excel  font-white"></i>
                                                </a>
                                                <a style="margin-left:-50px; font-size: 25px; color: white"  href="{{url('')}}/reports/incomeStatementsearch/{{$year}}/pdf/year">
                                                    <i class="fas fa-file-pdf  font-white"></i>
                                                </a>
                                                @endif
                                                @if ($index == 2)
                                                <a style="margin-left:200px; color: white;font-size: 25px"  href="{{url('')}}/reports/incomeStatementsearch/{{$month}}/excel/month">
                                                    <i class="fas fa-file-excel  font-white"></i>
                                                </a>
                                                <a style="margin-left:-50px; font-size: 25px; color: white"  href="{{url('')}}/reports/incomeStatementsearch/{{$month}}/pdf/month">
                                                    <i class="fas fa-file-pdf  font-white"></i>
                                                </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="custom_datatable">
                                            <form action="{{url('')}}/reports/incomeStatementsearch" method="POST" id="advanceSearch">
                                                @csrf
                                                <input type="hidden" name="menuid" value="{{$menu_id}}">
                                                <div class="tableview">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                      <div class="form-check-inline">
                                                          <label class="form-check-label" for="radio2">
                                                              <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                            </label>
                                                        </div>

                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Year</label>
                                                                <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                            </div>
                                                        </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Month</label>
                                                        <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="" style="visibility: hidden">.</label>
                                                        <button disabled id="search" style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>

                                            <div class="table-responsive">
                                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="3" align="center" style="font-weight: bold">
                                                                INCOME STATEMENT  ({{$index == 0 ? 'Over All Data' : ($index == 1 ? $year :$month)}})
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-weight: bold">Sales:Add</td>
                                                            <td>{{$sales}}</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Returns Inward:Sub</td>
                                                            <td>{{$return}}</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Net Sales</td>
                                                            <td></td>
                                                            <td>{{$sales - $return}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-weight: bold">COGS</td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Opening Inventory:Add</td>
                                                            <td>{{$opening_stock}}</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Purchases:Add</td>
                                                            <td>{{$purchase}}</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Closing Inventory:Sub</td>
                                                            <td>{{$closing_stock}}</td>
                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-weight: bold">Total COGS</td>
                                                            <td></td>
                                                            <td>{{($opening_stock + $purchase) - $closing_stock }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Sales Incentive:Sub</td>
                                                            <td></td>
                                                            <td>{{$saleinc}}</td>
                                                        </tr>
                                                        @php
                                                            $transportTotal=0;
                                                        @endphp
                                                        @foreach ($transportAccount as $t)
                                                            @if ($index == 0)
                                                                @php
                                                                    $gl = \App\GeneralLedger::where('account_code',$t->Code)->sum('debit');
                                                                    $transportTotal+=$gl;
                                                                @endphp
                                                            @endif
                                                            @if ($index == 1)
                                                                @php
                                                                    $gl = \App\GeneralLedger::where('account_code',$t->Code)
                                                                    ->where('posted_date','LIKE',$year.'%')->sum('debit');
                                                                    $transportTotal+=$gl;
                                                                @endphp
                                                            @endif
                                                            @if ($index == 2)
                                                                @php
                                                                    $gl = \App\GeneralLedger::where('account_code',$t->Code)
                                                                    ->where('period',$month)->sum('debit');
                                                                    $transportTotal+=$gl;
                                                                @endphp
                                                            @endif
                                                            <tr>
                                                                <td>{{$t->name_of_account}}</td>
                                                                <td>{{$gl}}</td>
                                                                <td></td>
                                                            </tr>
                                                        @endforeach
                                                        <tr>
                                                            <td>Total Transportation</td>
                                                            <td></td>
                                                            <td>{{$transportTotal}}</td>
                                                        </tr>
                                                        @php
                                                            $expFinal = 0;
                                                            $Totalexp = 0;
                                                            $tpl = (($sales - $return) - (($opening_stock + $purchase) - $closing_stock) - $saleinc - $transportTotal);
                                                        @endphp
                                                        <tr>
                                                            <td style="font-weight: bold">Gross {{$tpl > 0 ? 'Profit' : 'Loss'}}</td>
                                                            <td></td>
                                                            <td>{{$tpl}}</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-weight: bold">Expenses</td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                        @foreach ($expHead as $e)
                                                            <tr>
                                                                <td style="font-weight: bold">{{$e->name}}</td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            @php
                                                                $expAccount = \App\AccountDetails::where('Code','Like',$e->code.'%')->get();
                                                            @endphp
                                                            @foreach ($expAccount as $a)
                                                                @if ($index == 0)
                                                                    @php
                                                                        $expTotal = \App\GeneralLedger::where('account_code',$a->Code)->sum('debit');
                                                                        $expFinal+=$expTotal;
                                                                        $Totalexp  += $expTotal;
                                                                    @endphp
                                                                @endif
                                                                @if ($index == 1)
                                                                    @php
                                                                        $expTotal = \App\GeneralLedger::where('account_code',$a->Code)
                                                                        ->where('posted_date','LIKE',$year.'%')->sum('debit');
                                                                        $expFinal+=$expTotal;
                                                                        $Totalexp  += $expTotal;
                                                                    @endphp
                                                                @endif
                                                                @if ($index ==2)
                                                                    @php
                                                                        $expTotal = \App\GeneralLedger::where('account_code',$a->Code)
                                                                        ->where('period',$month)->sum('debit');
                                                                        $expFinal+=$expTotal;
                                                                        $Totalexp  += $expTotal;
                                                                    @endphp
                                                                @endif
                                                                <tr>
                                                                    <td>{{$a->name_of_account}}</td>
                                                                    <td>{{$expTotal}}</td>
                                                                    <td></td>
                                                                </tr>
                                                            @endforeach
                                                            <tr>
                                                                <td>Total {{$e->name}}</td>
                                                                <td></td>
                                                                <td>{{$expFinal}}</td>
                                                            </tr>
                                                            @php
                                                                $expFinal = 0;
                                                            @endphp
                                                        @endforeach
                                                        <tr>
                                                            <td style="font-weight: bold">Total Expenses</td>
                                                            <td></td>
                                                            <td>{{$Totalexp}}</td>
                                                        </tr>
                                                        @php
                                                            $final = $tpl - $Totalexp;
                                                        @endphp
                                                        <tr>
                                                            <td style="font-weight: bold">Net {{$final > 0 ? 'Profit' : 'Loss'}}</td>
                                                            <td></td>
                                                            <td>{{$final}}</td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

@section('custom-script')
@toastr_js
@toastr_render

    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable({
                scrollX: true
            });
        });
        $('input:radio[name="optradio"]').change(
        function(){
        if ($(this).is(':checked')) {
            $('#search').prop('disabled',false);
           var val = $(this).val();
           if(val == 'Year')
           {
                $('#year').prop('disabled',false);
                $('#year').attr('required',true);
                $('#month').attr('required',false);
                $('#month').prop('disabled',true);
           }
           if(val == 'Month')
           {
                $('#month').prop('disabled',false);
                $('#month').attr('required',true);
                $('#year').prop('disabled',true);
                $('#year').attr('required',false);
           }
        }
    });
    </script>
@endsection
@endsection
