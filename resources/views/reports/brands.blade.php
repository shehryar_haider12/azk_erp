@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #5F9EA0;
    }
    .modal-header{
        background: #5F9EA0 !important;
    }
    .form-check-inline {
    display: inline-flex;
    align-items: center;
    padding-left: 0;
    margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
    position: static;
    margin-top: 0;
    margin-right: .3125rem;
    margin-left: 0;
    }
</style>
@toastr_css

@section('sidebar-name1')
<li class="breadcrumb-item"><a>Reports</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="card">
                <div class="card-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="card-header" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fas fa-chart-bar font-white"></i>Brands Report
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="custom_datatable">


                                            <div class="table-responsive">
                                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Name</th>
                                                            <th>Purchased</th>
                                                            <th>Sold</th>
                                                            <th>Purchased Amount</th>
                                                            <th>Sold Amount</th>
                                                            <th>Porfit or Loss</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a = 1;
                                                            $ptotal = 0;
                                                            $stotal = 0;
                                                            $sqty = 0;
                                                            $pqty = 0;
                                                            $total = 0;
                                                            $pt = 0 ;
                                                            $st = 0 ;
                                                            $sq = 0 ;
                                                            $pq = 0 ;
                                                            $totals = 0 ;
                                                        @endphp
                                                        @foreach ($brand as $c)
                                                            <tr>
                                                                <td>
                                                                    {{$a}}
                                                                </td>
                                                                <td>
                                                                    {{$c->b_name}}
                                                                </td>

                                                                @if (count($c->products)>0)
                                                                    @php
                                                                        $stock_in_count = $c->products->pluck('stock_in_count')->toArray();
                                                                        $Vcost = $c->products->pluck('Vcost')->toArray();
                                                                        $ptotal = array_sum($stock_in_count) + array_sum($Vcost);
                                                                        $quantityIn = $c->products->pluck('quantityIn')->toArray();
                                                                        $VquantityIn = $c->products->pluck('VquantityIn')->toArray();
                                                                        $pqty = array_sum($quantityIn) + array_sum($VquantityIn);
                                                                        $stocks_out_count = $c->products->pluck('stocks_out_count')->toArray();
                                                                        $Vprice = $c->products->pluck('Vprice')->toArray();
                                                                        $stotal = array_sum($stocks_out_count) + array_sum($Vprice);
                                                                        $quantityOut = $c->products->pluck('quantityOut')->toArray();
                                                                        $VquantityOut = $c->products->pluck('VquantityOut')->toArray();
                                                                        $sqty = array_sum($quantityOut) + array_sum($VquantityOut);

                                                                        $pt += $ptotal ;
                                                                        $st += $stotal ;
                                                                        $sq += $sqty ;
                                                                        $pq += $pqty ;

                                                                    @endphp
                                                                @else
                                                                    @php
                                                                        $ptotal = 0;
                                                                        $stotal = 0;
                                                                        $sqty = 0;
                                                                        $pqty = 0;
                                                                        $pt += $ptotal ;
                                                                        $st += $stotal ;
                                                                        $sq += $sqty ;
                                                                        $pq += $pqty ;
                                                                    @endphp
                                                                @endif
                                                                <td>
                                                                    {{$pqty}}
                                                                </td>
                                                                <td>
                                                                    {{$sqty}}
                                                                </td>
                                                                <td>
                                                                    {{$ptotal}}
                                                                </td>
                                                                <td>
                                                                    {{$stotal}}
                                                                </td>
                                                                <td>
                                                                    @php
                                                                        $total = $stotal - $ptotal;
                                                                        $totals+=$total;
                                                                    @endphp
                                                                    {{$total >= 0 ? $total : '('.$total.')'}}
                                                                </td>

                                                            </tr>
                                                            @php
                                                                $a++;
                                                                $ptotal = 0;
                                                                $total = 0;
                                                                $stotal = 0;
                                                                $sqty = 0;
                                                                $pqty = 0;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th></th>
                                                            <th>TOTAL</th>
                                                            <th>{{$pq}}</th>
                                                            <th>{{$sq}}</th>
                                                            <th>{{$pt}}</th>
                                                            <th>{{$st}}</th>
                                                            <th>{{$totals}}</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

@section('custom-script')
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
@endsection
@endsection
