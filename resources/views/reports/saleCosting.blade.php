@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #5F9EA0;
    }
    .modal-header{
        background: #5F9EA0 !important;
    }
    .form-check-inline {
    display: inline-flex;
    align-items: center;
    padding-left: 0;
    margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
    position: static;
    margin-top: 0;
    margin-right: .3125rem;
    margin-left: 0;
    }
</style>
@toastr_css

@section('sidebar-name1')
<li class="breadcrumb-item"><a>Reports</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="card">
                <div class="card-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="card-header" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class=" fas fa-chart-bar font-white"></i>PROFIT/LOSS STATEMENT
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">
                                                @if ($index == 0)
                                                <a style="margin-left:200px; font-size: 25px; color: white"  href="{{route('reports.saleCosting.excel')}}">
                                                    <i class="fas fa-file-excel  font-white"></i>
                                                </a>
                                                <a style="margin-left:-50px; font-size: 25px; color: white"  href="{{route('reports.saleCosting.pdf')}}">
                                                    <i class="fas fa-file-pdf  font-white"></i>
                                                </a>
                                                @endif

                                                @if ($index == 1)
                                                <a style="margin-left:200px; font-size: 25px; color: white"  href="{{url('')}}/reports/saleCostingsearch/{{$year}}/excel/year">
                                                    <i class="fas fa-file-excel  font-white"></i>
                                                </a>
                                                <a style="margin-left:-50px; font-size: 25px; color: white"  href="{{url('')}}/reports/saleCostingsearch/{{$year}}/pdf/year">
                                                    <i class="fas fa-file-pdf  font-white"></i>
                                                </a>
                                                @endif
                                                @if ($index == 2)
                                                <a style="margin-left:200px; font-size: 25px; color: white"  href="{{url('')}}/reports/saleCostingsearch/{{$month}}/excel/month">
                                                    <i class="fas fa-file-excel  font-white"></i>
                                                </a>
                                                <a style="margin-left:-50px; font-size: 25px; color: white"  href="{{url('')}}/reports/saleCostingsearch/{{$month}}/pdf/month">
                                                    <i class="fas fa-file-pdf  font-white"></i>
                                                </a>
                                                @endif
                                                @if ($index == 3)
                                                <a style="margin-left:200px; font-size: 25px; color: white"  href="{{url('')}}/reports/saleCostingsearch/{{$from}}/{{$to}}/excel/date">
                                                    <i class="fas fa-file-excel  font-white"></i>
                                                </a>
                                                @endif
                                                @if ($index == 4)
                                                <a style="margin-left:200px; font-size: 25px; color: white"  href="{{url('')}}/reports/saleCostingsearch/{{$date}}/excel/date">
                                                    <i class="fas fa-file-excel  font-white"></i>
                                                </a>
                                                <a style="margin-left:-50px; font-size: 25px; color: white"  href="{{url('')}}/reports/saleCostingsearch/{{$date}}/pdf/date">
                                                    <i class="fas fa-file-pdf  font-white"></i>
                                                </a>
                                                @endif

                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="custom_datatable">
                                            <form action="{{url('')}}/reports/saleCostingsearch" method="POST" id="advanceSearch">
                                                @csrf
                                                <input type="hidden" name="menuid" value="{{$menu_id}}">
                                                <div class="tableview">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio2">
                                                            <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Date
                                                        </label>
                                                    </div>


                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Year</label>
                                                                <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                            </div>
                                                        </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Month</label>
                                                        <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">From</label>
                                                            <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">To</label>
                                                            <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="" style="visibility: hidden">.</label>
                                                        <button disabled id="search" style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>

                                            <div class="table-responsive">
                                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="11" style="text-align: center">
                                                                PROFIT/LOSS STATEMENT  ({{$index == 0 ? 'Over All Data' : ($index == 1 ? $year : ($index == 2 ? $month : ($index == 4 ? $date : $from.' - '.$to)))}})
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Invoice#</th>
                                                            <th>Date</th>
                                                            <th>Customer</th>
                                                            <th>Products</th>
                                                            <th>Qty</th>
                                                            <th>Sale Price</th>
                                                            <th>Sale Amount</th>
                                                            <th>Cost Price</th>
                                                            <th>Cost Amount</th>
                                                            <th>Profit/Loss</th>
                                                         </tr>

                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a=1;
                                                            $cost=0;
                                                            $price=0;
                                                            $avg = 0;
                                                            $avg1 = 0;
                                                            $q=0;
                                                            $sp=0;
                                                            $sa=0;
                                                            $cp=0;
                                                            $ca=0;
                                                            $pl=0;
                                                        @endphp
                                                        @foreach ($stock as $s => $st)
                                                            @foreach ($st as $item)
                                                            @if ($item->sale->s_status == "Partial" || $item->sale->s_status == "Complete" || $item->sale->s_status == "Delivered")


                                                                <tr>
                                                                    <td>
                                                                        {{$a}}
                                                                    </td>
                                                                    <td>
                                                                        {{$item->s_id}}
                                                                    </td>
                                                                    <td>
                                                                        {{$s}}
                                                                    </td>
                                                                    <td>
                                                                        {{$item->sale->customer->name}}
                                                                    </td>
                                                                    @if ($item->type == 1)
                                                                        <td>
                                                                            {{$item->variant->name}}
                                                                        </td>
                                                                    @else
                                                                        {{-- {{dd($item)}} --}}
                                                                        <td>
                                                                            {{$item->products['pro_code'].' - '.$item->products['pro_name']}}
                                                                        </td>
                                                                    @endif
                                                                    <td>
                                                                        {{$item->delivered_quantity}}
                                                                    </td>
                                                                    <td>
                                                                        {{$item->price}}
                                                                    </td>
                                                                    <td>
                                                                        {{$item->price * $item->delivered_quantity}}
                                                                        @php
                                                                            $price = $item->sub_total;
                                                                        @endphp
                                                                    </td>
                                                                    @if ($item->purchase->isEmpty())
                                                                        <td>
                                                                            @php
                                                                                $avg= $item->type == 1 ? $item->variant->cost : $item->products['cost'];
                                                                                $count =1 ;
                                                                                $cp+=($avg/$count);
                                                                            @endphp
                                                                            {{$item->type == 1 ? $item->variant->cost : $item->products['cost']}}
                                                                        </td>
                                                                    @else
                                                                        @php
                                                                            $count = count($item->purchase)
                                                                        @endphp
                                                                        <td>
                                                                            @foreach ($item->purchase as $i)
                                                                                @php
                                                                                    $avg += $i->cost;
                                                                                @endphp
                                                                            @endforeach
                                                                            @php
                                                                                $cp+=($avg/$count);
                                                                            @endphp
                                                                            {{$avg/$count}}
                                                                        </td>
                                                                    @endif
                                                                    @if ($item->purchase->isEmpty())
                                                                        <td>
                                                                            {{($item->type == 1 ? $item->variant->cost : $item->products['cost']) * $item->delivered_quantity}}
                                                                        </td>
                                                                        @php
                                                                            $avg1 = ($item->type == 1 ? $item->variant->cost : $item->products['cost']);
                                                                            $count =1 ;
                                                                            $ca+=($avg1/$count) * $item->delivered_quantity;
                                                                            $cost = ($item->type == 1 ? $item->variant->cost : $item->products['cost']) * $item->delivered_quantity;
                                                                        @endphp
                                                                    @else
                                                                        <td>
                                                                            @foreach ($item->purchase as $i)
                                                                                @php
                                                                                    $avg1 += $i->cost;
                                                                                @endphp
                                                                            @endforeach
                                                                            @php
                                                                                $ca+=($avg1/$count) * $item->delivered_quantity;
                                                                            @endphp
                                                                            {{($avg1/$count) * $item->delivered_quantity}}
                                                                        </td>
                                                                        @php
                                                                            $cost = ($avg1/$count) * $item->delivered_quantity;
                                                                        @endphp
                                                                    @endif
                                                                    @if ($price > $cost)
                                                                        <td>
                                                                            {{$price - $cost}}
                                                                        </td>
                                                                    @else
                                                                        <td>
                                                                            ({{$cost - $price}})
                                                                        </td>
                                                                    @endif
                                                                </tr>
                                                                @php
                                                                    $a++;
                                                                    $avg=0;
                                                                    $avg1=0;
                                                                    $q+=$item->quantity;
                                                                    $sp+=$item->price;
                                                                    $sa+=$item->price * $item->delivered_quantity;


                                                                @endphp
                                                            @endif
                                                            @endforeach
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th>TOTAL</th>
                                                            <th></th>
                                                            <th>{{$q}}</th>
                                                            <th>{{$sp}}</th>
                                                            <th>{{$sa}}</th>
                                                            <th>{{$cp}}</th>
                                                            <th>{{$ca}}</th>
                                                            <th></th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

@section('custom-script')
@toastr_js
@toastr_render

    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').DataTable({
                scrollX: true
            });
        });
        $('input:radio[name="optradio"]').change(
        function(){
        if ($(this).is(':checked')) {
            $('#search').prop('disabled',false);
           var val = $(this).val();
           if(val == 'Year')
           {
                $('#year').prop('disabled',false);
                $('#year').attr('required',true);
                $('#month').attr('required',false);
                $('#month').prop('disabled',true);
                $('#month').prop('disabled',true);
                $('#from').prop('disabled',true);
                $('#to').prop('disabled',true);

           }
           if(val == 'Month')
           {
                $('#month').prop('disabled',false);
                $('#month').attr('required',true);
                $('#year').prop('disabled',true);
                $('#year').attr('required',false);
                $('#from').prop('disabled',true);
                $('#to').prop('disabled',true);

           }
           if(val == 'Date')
           {
               $('#from').prop('disabled',false);
               $('#to').prop('disabled',false);
               $('#year').prop('disabled',true);
               $('#year').attr('required',false);
               $('#month').prop('disabled',true);
               $('#month').attr('required',false);
           }
        }
    });
    </script>
@endsection
@endsection
