<html>
  <head>
    {{-- <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> --}}
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
      .attendance-table table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #000;
         font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .blank-cell{

        min-width: 50px;


        }

        .attendance-cell{

        padding: 8px;


        }

        .attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
            border: 1px solid #000;
        }
        h2,h4 {
            color: #00CCFF;
        }
        .table-inv
        {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 50%;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }
        .table-inv thead
        {
            background-color: #ADD8E6;
        }
        .table-inv td,th
        {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        header
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            font-size: 30px;
            text-align: center;
            padding-top: 15px;
            background: #F0F8FF;
            height: 50px;
            font-weight: bold;
        }
        .label
        {
            background: #F0F8FF;
            font-size: 15px;
            text-align: left;
            color: #000;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            height: 60px;
            padding-left: 30px;
            /* font-weight: bold; */
        }
        .row
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            padding-left: 30px;
        }
        .col
        {
            font-size: 18px;
            background: #F0FFFF;
            height: 28px;
        }
        @page
        {
            margin: 0;
            size: A3;
        }
  </style>
  <body>

        <div class="attendance-table" style="margin-top: 25px">
            <h3 style="text-align: center"> INCOME STATEMENT ({{$monthName}})  </h3>
        <table class="table table-striped table-bordered">
            <tbody>
                <tr>
                    <td class="attendance-cell" style="font-weight: bold">Sales:Add</td>
                    <td class="attendance-cell">{{$sales}}</td>
                    <td class="attendance-cell"></td>
                </tr>
                <tr>
                    <td class="attendance-cell">Returns Inward:Sub</td>
                    <td class="attendance-cell">{{$return}}</td>
                    <td class="attendance-cell"></td>
                </tr>
                <tr>
                    <td class="attendance-cell">Net Sales</td>
                    <td class="attendance-cell"></td>
                    <td class="attendance-cell">{{$sales - $return}}</td>
                </tr>
                <tr>
                    <td style="font-weight: bold" class="attendance-cell">COGS</td>
                    <td class="attendance-cell"></td>
                    <td class="attendance-cell"></td>
                </tr>
                <tr>
                    <td class="attendance-cell">Opening Inventory:Add</td>
                    <td class="attendance-cell">{{$opening_stock}}</td>
                    <td class="attendance-cell"></td>
                </tr>
                <tr>
                    <td class="attendance-cell">Purchases:Add</td>
                    <td class="attendance-cell">{{$purchase}}</td>
                    <td class="attendance-cell"></td>
                </tr>
                <tr>
                    <td class="attendance-cell">Closing Inventory:Sub</td>
                    <td class="attendance-cell">{{$closing_stock}}</td>
                    <td class="attendance-cell"></td>
                </tr>
                <tr>
                    <td style="font-weight: bold" class="attendance-cell">Total COGS</td>
                    <td class="attendance-cell"></td>
                    <td class="attendance-cell">{{($opening_stock + $purchase) - $closing_stock }}</td>
                </tr>
                <tr>
                    <td class="attendance-cell">Sales Incentive:Sub</td>
                    <td class="attendance-cell"></td>
                    <td class="attendance-cell">{{$saleinc}}</td>
                </tr>
                @php
                    $transportTotal=0;
                @endphp
                @foreach ($transportAccount as $t)
                    @php
                        $gl = \App\GeneralLedger::where('account_code',$t->Code)->where('period',$month)->sum('debit');
                        $transportTotal+=$gl;
                    @endphp
                    <tr>
                        <td class="attendance-cell">{{$t->name_of_account}}</td>
                        <td class="attendance-cell">{{$gl}}</td>
                        <td class="attendance-cell"></td>
                    </tr>
                @endforeach
                <tr>
                    <td class="attendance-cell">Total Transportation</td>
                    <td class="attendance-cell"></td>
                    <td class="attendance-cell">{{$transportTotal}}</td>
                </tr>
                @php
                    $expFinal = 0;
                    $Totalexp = 0;
                    $tpl = (($sales - $return) - (($opening_stock + $purchase) - $closing_stock) - $saleinc - $transportTotal);
                @endphp
                <tr>
                    <td style="font-weight: bold" class="attendance-cell">Gross {{$tpl > 0 ? 'Profit' : 'Loss'}}</td>
                    <td class="attendance-cell"></td>
                    <td class="attendance-cell">{{$tpl}}</td>
                </tr>
                <tr>
                    <td class="attendance-cell" style="font-weight: bold">Expenses</td>
                    <td class="attendance-cell"></td>
                    <td class="attendance-cell"></td>
                </tr>
                @foreach ($expHead as $e)
                    <tr>
                        <td class="attendance-cell" style="font-weight: bold">{{$e->name}}</td>
                        <td class="attendance-cell"></td>
                        <td class="attendance-cell"></td>
                    </tr>
                    @php
                        $expAccount = \App\AccountDetails::where('Code','Like',$e->code.'%')->get();
                    @endphp
                    @foreach ($expAccount as $a)
                        @php
                            $expTotal = \App\GeneralLedger::where('account_code',$a->Code)->where('period',$month)->sum('debit');
                            $expFinal+=$expTotal;
                            $Totalexp  += $expTotal;
                        @endphp
                        <tr>
                            <td class="attendance-cell">{{$a->name_of_account}}</td>
                            <td class="attendance-cell">{{$expTotal}}</td>
                            <td class="attendance-cell"></td>
                        </tr>
                    @endforeach
                    <tr>
                        <td class="attendance-cell">Total {{$e->name}}</td>
                        <td class="attendance-cell"></td>
                        <td class="attendance-cell">{{$expFinal}}</td>
                    </tr>
                    @php
                        $expFinal = 0;
                    @endphp
                @endforeach
                <tr>
                    <td style="font-weight: bold" class="attendance-cell">Total Expenses</td>
                    <td class="attendance-cell"></td>
                    <td class="attendance-cell">{{$Totalexp}}</td>
                </tr>
                @php
                    $final = $tpl - $Totalexp;
                @endphp
                <tr>
                    <td class="attendance-cell" style="font-weight: bold">Net {{$final > 0 ? 'Profit' : 'Loss'}}</td>
                    <td class="attendance-cell"></td>
                    <td class="attendance-cell">{{$final}}</td>
                </tr>
            </tbody>

        </table>

    </div>
  </body>
</html>
