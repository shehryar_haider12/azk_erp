@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #5F9EA0;
    }
    .modal-header{
        background: #5F9EA0 !important;
    }
    .form-check-inline {
    display: inline-flex;
    align-items: center;
    padding-left: 0;
    margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
    position: static;
    margin-top: 0;
    margin-right: .3125rem;
    margin-left: 0;
    }
</style>
@toastr_css
@section('sidebar-name1')
<li class="breadcrumb-item"><a>Reports</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="card">
                <div class="card-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="card-header" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fa fa-bar-chart fas fa-chart-barfont-white"></i>Customer Detail Report
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="custom_datatable">
                                            {{-- <form action="{{url('')}}/reports/incomeStatementsearch" method="POST" id="advanceSearch">
                                                @csrf
                                                <input type="hidden" name="menuid" value="{{$menu_id}}">
                                                <div class="tableview">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                      <div class="form-check-inline">
                                                          <label class="form-check-label" for="radio2">
                                                              <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                            </label>
                                                        </div>

                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Year</label>
                                                                <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                            </div>
                                                        </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Month</label>
                                                        <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="" style="visibility: hidden">.</label>
                                                        <button disabled id="search" style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form> --}}

                                            <div class="table-responsive">
                                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Date</th>
                                                            <th>Ref. No</th>
                                                            <th>Biller</th>
                                                            <th>Customer</th>
                                                            <th>Product(Qty)</th>
                                                            <th>Total</th>
                                                            <th>Paid</th>
                                                            <th>Balance</th>
                                                            <th>Sale Status</th>
                                                            <th>Payment Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a = 1;
                                                            $total = 0;
                                                            $t=0;
                                                            $p=0;
                                                            $b=0;
                                                        @endphp
                                                        @foreach ($sales as $c)
                                                            <tr>
                                                                <td>
                                                                    {{$c->id}}
                                                                </td>
                                                                <td>
                                                                    {{$c->sale_date}}
                                                                </td>
                                                                <td>
                                                                    {{$c->ref_no}}
                                                                </td>
                                                                <td>
                                                                    {{$c->biller->name}}
                                                                </td>
                                                                <td>
                                                                    {{$c->customer->name}}
                                                                </td>
                                                                <td>
                                                                    @foreach ($c->sdetails as $d)
                                                                        {{$d->type == 0 ? $d->products->pro_name.'('.$d->quantity.')' : $d->variant->name.'('.$d->quantity.')'}}
                                                                    @endforeach
                                                                </td>
                                                                <td>
                                                                    {{$c->total}}
                                                                </td>
                                                                <td>
                                                                    {{$c->total_amount == null ? 0 : $c->total_amount }}
                                                                </td>
                                                                <td>
                                                                    {{$c->total - $c->total_amount}}
                                                                </td>
                                                                @if($c->s_status=='Pending')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-warning" id="{{$c->id}}">
                                                                            {{$c->s_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->s_status=='Approved')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-primary" id="{{$c->id}}">
                                                                            {{$c->s_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->s_status=='Partial')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-info" id="{{$c->id}}">
                                                                            {{$c->s_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->s_status=='Complete')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-success" id="{{$c->id}}">
                                                                            {{$c->s_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->s_status=='Return')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-default" id="{{$c->id}}">
                                                                            {{$c->s_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->s_status=='Closed')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs"  style="background-color:BurlyWood" id="{{$c->id}}">
                                                                            {{$c->s_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->s_status=='Delivered')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-danger" id="{{$c->id}}">
                                                                            {{$c->s_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->s_status=='Cancel')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-warning" id="{{$c->id}}">
                                                                            {{$c->s_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->p_status=='Pending')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-warning" id="{{$c->id}}">
                                                                            {{$c->p_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->p_status=='Partial')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-primary" id="{{$c->id}}">
                                                                            {{$c->p_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->p_status=='Paid')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-success" id="{{$c->id}}">
                                                                            {{$c->p_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->p_status=='Return')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-info" id="{{$c->id}}">
                                                                            {{$c->p_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                            </tr>
                                                            @php
                                                                $a++;
                                                                $total = 0;
                                                                $t += $c->total;
                                                                $p += $c->total_amount == null ? 0 : $c->total_amount;
                                                                $b += ($c->total - $c->total_amount);
                                                            @endphp
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th>TOTAL</th>
                                                            <th></th>
                                                            <th>{{$t}}</th>
                                                            <th>{{$p}}</th>
                                                            <th>{{$b}}</th>
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('modal')
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg" >

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Sales</h4>
                        <button type="button" class="close" data-dismiss="modal">×</button>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Sale Date</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Name" id="sale_date" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Reference No</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Code" id="ref_no" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Warehouse</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Weight" id="w_name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Customer</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="c_name" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Accountant</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="b_name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Sale Person</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="s_name" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Tax Status</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="tax_status" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Advance payment</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="advance" readonly>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-1">
                                <a href="" id="edithref">
                                    <button type="button"  id="edit" class="btn btn-sm  btn-info" >Edit</button>
                                </a>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <table id="example1" class="table table-responsive table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th >S.No</th>
                                            <th >Code - Name</th>
                                            <th>Brand</th>
                                            <th >Price</th>
                                            <th >Quantity</th>
                                            <th >Delivered Quantity</th>
                                            <th >Return Quantity</th>
                                            <th >Discounted Amount</th>
                                            <th >Sub total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>

                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    @endsection
@section('custom-script')
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
        function view(id)
        {
            $("#example1 tbody").empty();
            $('#example1 tfoot').empty();
            $.ajax({
                url:"{{url('')}}/sales/"+id,
                method:"GET",
                error: function (request, error) {
                    lert(" Can't do because: " + error +request);
                },
                success:function(data){
                    console.log(data);
                    $('#sale_date').val(data[0].sale_date);
                    $('#ref_no').val(data[0].ref_no);
                    $('#w_name').val(data[0].warehouse.w_name);
                    $('#c_name').val(data[0].customer.name);
                    $('#b_name').val(data[0].biller.name);
                    $('#s_name').val(data[0].saleperson.name);
                    $('#advance').val(data[0].advance);
                    $('#tax_status').val(data[0].tax_status);
                    if(data[0].s_status == "Pending")
                    {
                        $('#edit').attr('disabled',false);
                    }
                    else
                    {
                        $('#edit').attr('disabled',true);
                    }
                    $('#edithref').attr('href','{{url('')}}/sales/'+id+'/edit');
                    var tp =0;
                    var tq =0;
                    var dq =0;
                    var st =0;
                    for (let i = 0; i < data[1].length; i++) {
                        if(data[1][i].type == 1)
                        {
                            $("#example1").append("<tr><td>"+data[1][i].id+"</td><td>"+data[1][i].variant.name+"</td><td>"+data[1][i].variant.product.brands.b_name+"</td><td>"+data[1][i].price+"</td><td>"+data[1][i].quantity+"</td><td>"+data[1][i].delivered_quantity+"</td><td>"+data[1][i].returnQty+"</td> <td>"+data[1][i].discounted_amount+"</td>  <td>"+data[1][i].sub_total+"</td>  </tr>");
                            tp = +tp + +data[1][i].price;
                            tq = +tq + +data[1][i].quantity;
                            dq = +dq + +data[1][i].delivered_quantity;
                            st = +st + +data[1][i].sub_total;
                        }
                        else
                        {
                            $("#example1").append("<tr><td>"+data[1][i].id+"</td><td>"+data[1][i].products.pro_code+" - "+data[1][i].products.pro_name+"</td><td>"+data[1][i].products.brands.b_name+"</td> <td>"+data[1][i].price+"</td>  <td>"+data[1][i].quantity+"</td><td>"+data[1][i].delivered_quantity+"</td><td>"+data[1][i].returnQty+"</td>   <td>"+data[1][i].discounted_amount+"</td>  <td>"+data[1][i].sub_total+"</td>  </tr>");
                            tp = +tp + +data[1][i].price;
                            tq = +tq + +data[1][i].quantity;
                            dq = +dq + +data[1][i].delivered_quantity;
                            st = +st + +data[1][i].sub_total;
                        }
                    }
                    $('#example1 tfoot').append(`<tr><td>TOTAL</td><td></td><td></td><td>`+tp+`</td><td>`+tq+`</td><td>`+dq+`</td><td></td><td></td><td>`+st+`</td></tr>`);
                    $('#myModal').modal("show");
                    $('#example1').DataTable();

                }
            });
        }
        $('#example').on('click','td', function () {
        var col_index = $(this).index();
        var row_index = $(this).parent().index();
        var count = document.getElementById('example').rows[row_index].cells.length ;
        if(col_index <count)
        {
            var $tr = $(this).closest('tr');
            var id = $tr.find("td:eq(0)").text();
            view(id);
        }
    });
    </script>
@endsection
@endsection
