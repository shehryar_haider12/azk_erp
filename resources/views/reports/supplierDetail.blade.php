@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #5F9EA0;
    }
    .modal-header{
        background: #5F9EA0 !important;
    }
    .form-check-inline {
    display: inline-flex;
    align-items: center;
    padding-left: 0;
    margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
    position: static;
    margin-top: 0;
    margin-right: .3125rem;
    margin-left: 0;
    }
</style>
@toastr_css

@section('sidebar-name1')
<li class="breadcrumb-item"><a>Reports</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="card">
                <div class="card-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="card-header" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fas fa-chart-bar font-white"></i>Supplier Detail Report
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="custom_datatable">
                                            {{-- <form action="{{url('')}}/reports/incomeStatementsearch" method="POST" id="advanceSearch">
                                                @csrf
                                                <input type="hidden" name="menuid" value="{{$menu_id}}">
                                                <div class="tableview">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                      <div class="form-check-inline">
                                                          <label class="form-check-label" for="radio2">
                                                              <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                            </label>
                                                        </div>

                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Year</label>
                                                                <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                            </div>
                                                        </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Month</label>
                                                        <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="" style="visibility: hidden">.</label>
                                                        <button disabled id="search" style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form> --}}

                                            <div class="table-responsive">
                                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Date</th>
                                                            <th>Ref. No</th>
                                                            <th>Biller</th>
                                                            <th>Supplier</th>
                                                            <th>Product(Qty)</th>
                                                            <th>Total</th>
                                                            <th>Paid</th>
                                                            <th>Balance</th>
                                                            <th>Purchase Status</th>
                                                            <th>Payment Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a = 1;
                                                            $total = 0;
                                                        @endphp
                                                        @foreach ($purchase as $c)
                                                            <tr>
                                                                <td>
                                                                    {{$c->id}}
                                                                </td>
                                                                <td>
                                                                    {{$c->order_date}}
                                                                </td>
                                                                <td>
                                                                    {{$c->ref_no}}
                                                                </td>
                                                                <td>
                                                                    {{$c->biller == null ? 'No Biller' : $c->biller->nam}}
                                                                </td>
                                                                <td>
                                                                    {{$c->supplier->name}}
                                                                </td>
                                                                <td>
                                                                    @foreach ($c->orderdetails as $d)
                                                                        {{$d->type == 0 ? $d->products->pro_name.'('.$d->quantity.')' : $d->variant->name.'('.$d->quantity.')'}}
                                                                    @endforeach
                                                                </td>
                                                                <td>
                                                                    {{$c->total}}
                                                                </td>
                                                                <td>
                                                                    {{$c->total_amount == null ? 0 : $c->total_amount }}
                                                                </td>
                                                                <td>
                                                                    {{$c->total - $c->total_amount}}
                                                                </td>
                                                                @if($c->status=='Pending')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-warning" id="{{$c->id}}">
                                                                            {{$c->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->status=='Approved')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-primary" id="{{$c->id}}">
                                                                            {{$c->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->status=='Partial')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-info" id="{{$c->id}}">
                                                                            {{$c->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->status=='Received')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-success" id="{{$c->id}}">
                                                                            {{$c->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->status=='Closed')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-danger" id="{{$c->id}}">
                                                                            {{$c->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->p_status=='Pending')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-warning" id="{{$c->id}}">
                                                                            {{$c->p_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->p_status=='Partial')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-info" id="{{$c->id}}">
                                                                            {{$c->p_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->p_status=='Paid')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  tn-success" id="{{$c->id}}">
                                                                            {{$c->p_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                            </tr>
                                                            @php
                                                                $a++;
                                                                $total = 0;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('modal')
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Purchase Order</h4>
                    <button type="button" class="close" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Order Date</label>
                                <input class="form-control" type="text" placeholder="Enter Product Name" id="order_date" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Purchase Order No</label>
                                <input class="form-control" type="text" placeholder="Enter Product Code" id="o_no" readonly>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Reference No</label>
                                <input class="form-control" type="text"  id="ref_no" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Warehouse</label>
                                <input class="form-control" type="text" placeholder="Enter Product Weight" id="w_name" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Supplier</label>
                                <input class="form-control" type="text" placeholder="Enter Product Unit" id="s_name" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Accountant</label>
                                <input class="form-control" type="text" placeholder="Biller Name" id="b_name" readonly>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="example1" class="table table-responsive table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th width="5%">S.No</th>
                                        <th width="35%">Code - Name</th>
                                        <th width="7%">Cost</th>
                                        <th width="5%">Quantity</th>
                                        <th width="5%">Received Quantity</th>
                                        <th width="10%">Sub total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot></tfoot>


                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    @endsection
@section('custom-script')
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });

        function view(id)
        {
            $("#example1 tbody").empty();
            $('#example1 tfoot').empty();
            $.ajax({
                url:"{{url('')}}/purchase/"+id,
                method:"GET",
                error: function (request, error) {
                    lert(" Can't do because: " + error +request);
                },
                success:function(data){
                    console.log(data);
                    $('#order_date').val(data[0].order_date);
                    $('#ref_no').val(data[0].ref_no);
                    $('#o_no').val(data[0].id);
                    $('#w_name').val(data[0].warehouse.w_name);
                    $('#s_name').val(data[0].supplier.name);
                    var tc =0;
                    var tq =0;
                    var tr =0;
                    var ts =0;
                    if(data[0].biller == null)
                    {

                    }
                    else
                    {
                        $('#b_name').val(data[0].biller.name);
                    }
                    for (let i = 0; i < data[1].length; i++) {
                        if(data[1][i].variant == null)
                        {
                            tc = +tc + +data[1][i].cost;
                            tq = +tq + +data[1][i].quantity;
                            tr = +tr + +data[1][i].received_quantity;
                            ts = +ts + +data[1][i].sub_total;
                            $('#example1').append(`<tr><td>`+data[1][i].id+`</td><td>`+data[1][i].products.pro_code+` - `+data[1][i].products.pro_name+` - `+data[1][i].products.unit.u_name+` - `+data[1][i].products.brands.b_name+`</td><td>`+data[1][i].cost+`</td><td>`+data[1][i].quantity+`</td><td>`+data[1][i].received_quantity+`</td><td>`+data[1][i].sub_total+`</td></tr>`);
                        }
                        else
                        {
                            tc = +tc + +data[1][i].cost;
                            tq = +tq + +data[1][i].quantity;
                            tr = +tr + +data[1][i].received_quantity;
                            ts = +ts + +data[1][i].sub_total;
                            $('#example1').append(`<tr><td>`+data[1][i].id+`</td><td>`+data[1][i].variant.name+`</td><td>`+data[1][i].cost+`</td><td>`+data[1][i].quantity+`</td><td>`+data[1][i].received_quantity+`</td><td>`+data[1][i].sub_total+`</td></tr>`);
                        }
                    }
                    $('#example1 tfoot').append(`<tr><td>TOTAL</td><td></td><td>`+tc+`</td><td>`+tq+`</td><td>`+tr+`</td><td>`+ts+`</td></tr>`);
                    $('#myModal').modal("show");
                    $('#example1').DataTable();

                }
            });
        }
        $('#example').on('click','td', function () {
        var col_index = $(this).index();
        var row_index = $(this).parent().index();
        var count = document.getElementById('example').rows[row_index].cells.length ;
        if(col_index <count)
        {
            var $tr = $(this).closest('tr');
            var id = $tr.find("td:eq(0)").text();
            view(id);
        }
    });
    </script>
@endsection
@endsection
