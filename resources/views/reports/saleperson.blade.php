@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #5F9EA0;
    }
    .modal-header{
        background: #5F9EA0 !important;
    }
    .form-check-inline {
    display: inline-flex;
    align-items: center;
    padding-left: 0;
    margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
    position: static;
    margin-top: 0;
    margin-right: .3125rem;
    margin-left: 0;
    }
</style>
@toastr_css

@section('sidebar-name1')
<li class="breadcrumb-item"><a>Reports</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="card">
                <div class="card-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="card-header" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fas fa-chart-bar font-white"></i>Sale Persons Report
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="custom_datatable">

                                            <div class="table-responsive">
                                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Name</th>
                                                            <th>Phone</th>
                                                            <th>Email</th>
                                                            <th>Total Sales</th>
                                                            <th>Total Amount</th>
                                                            <th>Total Return</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a = 1 ;
                                                            $ts=0;
                                                            $ta=0;
                                                            $tr=0;
                                                        @endphp
                                                        @foreach ($saleperson as $s)
                                                            <tr>
                                                                <td>
                                                                    {{$a}}
                                                                </td>
                                                                <td>
                                                                    {{$s->name}}
                                                                </td>
                                                                <td>
                                                                    {{$s->c_no}}
                                                                </td>
                                                                <td>
                                                                    {{$s->email}}
                                                                </td>
                                                                <td>
                                                                    {{$s->total}}
                                                                </td>
                                                                <td>
                                                                    {{$s->total_amount == null ? 0 : $s->total_amount}}
                                                                </td>
                                                                <td>
                                                                    @php
                                                                        $counts = count($s->salesp) > 0 ? $s->salesp->pluck('return.id')->toArray() : 0;
                                                                        $totalamount = count($s->salesp) > 0 ? $s->salesp->pluck('return.total')->toArray() : 0;
                                                                        $count  = count($s->salesp) > 0 ? count($counts) : 0;
                                                                        $totalamountret = count($s->salesp) > 0 ? array_sum($totalamount) : 0;

                                                                    @endphp
                                                                    {{$count}} ({{$totalamountret}})
                                                                </td>
                                                            </tr>
                                                            @php
                                                                $ts+=$s->total;
                                                                $ta+=$s->total_amount == null ? 0 : $s->total_amount;
                                                                $tr+=$count;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th>TOTAL</th>
                                                            <th>{{$ts}}</th>
                                                            <th>{{$ta}}</th>
                                                            <th>{{$tr}}</th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

@section('custom-script')
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
@endsection
@endsection
