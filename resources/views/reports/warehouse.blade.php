@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #5F9EA0;
    }
    .modal-header{
        background: #5F9EA0 !important;
    }
    .form-check-inline {
    display: inline-flex;
    align-items: center;
    padding-left: 0;
    margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
    position: static;
    margin-top: 0;
    margin-right: .3125rem;
    margin-left: 0;
    }
</style>
@toastr_css

@section('sidebar-name1')
<li class="breadcrumb-item"><a>Reports</a></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="card">
                <div class="card-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="card-header" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-5  col-sm-5 col-xs-5">
                                                <i class="fas fa-chart-bar font-white"></i>Warehouse Stock Report (All Warehouses)
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-5" style="background-color: #428BCA; margin-left: 100px; padding:20px !important;">
                                                <h3 style=" text-align:center; font-weight:bold" >{{$total}}</h3>
                                                <p style="text-align:center; font-size: 20px">Total Items</p>
                                            </div>
                                            <div class="col-sm-5" style="background-color: #78CD51; padding: 20px !important; ">
                                                <h3 style=" text-align:center;font-weight:bold" >{{$qty}}</h3>
                                                <p style="text-align:center; font-size: 20px">Total Quantity</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <canvas id="canvas" height="280" width="600"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @endsection
