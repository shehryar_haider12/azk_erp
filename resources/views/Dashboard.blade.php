@extends('layouts.master')
@section('top-styles')
    <style>
        .links {
            background: #5F9EA0;
            color: rgba(255,255,255,.8);
            display: block;
            text-align: center;
            padding-top: 10px;
            padding-bottom: 10px;
        }
    </style>
@endsection
@section('sidebar-name1')
    <li class="breadcrumb-item"><a href="{{url('')}}/dashboard">Dashboard</a></li>
@endsection
@section('header-content')

    <div class="row">
        @if (in_array('count biller',$permission))
            <div class="col-xl-3 col-md-6">
                <div class="card card-stats">
                <!-- Card body -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title text-uppercase text-muted mb-4">TOTAL BILLERS</h5>
                                <span class="h2 font-weight-bold mb-0">{{$biller}}</span>
                            </div>
                            <div class="col-auto">
                                <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                    <i class="ni ni-single-02"></i>
                                </div>
                            </div>
                            <a href="{{route('biller.index')}}" class="small-box-footer" style="padding-left: 16px">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if (in_array('count supplier',$permission))
            <div class="col-xl-3 col-md-6">
                <div class="card card-stats">
                <!-- Card body -->
                    <div class="card-body">
                    <div class="row">
                        <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-4">TOTAL SUPPLIERS</h5>
                        <span class="h2 font-weight-bold mb-0">{{$supplier}}</span>
                        </div>
                        <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                            <i class="ni ni-single-02"></i>
                        </div>
                        </div>
                        <a href="{{route('supplier.index')}}" class="small-box-footer" style="padding-left: 16px">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                    </div>
                </div>
            </div>
        @endif
        @if (in_array('count customer',$permission))
            <div class="col-xl-3 col-md-6">
                <div class="card card-stats">
                <!-- Card body -->
                    <div class="card-body">
                    <div class="row">
                        <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">TOTAL CUSTOMERS</h5>
                        <span class="h2 font-weight-bold mb-0">{{$customer}}</span>
                        </div>
                        <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                            <i class="ni ni-single-02"></i>
                        </div>
                        </div>
                        <a href="{{route('customer.index')}}" class="small-box-footer" style="padding-left: 16px">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                    </div>
                </div>
            </div>
        @endif
        @if (in_array('count sale person',$permission))
            <div class="col-xl-3 col-md-6">
                <div class="card card-stats">
                <!-- Card body -->
                    <div class="card-body">
                    <div class="row">
                        <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">TOTAL SALE PERSONS</h5>
                        <span class="h2 font-weight-bold mb-0">{{$sp}}</span>
                        </div>
                        <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                            <i class="ni ni-single-02"></i>
                        </div>
                        </div>
                        <a href="{{route('saleperson.index')}}" class="small-box-footer" style="padding-left: 16px">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                    </div>
                </div>
            </div>
        @endif
        @if (in_array('count brands',$permission))
            <div class="col-xl-3 col-md-6">
                <div class="card card-stats">
                <!-- Card body -->
                    <div class="card-body">
                    <div class="row">
                        <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-4">TOTAL BRANDS</h5>
                        <span class="h2 font-weight-bold mb-0">{{$brand}}</span>
                        </div>
                        <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                            <i class="fa fa-bold"></i>
                        </div>
                        </div>
                        <a href="{{route('brands.index')}}" class="small-box-footer" style="padding-left: 16px">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                    </div>
                </div>
            </div>
        @endif
        @if (in_array('count category',$permission))
            <div class="col-xl-3 col-md-6">
                <div class="card card-stats">
                <!-- Card body -->
                    <div class="card-body">
                    <div class="row">
                        <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">TOTAL CATEGORIES</h5>
                        <span class="h2 font-weight-bold mb-0">{{$cat}}</span>
                        </div>
                        <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                            <i class="fab fa-cuttlefish"></i>
                        </div>
                        </div>
                        <a href="{{route('category.index')}}" class="small-box-footer" style="padding-left: 16px">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                    </div>
                </div>
            </div>
        @endif
        @if (in_array('count purchase orders',$permission))
            <div class="col-xl-3 col-md-6">
                <div class="card card-stats">
                <!-- Card body -->
                    <div class="card-body">
                    <div class="row">
                        <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">TOTAL PURCHASE ORDERS </h5>
                        <span class="h2 font-weight-bold mb-0">{{$po}}</span>
                        </div>
                        <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                        </div>
                        <a href="{{route('purchase.index')}}" class="small-box-footer" style="padding-left: 16px">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                    </div>
                </div>
            </div>
        @endif
        @if (in_array('count sales',$permission))
            <div class="col-xl-3 col-md-6">
                <div class="card card-stats">
                <!-- Card body -->
                    <div class="card-body">
                    <div class="row">
                        <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">TOTAL SALES ORDERS</h5>
                        <span class="h2 font-weight-bold mb-0">{{$sale}}</span>
                        </div>
                        <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                            <i class="fa fa-credit-card"></i>
                        </div>
                        </div>
                        <a href="{{route('sales.index')}}" class="small-box-footer" style="padding-left: 16px" >More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                    </div>
                </div>
            </div>
        @endif
        @if (in_array('count finish',$permission))
            <div class="col-xl-3 col-md-6">
                <div class="card card-stats">
                <!-- Card body -->
                    <div class="card-body">
                    <div class="row">
                        <div class="col">
                        <h5 class="card-title text-uppercase text-muted mb-0">TOTAL PRODUCTS</h5>
                        <span class="h2 font-weight-bold mb-0">{{$finish== null ? 0 : $finish[0]->quantity}}</span>
                        </div>
                        <div class="col-auto">
                        <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                            <i class="fa fa-list-alt"></i>
                        </div>
                    </div>
                        <a href="{{route('product.index')}}" class="small-box-footer" style="padding-left: 15px">More info <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                    </div>
                </div>
            </div>
        @endif

    </div>
@endsection

@section('content')
    <hr color="black" >
    <h3 style="text-decoration: underline"><b>QUICK LINKS</b></h3>
    <div class="row">
        @if (in_array('Add Bank',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/bank/create" class="links"><i id="icon" class="fas fa-landmark"></i> &nbsp; Add Bank</a>
        </div>
        @endif
        @if (in_array('Add Brand',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/brands/create" class="links"><i id="icon" class="fa fa-bold"></i>&nbsp; Add Brand</a>
        </div>
        @endif
        @if (in_array('Add Product',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/product/create" class="links"><i id="icon" class="fa fa-list-alt"></i>&nbsp; Add Product</a>
        </div>
        @endif
        @if (in_array('Add Finish Product',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/product/add" class="links"><i id="icon" class="fa fa-list-alt"></i>Add Finish Product</a>
        </div>
        @endif
    </div>
    <br>
    <div class="row">
        @if (in_array('Add Stock',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/stocks/create" class="links"><i id="icon" class="fa fa-tasks"></i>&nbsp; Add Stock</a>
        </div>
        @endif
        @if (in_array('Add Purchase Order',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/purchase/create" class="links"><i id="icon" class="fa fa-shopping-cart"></i>&nbsp; New Purchase Order</a>
        </div>
        @endif
        @if (in_array('Add Sale',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/sales/create" class="links"><i id="icon" class="fa fa-credit-card"></i>&nbsp; New Sale Order</a>
        </div>
        @endif


    </div>
@endsection



