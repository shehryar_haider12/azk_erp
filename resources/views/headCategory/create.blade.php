@extends('layouts.master')
@section('top-styles')
@toastr_css
@section('sidebar-name1')
<li class="breadcrumb-item"><a href="{{ url('')}}/headcategory">Head Category</a></li>
<li class="breadcrumb-item"><span>{{$isEdit ? 'Edit' : 'Add'}} Head Category</span></li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="card" id="form_wizard_1">
                <div class="card-header">
                    <div class="caption">
                        <i class=" fa fa-money-bill-alt font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Head Category</span>
                    </div>

                </div>
                <div class="card-body">
                    <!-- BEGIN FORM-->

                    <form action="{{$isEdit ? route('headcategory.update',$hcat->id) :  route('headcategory.store')}}" class="form-horizontal" method="POST" id="HoCForm">
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >HOA Name<span class="text-danger">*</span></label>
                                        @if ($isEdit)
                                        <select  id="a_id" size="1" class="form-control js-example-basic-single" name="a_id" disabled>
                                            <option value="" disabled selected>Select...</option>
                                                @foreach ($hoa as $s)
                                                <option {{$s->id == $hcat->a_id ? 'selected' : null}} value="{{$s->id}}">{{$s->name}}</option>
                                                @endforeach
                                        </select>
                                            @else
                                            <select  id="a_id" size="1" class="form-control js-example-basic-single" name="a_id" >
                                                <option value="" disabled selected>Select...</option>
                                                    @foreach ($hoa as $s)
                                                    <option value="{{$s->id}}" {{old('a_id') == $s->id ? 'selected' : null}}>{{$s->name}}</option>
                                                    @endforeach
                                            </select>
                                            <span class="text-danger">{{$errors->first('a_id') ? 'Select anyone' : null}}</span>
                                            @endif
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Head Category Name<span class="text-danger">*</span></label>
                                        <input value="{{$hcat->name ?? old('name')}}" class="form-control" type="text" placeholder="Enter Head Category Name" name="name" >
                                        <span class="text-danger">{{$errors->first('name') ? 'Enter Name' : null}}</span>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Code<span class="text-danger">*</span></label>
                                        <input value="{{$hcat->code ?? old('code')}}" class="form-control" type="text" name="code" id="code" readonly >
                                    </div>
                                </div>
                            </div>
                            <br>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
@endsection
@section('custom-script')
@toastr_js
@toastr_render
<script>
    $('#HoCForm').validate({
        rules: {
            a_id: {
                required: true,
            },
            name: {
                required: true,
            },
            code: {
                required: true,
            },

        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback back_error');
            element.closest('.form-outline').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');

        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
</script>
<script>
    $(document).on('change','#a_id',function(){
        var id=$(this).val();
        $.ajax({
            url:"{{url('')}}/headofaccounts/"+id,
            method:"GET",
            error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
            success:function(data){
                $('#code').val(data);
            }
        });
    });
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });

</script>
@endsection
